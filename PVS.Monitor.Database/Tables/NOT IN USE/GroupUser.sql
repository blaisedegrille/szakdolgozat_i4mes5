/*
   2021. március 13., szombat16:53:24
   User: sa
   Server: tigris
   Database: PVS_Monitor
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.[GroupUser]
	(
	Id int NOT NULL IDENTITY(1,1),
	GroupId int NOT NULL CONSTRAINT FK_GroupUser_GroupId FOREIGN KEY REFERENCES dbo.[Group](Id),
	UserId int NOT NULL CONSTRAINT FK_GroupUser_UserId FOREIGN KEY REFERENCES dbo.[User](Id),
	Active bit NOT NULL,
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.[GroupUser] ADD CONSTRAINT
	PK_GroupUser PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[GroupUser] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[GroupUser]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[GroupUser]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[GroupUser]', 'Object', 'CONTROL') as Contr_Per 