/*
   2021. március 13., szombat14:56:09
   User: sa
   Server: tigris
   Database: PVS_Monitor
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.[Group]
	(
	Id int NOT NULL IDENTITY(1,1),
	Name nvarchar(50) NOT NULL,
	Description nvarchar(100) NOT NULL,
	Active bit NOT NULL,
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.[Group] ADD CONSTRAINT
	PK_Group PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[Group] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[Group]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[Group]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[Group]', 'Object', 'CONTROL') as Contr_Per 