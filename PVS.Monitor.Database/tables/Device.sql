/*
   2021. május 3., hétfő22:12:05
   User: sa
   Server: tigris
   Database: PVS_Monitor
   Application: PVS.Monitor
   Author: racsbalazs
*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.[Device]
	(
	Id int NOT NULL IDENTITY(1,1),
	DeviceId nvarchar(100) NOT NULL,
	CONSTRAINT AK_DeviceId UNIQUE(DeviceId),
	PvSystemId nvarchar(100) NOT NULL CONSTRAINT FK_Device_PvSystemId FOREIGN KEY  REFERENCES dbo.[PvSystem](PvSystemId),
	DeviceName nvarchar(50) NULL,
	DeviceType nvarchar(50) NULL,
	DeviceManufacturer nvarchar(50) NULL,
	SerialNumber nvarchar(50) NULL,
	DeviceTypeDetails nvarchar(100) NULL,
	DataloggerId nvarchar(100) NULL,
	NodeType int NULL,
	NumberMPPTrackers int NULL,
	NumberPhases int NULL,
	PeakPowerDc1 float(53) NULL,
	PeakPowerDc2 float(53) NULL,
	NominalAcPower float(53) NULL,
	FirmwareUpdateAvailable bit NULL,
	FirmwareInstalledVersion nvarchar(50) NULL,
	FirmwareAvailableVersion nvarchar(50) NULL,
	IsActive bit NOT NULL,
	ActivationDate date NULL,
	DeactivationDate date NULL,
	Capacity int NULL,
	SensorName nvarchar(50) NULL,
	IsOnline bit NULL,
	IpAddressV4 nvarchar(50) NULL,
	CreateDate datetime NOT NULL,
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.[Device] ADD CONSTRAINT
	PK_Device PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[Device] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[Device]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[Device]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[Device]', 'Object', 'CONTROL') as Contr_Per 
