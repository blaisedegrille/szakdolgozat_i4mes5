/*
   2021. március 13., szombat15:06:41
   User: sa
   Server: tigris
   Database: PVS_Monitor
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.[PvSystem]
	(
	Id int NOT NULL IDENTITY(1,1),
	PvSystemId nvarchar(100) NOT NULL,
	CONSTRAINT AK_PvSystemId UNIQUE(PvSystemId),  
	Name nvarchar(100) NOT NULL,
	Description nvarchar(100) NOT NULL,
	PeakPower float(53) NOT NULL,
	Country nvarchar(50) NOT NULL,
	ZipCode nvarchar(50) NOT NULL,
	Street nvarchar(50) NOT NULL,
	City nvarchar(50) NOT NULL,
	Region nvarchar(50) NULL,
	PictureURL nvarchar(400) NULL,
	InstallationDate datetime NULL,
	LastImport datetime NULL,
	MeteoData nvarchar(50) NULL,
	TimeZone nvarchar(50) NULL,
	CreateDate datetime NOT NULL,
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.[PvSystem] ADD CONSTRAINT
	PK_PvSystem PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[PvSystem] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[PvSystem]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[PvSystem]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[PvSystem]', 'Object', 'CONTROL') as Contr_Per 