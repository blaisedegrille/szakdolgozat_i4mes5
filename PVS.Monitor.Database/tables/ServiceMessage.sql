/*
   2021. március 13., szombat14:56:09
   User: sa
   Server: tigris
   Database: PVS_Monitor
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.[ServiceMessage]
	(
	Id int NOT NULL IDENTITY(1,1),
	PvSystemId nvarchar(100) NOT NULL CONSTRAINT FK_ServiceMessage_PvSystemId FOREIGN KEY REFERENCES dbo.[PvSystem](PvSystemId),
	DeviceId nvarchar(100) NOT NULL CONSTRAINT FK_ServiceMessage_DeviceId FOREIGN KEY REFERENCES dbo.[Device](DeviceId),
	StateType nvarchar(50) NOT NULL,
	StateCode int NOT NULL,
	StateSeverity nvarchar(50) NOT NULL,
	LogDateTime nvarchar(50) NULL,
	MessageText nvarchar(400) NULL,
	CreateDate datetime NOT NULL,
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.[ServiceMessage] ADD CONSTRAINT
	PK_ServiceMessage PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[ServiceMessage] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[ServiceMessage]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[ServiceMessage]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[ServiceMessage]', 'Object', 'CONTROL') as Contr_Per 