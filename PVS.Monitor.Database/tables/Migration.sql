/*
   2021. március 13., szombat14:56:09
   User: sa
   Server: tigris
   Database: PVS_Monitor
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.[Migration]
	(
	Id int NOT NULL IDENTITY(1,1),
	Description nvarchar(500) NOT NULL,
	MigrationDate datetime NOT NULL,
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.[Migration] ADD CONSTRAINT
	PK_Migration PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[Migration] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[Migration]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[Migration]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[Migration]', 'Object', 'CONTROL') as Contr_Per 