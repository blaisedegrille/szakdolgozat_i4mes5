/*
   2021. március 13., szombat14:56:09
   User: sa
   Server: tigris
   Database: PVS_Monitor
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.[PvSystemData]
	(
	Id int NOT NULL IDENTITY(1,1),
	PvSystemId nvarchar(100) NOT NULL CONSTRAINT FK_PvSystemData_PvSystemId FOREIGN KEY REFERENCES dbo.[PvSystem](PvSystemId),
	LogDate nvarchar(50) NOT NULL,
	ChannelName nvarchar(50) NOT NULL,
	ChannelType nvarchar(100) NOT NULL,
	Unit nvarchar(20) NOT NULL,
	Value float(53),
	CreateDate datetime NOT NULL,
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.[PvSystemData] ADD CONSTRAINT
	PK_PvSystemData PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[PvSystemData] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.[PvSystemData]', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.[PvSystemData]', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.[PvSystemData]', 'Object', 'CONTROL') as Contr_Per 