﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PVS.Monitor.Entities.Models
{
    public partial class Group
    {
        public Group()
        {
            GroupPvSystem = new HashSet<GroupPvSystem>();
            GroupUser = new HashSet<GroupUser>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<GroupPvSystem> GroupPvSystem { get; set; }
        public virtual ICollection<GroupUser> GroupUser { get; set; }
    }
}
