﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PVS.Monitor.Entities.Models
{
    public partial class Device
    {
        public Device()
        {
            DeviceData = new HashSet<DeviceData>();
            ServiceMessage = new HashSet<ServiceMessage>();
        }

        public int Id { get; set; }
        public string DeviceId { get; set; }
        public string PvSystemId { get; set; }
        public string DeviceName { get; set; }
        public string DeviceType { get; set; }
        public string DeviceManufacturer { get; set; }
        public string SerialNumber { get; set; }
        public string DeviceTypeDetails { get; set; }
        public string DataloggerId { get; set; }
        public int? NodeType { get; set; }
        public int? NumberMpptrackers { get; set; }
        public int? NumberPhases { get; set; }
        public double? PeakPowerDc1 { get; set; }
        public double? PeakPowerDc2 { get; set; }
        public double? NominalAcPower { get; set; }
        public bool? FirmwareUpdateAvailable { get; set; }
        public string FirmwareInstalledVersion { get; set; }
        public string FirmwareAvailableVersion { get; set; }
        public bool IsActive { get; set; }
        public DateTime? ActivationDate { get; set; }
        public DateTime? DeactivationDate { get; set; }
        public int? Capacity { get; set; }
        public string SensorName { get; set; }
        public bool? IsOnline { get; set; }
        public string IpAddressV4 { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual PvSystem PvSystem { get; set; }
        public virtual ICollection<DeviceData> DeviceData { get; set; }
        public virtual ICollection<ServiceMessage> ServiceMessage { get; set; }
    }
}
