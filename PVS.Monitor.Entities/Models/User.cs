﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PVS.Monitor.Entities.Models
{
    public partial class User
    {
        public User()
        {
            GroupUser = new HashSet<GroupUser>();
            RefreshToken = new HashSet<RefreshToken>();
            UserRole = new HashSet<UserRole>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string LoginName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<GroupUser> GroupUser { get; set; }
        public virtual ICollection<RefreshToken> RefreshToken { get; set; }
        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}
