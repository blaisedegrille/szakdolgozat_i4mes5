﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PVS.Monitor.Entities.Models
{
    public partial class GroupPvSystem
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int PvSystemId { get; set; }
        public bool Active { get; set; }

        public virtual Group Group { get; set; }
        public virtual PvSystem PvSystem { get; set; }
    }
}
