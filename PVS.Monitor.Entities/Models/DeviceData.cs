﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PVS.Monitor.Entities.Models
{
    public partial class DeviceData
    {
        public int Id { get; set; }
        public string PvSystemId { get; set; }
        public string DeviceId { get; set; }
        public DateTime LogDate { get; set; }
        public string ChannelName { get; set; }
        public string ChannelType { get; set; }
        public string Unit { get; set; }
        public double? Value { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual Device Device { get; set; }
        public virtual PvSystem PvSystem { get; set; }
    }
}
