﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PVS.Monitor.Entities.Models
{
    public partial class PvSystem
    {
        public PvSystem()
        {
            Device = new HashSet<Device>();
            DeviceData = new HashSet<DeviceData>();
            GroupPvSystem = new HashSet<GroupPvSystem>();
            PvSystemData = new HashSet<PvSystemData>();
            ServiceMessage = new HashSet<ServiceMessage>();
        }

        public int Id { get; set; }
        public string PvSystemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double PeakPower { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PictureUrl { get; set; }
        public DateTime? InstallationDate { get; set; }
        public DateTime? LastImport { get; set; }
        public string MeteoData { get; set; }
        public string TimeZone { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual ICollection<Device> Device { get; set; }
        public virtual ICollection<DeviceData> DeviceData { get; set; }
        public virtual ICollection<GroupPvSystem> GroupPvSystem { get; set; }
        public virtual ICollection<PvSystemData> PvSystemData { get; set; }
        public virtual ICollection<ServiceMessage> ServiceMessage { get; set; }
    }
}
