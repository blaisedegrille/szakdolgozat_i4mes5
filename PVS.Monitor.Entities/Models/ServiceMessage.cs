﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PVS.Monitor.Entities.Models
{
    public partial class ServiceMessage
    {
        public int Id { get; set; }
        public string PvSystemId { get; set; }
        public string DeviceId { get; set; }
        public string StateType { get; set; }
        public int StateCode { get; set; }
        public string StateSeverity { get; set; }
        public string LogDateTime { get; set; }
        public string MessageText { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual Device Device { get; set; }
        public virtual PvSystem PvSystem { get; set; }
    }
}
