﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PVS.Monitor.Entities.Models
{
    public partial class PVS_MonitorContext : DbContext
    {
        public PVS_MonitorContext()
        {
        }

        public PVS_MonitorContext(DbContextOptions<PVS_MonitorContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Device> Device { get; set; }
        public virtual DbSet<DeviceData> DeviceData { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<GroupPvSystem> GroupPvSystem { get; set; }
        public virtual DbSet<GroupUser> GroupUser { get; set; }
        public virtual DbSet<Migration> Migration { get; set; }
        public virtual DbSet<PvSystem> PvSystem { get; set; }
        public virtual DbSet<PvSystemData> PvSystemData { get; set; }
        public virtual DbSet<RefreshToken> RefreshToken { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<ServiceMessage> ServiceMessage { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=PVS_Monitor;Integrated Security=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Device>(entity =>
            {
                entity.HasIndex(e => e.DeviceId)
                    .HasName("AK_DeviceId")
                    .IsUnique();

                entity.Property(e => e.ActivationDate).HasColumnType("date");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DataloggerId).HasMaxLength(100);

                entity.Property(e => e.DeactivationDate).HasColumnType("date");

                entity.Property(e => e.DeviceId)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.DeviceManufacturer).HasMaxLength(50);

                entity.Property(e => e.DeviceName).HasMaxLength(50);

                entity.Property(e => e.DeviceType).HasMaxLength(50);

                entity.Property(e => e.DeviceTypeDetails).HasMaxLength(100);

                entity.Property(e => e.FirmwareAvailableVersion).HasMaxLength(50);

                entity.Property(e => e.FirmwareInstalledVersion).HasMaxLength(50);

                entity.Property(e => e.IpAddressV4).HasMaxLength(50);

                entity.Property(e => e.NumberMpptrackers).HasColumnName("NumberMPPTrackers");

                entity.Property(e => e.PvSystemId)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.SensorName).HasMaxLength(50);

                entity.Property(e => e.SerialNumber).HasMaxLength(50);

                entity.HasOne(d => d.PvSystem)
                    .WithMany(p => p.Device)
                    .HasPrincipalKey(p => p.PvSystemId)
                    .HasForeignKey(d => d.PvSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Device_PvSystemId");
            });

            modelBuilder.Entity<DeviceData>(entity =>
            {
                entity.Property(e => e.ChannelName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ChannelType)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DeviceId).HasMaxLength(100);

                entity.Property(e => e.LogDate).HasColumnType("date");

                entity.Property(e => e.PvSystemId)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Unit)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.DeviceData)
                    .HasPrincipalKey(p => p.DeviceId)
                    .HasForeignKey(d => d.DeviceId)
                    .HasConstraintName("FK_DeviceData_DeviceId");

                entity.HasOne(d => d.PvSystem)
                    .WithMany(p => p.DeviceData)
                    .HasPrincipalKey(p => p.PvSystemId)
                    .HasForeignKey(d => d.PvSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DeviceData_PvSystemId");
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<GroupPvSystem>(entity =>
            {
                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupPvSystem)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupPvSystem_GroupId");

                entity.HasOne(d => d.PvSystem)
                    .WithMany(p => p.GroupPvSystem)
                    .HasForeignKey(d => d.PvSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupPvSystem_PvSystemId");
            });

            modelBuilder.Entity<GroupUser>(entity =>
            {
                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupUser)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupUser_GroupId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.GroupUser)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupUser_UserId");
            });

            modelBuilder.Entity<Migration>(entity =>
            {
                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.MigrationDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<PvSystem>(entity =>
            {
                entity.HasIndex(e => e.PvSystemId)
                    .HasName("AK_PvSystemId")
                    .IsUnique();

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.InstallationDate).HasColumnType("datetime");

                entity.Property(e => e.LastImport).HasColumnType("datetime");

                entity.Property(e => e.MeteoData).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PictureUrl)
                    .HasColumnName("PictureURL")
                    .HasMaxLength(400);

                entity.Property(e => e.PvSystemId)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Region).HasMaxLength(50);

                entity.Property(e => e.Street)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.TimeZone).HasMaxLength(50);

                entity.Property(e => e.ZipCode)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PvSystemData>(entity =>
            {
                entity.Property(e => e.ChannelName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ChannelType)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.LogDate)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PvSystemId)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Unit)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.PvSystem)
                    .WithMany(p => p.PvSystemData)
                    .HasPrincipalKey(p => p.PvSystemId)
                    .HasForeignKey(d => d.PvSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PvSystemData_PvSystemId");
            });

            modelBuilder.Entity<RefreshToken>(entity =>
            {
                entity.Property(e => e.RefreshTokenExpiration).HasColumnType("datetime");

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.RefreshToken)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RefreshToken_UserId");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<ServiceMessage>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DeviceId)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LogDateTime).HasMaxLength(50);

                entity.Property(e => e.MessageText).HasMaxLength(400);

                entity.Property(e => e.PvSystemId)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.StateSeverity)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StateType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.ServiceMessage)
                    .HasPrincipalKey(p => p.DeviceId)
                    .HasForeignKey(d => d.DeviceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ServiceMessage_DeviceId");

                entity.HasOne(d => d.PvSystem)
                    .WithMany(p => p.ServiceMessage)
                    .HasPrincipalKey(p => p.PvSystemId)
                    .HasForeignKey(d => d.PvSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ServiceMessage_PvSystemId");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.Phone).HasMaxLength(30);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserRole_RoleId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserRole_UserId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
