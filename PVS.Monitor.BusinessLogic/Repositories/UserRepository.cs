﻿using Microsoft.EntityFrameworkCore;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Base;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Repositories.Interfaces
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        protected new readonly DbSet<User> dbSet;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public UserRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            dbSet = this.unitOfWork.DbContext.Set<User>();
        }
        private List<string> SetRoles(ICollection<UserRole> userRoles)
        {
            List<string> roles = new List<string>();

            foreach (var role in userRoles)
            {
                roles.Add(role.Role.Name);
            }

            return roles;
        }

        public IEnumerable<UserViewModel> GetUsersWithRoles()
        {
            List<UserViewModel> userVmList = new List<UserViewModel>();
            var userIdList = dbSet.Where(w => w.Active).Select(s => s.Id);

            foreach (var userId in userIdList)
            {
                UserViewModel userVm = null;
                var user = dbSet
                .Where(x => x.Active && x.Id == userId)
                .Include(x => x.UserRole)
                .ThenInclude(x => x.Role)
                .FirstOrDefault();
                userVm = new UserViewModel
                {
                    Id = user.Id,
                    Name = user.Name,
                    LoginName = user.LoginName,
                    Email = user.Email,
                    Roles = SetRoles(user.UserRole)
                };
                userVmList.Add(userVm);
            }
            return userVmList;
        }

        public UserViewModel GetUserWithRolesByUserName(string username)
        {
            var user = dbSet
                .Where(x => x.LoginName == username && x.Active)
                .Include(x => x.UserRole)
                .ThenInclude(x => x.Role)
                .FirstOrDefault();

            UserViewModel userView = null;

            if (user != null)
            {
                userView = new UserViewModel();
                userView.Id = user.Id;
                userView.LoginName = user.LoginName;
                userView.Name = user.Name;
                userView.LoginName = user.LoginName;
                userView.Email = user.Email;
                userView.Roles = SetRoles(user.UserRole);
            }

            return userView;
        }

        public User GetUserByUserName(string username)
        {
            var user = dbSet
                .Where(x => x.LoginName == username && x.Active)
                .FirstOrDefault();
            return user;
        }

        public User GetGroupUser(int id)
        {
            return dbSet.Include(e => e.UserRole).ThenInclude(g => g.User).Where(e => e.Id == id).FirstOrDefault();
        }

        public IEnumerable<GroupUserViewModel> GetGroupUsersByList(IEnumerable<GroupUserViewModel> model)
        {
            List<GroupUserViewModel> list = new List<GroupUserViewModel>();
            var list2 = dbSet.Where(x => x.Active).ToList();

            foreach (var user in list2)
            {
                GroupUserViewModel guModel = null;
                guModel = new GroupUserViewModel();
                guModel.UserId = user.Id;
                guModel.Name = user.Name;
                guModel.LoginName = user.LoginName;
                guModel.Email = user.Email;
                guModel.Roles = SetRoles(user.UserRole);
                if (model.Any(x => x.UserId == user.Id))
                {
                    guModel.Selected = true;
                }
                else
                {
                    guModel.Selected = false;
                }
                list.Add(guModel);
            }
            return list.OrderBy(x => x.Name).OrderByDescending(x => x.Selected);
        }

    }
}
