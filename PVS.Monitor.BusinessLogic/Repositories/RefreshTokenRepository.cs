﻿using Microsoft.EntityFrameworkCore;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Base;
using PVS.Monitor.Entities.Models;
using System.Linq;
using System.Threading.Tasks;

namespace PVS.Monitor.BusinessLogic.Repositories.Interfaces
{
    public class RefreshTokenRepository : RepositoryBase<RefreshToken>, IRefreshTokenRepository
    {
        protected new readonly DbSet<RefreshToken> dbSet;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public RefreshTokenRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            dbSet = this.unitOfWork.DbContext.Set<RefreshToken>();
        }

        public Task<RefreshToken> GetByToken(string _refreshToken)
        {
            RefreshToken refreshToken = dbSet.FirstOrDefault(r => r.Token == _refreshToken);

            return Task.FromResult(refreshToken);
        }

    }
}
