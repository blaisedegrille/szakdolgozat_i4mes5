﻿using Microsoft.EntityFrameworkCore;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Base;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PVS.Monitor.BusinessLogic.Repositories.Interfaces
{
    public class PvSystemDataRepository : RepositoryBase<PvSystemData>, IPvSystemDataRepository
    {
        protected new readonly DbSet<PvSystemData> dbSet;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public PvSystemDataRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            dbSet = this.unitOfWork.DbContext.Set<PvSystemData>();
        }
    }
}
