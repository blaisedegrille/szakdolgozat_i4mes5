﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PVS.Monitor.BusinessLogic.Repositories.Base
{
    public interface IRepositoryBase<T> where T : class
    {
        T Add(T entity, string identity = null, bool fillUserDate = true);
        T Update(T entity, string identity = null);
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> where);
        T GetById(long id);
        T GetById(int id);
        T Get(Expression<Func<T, bool>> where);
        IEnumerable<T> GetAll();
        IQueryable<T> GetAllAsQueryable();
        IQueryable<T> GetManyAsQueryable(Expression<Func<T, bool>> where);
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where);
        Task<IEnumerable<T>> GetManyAsync(Expression<Func<T, bool>> where);
        IEnumerable<T> GetManyOrderBy<TOrderBy>(Expression<Func<T, bool>> where, Expression<Func<T, TOrderBy>> orderBy);
        IEnumerable<T> GetManyOrderByDescending<TOrderByDescending>(Expression<Func<T, bool>> where, Expression<Func<T, TOrderByDescending>> orderByDescending);
        IEnumerable<T> GetMany(List<Expression<Func<T, bool>>> where);
        T CreateInstance();
    }
}
