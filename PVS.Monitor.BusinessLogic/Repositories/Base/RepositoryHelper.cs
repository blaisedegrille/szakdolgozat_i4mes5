﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Repositories.Base
{
    /// <summary>
    /// Helper class for repositories
    /// </summary>
    public static class RepositoryHelper
    {
        /// <summary>
        /// identfier of the currentUser
        /// </summary>
        public static string userName { set; get; } = "";

    }
}
