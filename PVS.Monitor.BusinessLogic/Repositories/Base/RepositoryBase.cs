﻿using Microsoft.EntityFrameworkCore;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PVS.Monitor.BusinessLogic.Repositories.Base
{
    /// <summary>
    /// Base class for repositories
    /// </summary>
    /// <typeparam name="T">entity type</typeparam>
    public class RepositoryBase<T> where T : class
    {
        protected readonly DbSet<T> dbSet;
        protected IUnitOfWork unitOfWork;


        /// <summary>
        /// For setting entity properties (Cru, Lmu, Crd, Lmd) if the property exists, and its value is null
        /// </summary>
        /// <param name="entity">entity whose property has to be set</param>
        /// <param name="name">name of the property</param>
        /// <param name="value">value of the property</param>
        protected void SetProperty(T entity, String name, Object value)
        {
            var propertyInfo = entity.GetType().GetProperty(name);
            if (propertyInfo != null)
            {
                // dátum típus esetén alapraméretezett érték esetén kell értéket adni.
                if (propertyInfo.PropertyType.Equals(typeof(DateTime)))
                {
                    if (propertyInfo.GetValue(entity).Equals(default(DateTime)) || propertyInfo.Name == "Lmd")
                    {
                        propertyInfo.SetValue(entity, value);
                    }
                }
                // minden más típus esetén null érték esetén kell értéket adni.
                else if (propertyInfo.GetValue(entity) == null)
                {
                    propertyInfo.SetValue(entity, value);
                }
                //a módosítót azért adjuk tovább
                else if (name == "Lmu")
                {
                    propertyInfo.SetValue(entity, value);
                }
            }
        }

        /// <summary>
        /// Constructor, implements the unit of work pattern
        /// </summary>
        /// <param name="unitOfWork">unit of work</param>
        protected RepositoryBase(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            dbSet = this.unitOfWork.DbContext.Set<T>();
        }

        public T CreateInstance()
        {
            T entity = (T)Activator.CreateInstance(typeof(T));
            SetProperty(entity, "Cru", "");
            SetProperty(entity, "Crd", DateTime.Now);
            SetProperty(entity, "Lmu", "");
            SetProperty(entity, "Lmd", DateTime.Now);
            return entity;
        }

        /// <summary>
        /// Creates a new entity
        /// </summary>
        /// <param name="entity">the new entity</param>
        /// <param name="identity">identity of the creator (optional)</param>
        /// <param name="fillUserDate">fill Cru, Crd, Lmu, Lmd fields</param>
        /// <returns>the saved entity</returns>
        public virtual T Add(T entity, string identity = "racsbalazs", bool fillUserDate = true)
        {
            if (fillUserDate)
            {
                SetProperty(entity, "Cru", identity);
                SetProperty(entity, "Crd", DateTime.Now);
                SetProperty(entity, "Lmu", identity);
                SetProperty(entity, "Lmd", DateTime.Now);
            }
            dbSet.Add(entity);
            return entity;
        }

        /// <summary>
        /// Saves the updated entity
        /// </summary>
        /// <param name="entity">the updated entity</param>
        /// <param name="identity">identity of the modifier (optional)</param>
        /// <returns>the saved entity</returns>
        public virtual T Update(T entity, string identity = null)
        {
            SetProperty(entity, "Lmu", identity);
            SetProperty(entity, "Lmd", DateTime.Now);

            dbSet.Attach(entity);
            this.unitOfWork.DbContext.Entry(entity).State = EntityState.Modified;

            return entity;
        }

        /// <summary>
        /// Removes an entity
        /// </summary>
        /// <param name="entity">the entity for removing</param>
        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

        /// <summary>
        /// Removes a set of entities
        /// </summary>
        /// <param name="where">condition</param>
        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = dbSet.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                dbSet.Remove(obj);
        }

        /// <summary>
        /// Returns an entity by its id
        /// </summary>
        /// <param name="id">id of the entity</param>
        /// <returns>entity</returns>
        public virtual T GetById(long id)
        {
            return dbSet.Find(id);
        }

        /// <summary>
        /// Returns an entity by its id
        /// </summary>
        /// <param name="id">id of the entity</param>
        /// <returns>entity</returns>
        public virtual T GetById(int id)
        {
            return dbSet.Find(id);
        }

        /// <summary>
        /// Returns an entity by a condition
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public T Get(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).FirstOrDefault();
        }

        /// <summary>
        /// Returns all entities
        /// </summary>
        /// <returns>list of entities</returns>
        public virtual IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
        }

        /// <summary>
        /// Returns all entities as queryable
        /// </summary>
        /// <returns>queryable of entities</returns>
        public virtual IQueryable<T> GetAllAsQueryable()
        {
            return dbSet.AsQueryable();
        }

        /// <summary>
        /// Returns all entities as queryable
        /// </summary>
        /// <returns>queryable of entities</returns>
        public virtual IQueryable<T> GetManyAsQueryable(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where);
        }

        /// <summary>
        /// Returns a set of entities
        /// </summary>
        /// <param name="where">condition</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).ToList();
        }

        /// <summary>
        /// Returns a set of entities
        /// </summary>
        /// <param name="where">condition</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetManyAsync(Expression<Func<T, bool>> where)
        {
            return await dbSet.Where(where).ToListAsync();
        }

        /// <summary>
        /// Returns an ordered set of entities
        /// </summary>
        /// <typeparam name="TOrderBy"></typeparam>
        /// <param name="where"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetManyOrderBy<TOrderBy>(Expression<Func<T, bool>> where, Expression<Func<T, TOrderBy>> orderBy)
        {
            return dbSet.Where(where).OrderBy(orderBy).ToList();
        }

        /// <summary>
        /// Returns an ordered set of entities
        /// </summary>
        /// <typeparam name="TOrderByDescending"></typeparam>
        /// <param name="where"></param>
        /// <param name="orderByDescending"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetManyOrderByDescending<TOrderByDescending>(Expression<Func<T, bool>> where, Expression<Func<T, TOrderByDescending>> orderByDescending)
        {
            return dbSet.Where(where).OrderByDescending(orderByDescending).ToList();
        }

        /// <summary>
        /// Returns a set of entities
        /// </summary>
        /// <param name="whereList">conditions</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetMany(List<Expression<Func<T, bool>>> where)
        {
            IQueryable<T> filteredDbSet;
            filteredDbSet = dbSet.AsQueryable();
            foreach (var whereItem in where)
                filteredDbSet = filteredDbSet.Where(whereItem);

            return filteredDbSet.ToList();
        }

        /// <summary>
        /// Returns the current user.
        /// </summary>
        /// <returns></returns>
        private string GetCurrentUser()
        {
            return RepositoryHelper.userName;
        }
    }
}