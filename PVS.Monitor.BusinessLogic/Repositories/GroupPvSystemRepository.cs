﻿using Microsoft.EntityFrameworkCore;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Base;
using PVS.Monitor.Entities.Models;

namespace PVS.Monitor.BusinessLogic.Repositories.Interfaces
{
    public class GroupPvSystemRepository : RepositoryBase<GroupPvSystem>, IGroupPvSystemRepository
    {
        protected new readonly DbSet<GroupPvSystem> dbSet;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public GroupPvSystemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            dbSet = this.unitOfWork.DbContext.Set<GroupPvSystem>();
        }
    }
}
