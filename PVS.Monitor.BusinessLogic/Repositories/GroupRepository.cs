﻿using Microsoft.EntityFrameworkCore;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Base;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Repositories.Interfaces
{
    public class GroupRepository : RepositoryBase<Group>, IGroupRepository
    {
        protected new readonly DbSet<Group> dbSet;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public GroupRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            dbSet = this.unitOfWork.DbContext.Set<Group>();
        }
    }
}
