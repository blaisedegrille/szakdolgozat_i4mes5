﻿using Microsoft.EntityFrameworkCore;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Base;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Repositories.Interfaces
{
    public class RoleRepository : RepositoryBase<Role>, IRoleRepository
    {
        protected new readonly DbSet<Role> dbSet;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public RoleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            dbSet = this.unitOfWork.DbContext.Set<Role>();
        }
        
    }
}
