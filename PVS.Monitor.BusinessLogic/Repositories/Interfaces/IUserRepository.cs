﻿using PVS.Monitor.BusinessLogic.Repositories.Base;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PVS.Monitor.BusinessLogic.Repositories.Interfaces
{
    public partial interface IUserRepository : IRepositoryBase<User>
    {
        public User GetUserByUserName(string username);

        public IEnumerable<UserViewModel> GetUsersWithRoles();

        UserViewModel GetUserWithRolesByUserName(string email);

        User GetGroupUser(int id);

        IEnumerable<GroupUserViewModel> GetGroupUsersByList(IEnumerable<GroupUserViewModel> model);
    }
}
