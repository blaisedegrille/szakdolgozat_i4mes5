﻿using PVS.Monitor.BusinessLogic.Repositories.Base;
using PVS.Monitor.Entities.Models;
using System.Threading.Tasks;

namespace PVS.Monitor.BusinessLogic.Repositories.Interfaces
{
    public partial interface IRefreshTokenRepository : IRepositoryBase<RefreshToken>
    {
        Task<RefreshToken> GetByToken(string _refreshToken);
    }
}
