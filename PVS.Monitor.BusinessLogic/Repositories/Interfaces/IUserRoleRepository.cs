﻿using PVS.Monitor.BusinessLogic.Repositories.Base;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Repositories.Interfaces
{
    public partial interface IUserRoleRepository : IRepositoryBase<UserRole>
    {
    }
}
