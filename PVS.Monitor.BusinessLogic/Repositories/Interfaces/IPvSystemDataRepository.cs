﻿using PVS.Monitor.BusinessLogic.Repositories.Base;
using PVS.Monitor.Entities.Models;

namespace PVS.Monitor.BusinessLogic.Repositories.Interfaces
{
    public partial interface IPvSystemDataRepository : IRepositoryBase<PvSystemData>
    {
    }
}
