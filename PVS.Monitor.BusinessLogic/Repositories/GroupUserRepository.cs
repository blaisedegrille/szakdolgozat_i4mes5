﻿using Microsoft.EntityFrameworkCore;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Base;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Repositories.Interfaces
{
    public class GroupUserRepository : RepositoryBase<GroupUser>, IGroupUserRepository
    {
        protected new readonly DbSet<GroupUser> dbSet;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork"></param>
        public GroupUserRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            dbSet = this.unitOfWork.DbContext.Set<GroupUser>();
        }

        public IEnumerable<GroupUser> GetByGroupId(int groupId)
        {
            return dbSet.Where(x => x.GroupId == groupId && x.Active).ToList();
        }
    }
}
