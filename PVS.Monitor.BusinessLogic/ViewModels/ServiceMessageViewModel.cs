﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class ServiceMessageViewModel
    {
        public int Id { get; set; }
        public string PvSystemId { get; set; }
        public string PvSystemName { get; set; }
        public string DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string DeviceType { get; set; }
        public string StateType { get; set; }
        public int StateCode { get; set; }
        public string StateSeverity { get; set; }
        public string LogDateTime { get; set; }
        public string MessageText { get; set; }
    }
}
