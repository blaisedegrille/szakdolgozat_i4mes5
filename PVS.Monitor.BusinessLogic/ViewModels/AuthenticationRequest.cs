﻿using System.ComponentModel.DataAnnotations;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class AuthenticationRequest
    {
        [Required]
        public string LoginName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
