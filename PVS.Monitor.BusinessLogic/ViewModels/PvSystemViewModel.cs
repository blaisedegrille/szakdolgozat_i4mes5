﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class PvSystemViewModel
    {
        public int Id { get; set; }
        public string PvSystemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double PeakPower { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PictureUrl { get; set; }
        public DateTime? InstallationDate { get; set; }
        public DateTime? LastImport { get; set; }
        public string MeteoData { get; set; }
        public string TimeZone { get; set; }
        public string Address { get; set; }
    }
}
