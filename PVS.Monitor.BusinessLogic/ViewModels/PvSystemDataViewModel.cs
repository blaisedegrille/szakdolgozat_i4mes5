﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class PvSystemDataViewModel
    {
        public string PvSystemId { get; set; }
        public string PvSystemName { get; set; }
        public string Unit { get; set; }
        public double? Value { get; set; }
        public string Year { get; set; }
    }
}
