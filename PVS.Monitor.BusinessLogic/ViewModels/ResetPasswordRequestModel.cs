﻿using System.ComponentModel.DataAnnotations;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class ResetPasswordRequestModel
    {
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public string Token { get; set; }
    }
}

