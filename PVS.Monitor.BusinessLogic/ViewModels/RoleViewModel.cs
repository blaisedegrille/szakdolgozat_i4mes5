﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class RoleViewModel
    {
        public int Id { get; set; }
        public string RoleName { get; set; }

    }
}
