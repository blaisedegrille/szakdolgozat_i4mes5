﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class UserRoleViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
