﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class PvSystemDataMigrationViewModel
    {
        public string PvSystemId { get; set; }
        public IEnumerable<PvSystemLogDataItem> Data { get; set; }
    }
    
    public class PvSystemLogDataItem
    {
        public string LogDateTime { get; set; }
        public IEnumerable<PvSystemChannelItem> Channels { get; set; }

    }

    public class PvSystemChannelItem {
        public string ChannelName { get; set; }
        public string ChannelType { get; set; }
        public string Unit { get; set; }
        public double? Value { get; set; }
    }
}
