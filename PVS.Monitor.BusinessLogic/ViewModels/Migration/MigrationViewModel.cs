﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class MigrationViewModel {
        public string Description { get; set; }
        public DateTime MigrationDate { get; set; }
    }
}
