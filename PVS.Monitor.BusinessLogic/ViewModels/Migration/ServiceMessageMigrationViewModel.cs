﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class ServiceMessageMigrationViewModel { 
        public IEnumerable<ServiceMessageItem> Messages { get; set; }
    }
    
    public class ServiceMessageItem
    {
        public string PvSystemId { get; set; }
        public string DeviceId { get; set; }
        public string StateType { get; set; }
        public int StateCode { get; set; }
        public string StateSeverity { get; set; }
        public string LogDateTime { get; set; }
        public string Text  { get; set; }
    }
}
