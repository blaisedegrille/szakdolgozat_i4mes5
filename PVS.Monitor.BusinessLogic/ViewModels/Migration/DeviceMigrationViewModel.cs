﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
	public class DeviceMigrationViewModel
	{
		public IEnumerable<DeviceItem> Devices { get; set; }
	}
	
	public class DeviceItem
	{
		public string DeviceId { get; set; }
		public string? PvSystemId { get; set; }
		public string DeviceName { get; set; }
		public string DeviceType { get; set; }
		public string DeviceManufacturer { get; set; }
		public string? DeviceTypeDetails { get; set; }
		public string? SerialNumber { get; set; }
		public string? DataloggerId { get; set; }
		public int? NodeType { get; set; }
		public int? NumberMPPTrackers { get; set; }
		public int? NumberPhases { get; set; }
		public PeakPower? PeakPower { get; set; }
		public float? NominalAcPower { get; set; }
		public Firmware? Firmware { get; set; }
		public bool IsActive { get; set; }
		public DateTime? ActivationDate { get; set; }
		public DateTime? DeactivationDate { get; set; }
		public int? Capacity { get; set; }
		public IEnumerable<Sensor>? Sensors { get; set; }
		public bool? IsOnline { get; set; }
		public string? IpAddressV4 { get; set; }
	}

    public class PeakPower {
        public float? Dc1 { get; set; }
        public float? Dc2 { get; set; }
    }
	public class Firmware
	{
		public bool? UpdateAvailable { get; set; }
		public string? InstalledVersion { get; set; }
		public string? AvailableVersion { get; set; }
	}

	public class Sensor
	{
		public string? SensorName { get; set; }
		public bool? IsActive { get; set; }
		public DateTime? ActivationDate { get; set; }
		public DateTime? DeactivationDate { get; set; }
	}
}
