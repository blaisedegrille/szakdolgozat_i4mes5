﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class DeviceDataMigrationViewModel {
        public string PvSystemId { get; set; }
        public string? DeviceId{ get; set; }
        public IEnumerable<DeviceChannelLogDataItem> Data { get; set; }
    }
    
    public class DeviceChannelLogDataItem
    {
        public DateTime LogDateTime { get; set; }
        public IEnumerable<DeviceChannelItem> Channels { get; set; }

    }

    public class DeviceChannelItem
    {
        public string ChannelName { get; set; }
        public string ChannelType { get; set; }
        public string Unit { get; set; }
        public double? Value { get; set; }
    }
}
