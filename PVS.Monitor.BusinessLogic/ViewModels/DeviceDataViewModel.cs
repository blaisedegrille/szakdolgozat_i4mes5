﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class DeviceDataViewModel
    {
        public string PvSystemId { get; set; }
        public string PvSystemName { get; set; }
        public string DeviceId { get; set; }
        public string DeviceName { get; set; }
        public DateTime LogDate { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public double? Value { get; set; }
    }
}
