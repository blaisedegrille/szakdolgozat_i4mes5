﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class PvSystemDropDownViewModel
    {
        public int Id { get; set; }
        public string PvSystemId { get; set; }
        public string Name { get; set; }

    }
}
