﻿using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.ViewModels
{
    public class EditUserViewModel
    {
        public int Id { get; set; }
        public string? Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string LoginName { get; set; }
        public List<RoleViewModel> Roles { get; set; }
        public bool Active { get; set; }

        //public string Token { get; set; }
        //public string RefreshToken { get; set; }
        //public DateTime RefreshTokenExpiration { get; set; }
    }
}
