﻿using PVS.Monitor.Entities.Models;
using System;
using System.Threading.Tasks;

namespace PVS.Monitor.BusinessLogic.Infrastructure.Interfaces
{
    /// <summary>
    /// A Repository és a Unity of Work pattern megvalósítához szükséges interface.
    /// Osztott context biztosítása a repository-k számára
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        PVS_MonitorContext DbContext { get; }

        void Commit();

        Task<int> CommitAsync();
    }
}
