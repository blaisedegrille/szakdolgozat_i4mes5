﻿namespace PVS.Monitor.BusinessLogic.Infrastructure
{
    /// <summary>
    /// Szervíz funkció futásának eredménye
    /// </summary>
    public class CommandResult
    {
        /// <summary>
        /// The inserted id if is there any
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// If the command was successfully 
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Hibaüzenet
        /// </summary>
        public string ErrorCode { get; set; }
        /// <summary>
        /// Hibaüzenet
        /// </summary>
        public string Message { get; set; }
    }
}
