﻿using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.Entities.Models;
using System;
using System.Threading.Tasks;

namespace PVS.Monitor.BusinessLogic.Infrastructure
{
    /// <summary>
    /// A Repository és a Unity of Work pattern megvalósítához szükséges osztály.
    /// Osztott context biztosítása a repository-k számára
    /// </summary>
    public sealed class UnitOfWork : IUnitOfWork
    {
        private PVS_MonitorContext dbContext;

        public UnitOfWork(PVS_MonitorContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public PVS_MonitorContext DbContext
        {
            get
            {
                return dbContext;
            }
        }

        //   Ha validationerrors-t kapsz, ezt kérdezd le...
        //  ((System.Data.Entity.Validation.DbEntityValidationException)$exception).EntityValidationErrors
        //
        public void Commit()
        {
            dbContext.SaveChanges();
        }

        /// <summary>
        /// Saves changes to context
        /// </summary>
        public async Task<int> CommitAsync()
        {
            return await dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            if (dbContext != null) dbContext.Dispose();
            GC.SuppressFinalize(this);
        }
    }

}
