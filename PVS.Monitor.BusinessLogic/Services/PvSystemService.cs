﻿using Coop.NFR.BusinessLogic.Services.Base;
using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Interfaces;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PVS.Monitor.BusinessLogic.Services
{
    public class PvSystemService : ServiceBase, IPvSystemService
    {

        /// <summary>
        /// PvSystem adatokat kezelő repository
        /// </summary>
        private readonly IPvSystemRepository pvSystemRepository;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork">Unity of Work pattern megvalósítához szükséges interface</param>
        /// <param name="pvSystemRepository">PvSystem adatokat kezelő repository</param>
        public PvSystemService(IUnitOfWork unitOfWork, IPvSystemRepository pvSystemRepository)
            : base(unitOfWork)
        {
            this.pvSystemRepository = pvSystemRepository;
        }

        public PvSystemViewModel GetPvSystem(int id)
        {
            var pvSystem = pvSystemRepository.GetById(id);
            if (pvSystem == null)
            {
                throw new KeyNotFoundException("group not found");
            }
            return new PvSystemViewModel()
            {
                Id = pvSystem.Id,
                Name = pvSystem.Name,
                Description = pvSystem.Description,
                Address = pvSystem.ZipCode + " " + pvSystem.City + " " + pvSystem.Street
            };
        }

        public PvSystemViewModel GetPvSystemById(int id)
        {
            var pvSytsem = pvSystemRepository.GetById(id);
            PvSystemViewModel pvVM;
            pvVM = new PvSystemViewModel() {
                 Id = pvSytsem.Id,
                 Name = pvSytsem.Name,
                 PeakPower = pvSytsem.PeakPower,
                 Country = pvSytsem.Country,
                 City = pvSytsem.City,
                 State = pvSytsem.Region,
                 ZipCode = pvSytsem.ZipCode,
                 Street = pvSytsem.Street
            };
            return pvVM;
        }

        public IEnumerable<PvSystemViewModel> GetAllPvSystems()
        {
            //new Uri
            var all = pvSystemRepository.GetAllAsQueryable()
             .Select(s => new PvSystemViewModel()
             {
                 Id = s.Id,
                 Name = s.Name,
                 PeakPower = s.PeakPower,
                 Country = s.Country,
                 City = s.City,
                 State = s.Region,
                 ZipCode = s.ZipCode,
                 Street = s.Street,
                 PictureUrl = s.PictureUrl,
                 InstallationDate = s.InstallationDate,
                 LastImport = s.LastImport,
                 MeteoData = s.MeteoData,
                 Address = s.ZipCode + " " + s.City + " " + s.Street,
                 TimeZone = s.TimeZone,
                 Description = s.Description,
                 PvSystemId = s.PvSystemId
                 
             }).OrderBy(x => x.Name);
            return all;
        }

        public IEnumerable<PvSystemDropDownViewModel> GetPvSystemDropDown()
        {
            var all = pvSystemRepository.GetAllAsQueryable()
             .Select(s => new PvSystemDropDownViewModel()
             {
                 Id = s.Id,
                 Name = s.Name,
                 PvSystemId = s.PvSystemId

             }).OrderBy(x => x.Name);
            return all;
        }

        public CommandResult UpdatePvSystem(PvSystemViewModel model)
        {
            throw new NotImplementedException();
        }

        public CommandResult CreatePvSystem(PvSystemViewModel model)
        {
            var error = Validation(model);
            if (!string.IsNullOrEmpty(error))
            {
                return new CommandResult() { Message = error };
            }
            var pvSystem = new PvSystem() {
                PvSystemId = model.PvSystemId,
                Name = model.Name, 
                Description = model.Description,
                PeakPower = model.PeakPower,
                Country = model.Country,
                ZipCode = model.ZipCode,
                Street = model.Street,
                City = model.City,
                Region = model.State,
                PictureUrl = model.PictureUrl,
                InstallationDate = model.InstallationDate,
                LastImport = model.LastImport,
                MeteoData = model.MeteoData,
                TimeZone = model.TimeZone
            };
            pvSystemRepository.Add(pvSystem);
            unitOfWork.Commit();
            return new CommandResult() { IsSuccess = true, Id = pvSystem.Id };
        }
        public CommandResult CreatePvSystemsFromList(IEnumerable<PvSystemMigrationViewModel> model)
        {
            foreach (var pvSystem in model)
            {
                /*
                var system = new PvSystem()
                {
                    Swid = pvSystem.PvSystemId,
                    Name = pvSystem.Name,
                    Description = pvSystem.Name,
                    PeakPower = pvSystem.PeakPower,
                    Country = pvSystem.Address.Country,
                    ZipCode = pvSystem.Address.ZipCode,
                    Street = pvSystem.Address.Street,
                    City = pvSystem.Address.City,
                    Region = pvSystem.Address.State,
                    PictureUrl = pvSystem.PictureUrl,
                    InstallationDate = pvSystem.InstallationDate,
                    LastImport = pvSystem.LastImport,
                    MeteoData = pvSystem.MeteoData,
                    TimeZone = pvSystem.TimeZone
                };
                
                pvSystemRepository.Add(system);
                unitOfWork.Commit();
                */
            }
            return new CommandResult() { IsSuccess = true };

        }
        private string Validation(PvSystemViewModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                return "plant.validation.name.required";
            }
            if (model.Name.Length > 200)
            {
                return "plant.validation.name.maxlength";
            }
            if (pvSystemRepository.GetAllAsQueryable().Any(a => a.Id != model.Id && a.Name.ToLower() == model.Name.ToLower()))
            {
                return "plant.validation.name.unique";
            }
            return string.Empty;
        }

    }
}

/*
         public int Id { get; set; }
        public string Swid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PeakPower { get; set; }
        public string Country { get; set; }
        public int PostalCode { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PictureUrl { get; set; }
        public DateTime? InstalationDate { get; set; }
        public DateTime? LastDataImport { get; set; }
        public string MeteoData { get; set; }
        public string TimeZone { get; set; }
 */