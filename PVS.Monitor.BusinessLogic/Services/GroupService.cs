﻿using Coop.NFR.BusinessLogic.Services.Base;
using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Interfaces;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Services
{
    public class GroupService : ServiceBase, IGroupService
    {
        /// <summary>
        /// Csoport adatokat kezelő repository
        /// </summary>
        private readonly IGroupRepository groupRepository;

        /// <summary>
        /// Felhasználó - csoport adatokat kezelő repository
        /// </summary>
        private readonly IGroupUserRepository groupUserRepository;

        /// <summary>
        /// Kiserőmű - csoport adatokat kezelő repository
        /// </summary>
        private readonly IGroupPvSystemRepository groupPvSystemRepository;

        /// <summary>
        /// Felhasználó - csoport adatokat kezelő repository
        /// </summary>
        private readonly IUserRepository userRepository;

        /// <summary>
        /// Kiserőmű - csoport adatokat kezelő repository
        /// </summary>
        private readonly IPvSystemRepository pvSystemRepository;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork">Unity of Work pattern megvalósítához szükséges interface</param>
        /// <param name="groupRepository">Csoport adatokat kezelő repository</param>
        public GroupService(
            IUnitOfWork unitOfWork, 
            IGroupRepository groupRepository, 
            IGroupUserRepository groupUserRepository, 
            IGroupPvSystemRepository groupPvSystemRepository, 
            IUserRepository userRepository, 
            IPvSystemRepository pvSystemRepository)
            : base(unitOfWork)
        {
            this.groupRepository = groupRepository;
            this.groupUserRepository = groupUserRepository;
            this.groupPvSystemRepository = groupPvSystemRepository;
            this.userRepository = userRepository;
            this.pvSystemRepository = pvSystemRepository;
        }

        public CommandResult Create(GroupViewModel model)
        {
            var error = Validation(model);
            if (!string.IsNullOrEmpty(error))
            {
                return new CommandResult() { Message = error };
            }
            var group = new Group() { Name = model.Name, Description = model.Description, Active = true };
            groupRepository.Add(group);
            unitOfWork.Commit();
            return new CommandResult() { IsSuccess = true, Id = group.Id };
        }

        public CommandResult Update(GroupViewModel model)
        {
            var group = groupRepository.GetById(model.Id);
            if (group == null || !group.Active)
            {
                return new CommandResult() { Message = "group.validation.notfound" };
            }
            var error = Validation(model);
            if (!string.IsNullOrEmpty(error))
            {
                return new CommandResult() { Message = error };
            }
            group.Name = model.Name;
            group.Description = model.Description;

            groupRepository.Update(group);
            unitOfWork.Commit();
            return new CommandResult() { IsSuccess = true };
        }

        public CommandResult Delete(int id)
        {
            var group = groupRepository.GetById(id);
            if (group == null || !group.Active)
            {
                return new CommandResult() { Message = "group.validation.notfound" };
            }
            group.Active = false;
            groupRepository.Update(group);
            unitOfWork.Commit();
            return new CommandResult() { IsSuccess = true, Id = group.Id };
        }

        public CommandResult Revocation(int id)
        {
            var group = groupRepository.GetById(id);
            if (group == null || group.Active)
            {
                return new CommandResult() { Message = "group.validation.notfound" };
            }
            group.Active = true;
            groupRepository.Update(group);
            unitOfWork.Commit();
            return new CommandResult() { IsSuccess = true };
        }

        public GroupViewModel Get(int id)
        {
            var group = groupRepository.GetById(id);
            if (group == null || !group.Active)
            {
                throw new KeyNotFoundException("group not found");
            }
            return new GroupViewModel()
            {
                Id = group.Id,
                Name = group.Name,
                Description = group.Description,
                Active = group.Active
            };
        }

        public IEnumerable<GroupListViewModel> GetAll()
        {
            return groupRepository.GetAllAsQueryable()
               .Where(w => w.Active).Select(s => new GroupListViewModel() { Id = s.Id, Name = s.Name, Description = s.Description, Active = s.Active});
        }

        public IEnumerable<GroupPvSystemViewModel> GetGroupPvSystemsByGroupId(int groupId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<GroupUserViewModel> GetGroupUsersByGroupId(int groupId)
        {
            IEnumerable<GroupUserViewModel> userIdList;
            userIdList = groupUserRepository.GetByGroupId(groupId).Select(s => new GroupUserViewModel() { Id = s.Id, UserId = s.UserId });
            foreach (var user in userIdList)
            {
                if (user.UserId != null)
                {
                    int id = user.UserId;
                    var gu = userRepository.GetGroupUser(user.UserId);
                    var u = userRepository.GetUserWithRolesByUserName(gu.LoginName);

                    user.LoginName = gu.LoginName;
                    user.Name = gu.Name;
                    user.Email = gu.Email;
                }
            }
            userIdList = userRepository.GetGroupUsersByList(userIdList);
            return userIdList;
        }

        public CommandResult BindGroupPvSystems(int groupId, IEnumerable<GroupPvSystemViewModel> model)
        {
            throw new NotImplementedException();
        }

        public CommandResult BindGroupUsers(int groupId, IEnumerable<GroupUserViewModel> model)
        {
            throw new NotImplementedException();
        }













        private string Validation(GroupViewModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                return "group.validation.name.required";
            }
            if (model.Name.Length > 200)
            {
                return "group.validation.name.maxlength";
            }
            if (groupRepository.GetAllAsQueryable().Any(a => a.Id != model.Id && a.Name.ToLower() == model.Name.ToLower()))
            {
                return "group.validation.name.unique";
            }
            return string.Empty;
        }
    }
}
