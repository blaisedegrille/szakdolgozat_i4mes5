﻿using Coop.NFR.BusinessLogic.Services.Base;
using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Interfaces;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Services
{
    public class PvSystemDataService : ServiceBase, IPvSystemDataService
    {
        public readonly IPvSystemDataRepository pvSystemDataRepository;
        public PvSystemDataService(IUnitOfWork unitOfWork, IPvSystemDataRepository pvSystemDataRepository) : base(unitOfWork)
        {
            this.pvSystemDataRepository = pvSystemDataRepository;
        }

        // éves

        public IEnumerable<PvSystemDataViewModel> GetSpecificYearProfit(string pvSystemId, string year) {
            return unitOfWork.DbContext.PvSystemData
                .Where(x => x.PvSystemId == pvSystemId && x.ChannelName == "Profits" && x.LogDate == year)
                .Select(m => new PvSystemDataViewModel { PvSystemId = m.PvSystemId, PvSystemName = m.PvSystem.Name, Unit = m.Unit, Value = m.Value, Year = m.LogDate });
        }
        public IEnumerable<PvSystemDataViewModel> GetSpecificYearCO2(string pvSystemId, string year) {
            return unitOfWork.DbContext.PvSystemData
                .Where(x => x.PvSystemId == pvSystemId && x.ChannelName == "SavingsCO2" && x.LogDate == year)
                .Select(m => new PvSystemDataViewModel { PvSystemId = m.PvSystemId, PvSystemName = m.PvSystem.Name, Unit = m.Unit, Value = m.Value, Year = m.LogDate });
        }
    }
}
