﻿using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PVS.Monitor.BusinessLogic.Services
{
    public interface IUserService
    {
        IEnumerable<UserViewModel> GetUserList();
        CommandResult Update(EditUserViewModel userVM);
        CommandResult Create(NewUserViewModel userVM);
        CommandResult Delete(int id);
        UserViewModel GetByUserName(string loginName);
    }
}
