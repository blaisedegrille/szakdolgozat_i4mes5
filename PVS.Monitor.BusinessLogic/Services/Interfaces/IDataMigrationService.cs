﻿using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Services
{
    public interface IDataMigrationService
    {
        CommandResult MigratePvSystemListFirst();
        CommandResult MigrateDeviceListFirst(string pvSystemId);
        CommandResult MigrateServiceMessageListFirst(string pvSystemId, string dateFrom, string dateTo);
        CommandResult MigratePvSystemDataListFirst(string pvSystemId, string dateFrom, string dateTo);
        CommandResult MigrateDeviceDataListFirst(string pvSystemId, string deviceId, string dateFrom, string dateTo);

        // PvSystemMigrationViewModel MigratePvSystemList();
        // DeviceMigrationViewModel MigrateDeviceList();

    }
}