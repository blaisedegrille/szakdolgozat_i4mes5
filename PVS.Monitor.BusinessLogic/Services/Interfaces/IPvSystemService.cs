﻿using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Services
{
    public interface IPvSystemService
    {
        /// <summary>
        /// Egy csoport adatainak lekérdezése a jogosultsággal együtt 
        /// </summary>
        /// <param name="id">a csoport azonosítója</param>
        /// <returns>csoport adatai a jogosultsággal</returns>
        PvSystemViewModel GetPvSystemById(int id);
        /// <summary>
        /// Az összes csoport adatát adja vissza
        /// </summary>
        /// <returns></returns>
        IEnumerable<PvSystemViewModel> GetAllPvSystems();

        /// <summary>
        /// Csoport módosítása
        /// </summary>
        /// <param name="model">a csoport adatait tartalmazó view model</param>
        /// <returns>A művelet eredménye, hibaüzenet</returns>
        CommandResult UpdatePvSystem(PvSystemViewModel model);
        CommandResult CreatePvSystem(PvSystemViewModel model); 
        CommandResult CreatePvSystemsFromList(IEnumerable<PvSystemMigrationViewModel> model);

        IEnumerable<PvSystemDropDownViewModel> GetPvSystemDropDown();

    }
}