﻿using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Services
{
    public interface IDeviceDataService
    {
        IEnumerable<DeviceDataViewModel> GetAllDayInRangeData(string pvSystemId, DateTime from, DateTime to);
        IEnumerable<DeviceDataViewModel> GetSpecificDayData(string pvSystemId, DateTime date);
        IEnumerable<DeviceDataViewModel> GetSpecificMonthData(string pvSystemId, DateTime date);
        IEnumerable<DeviceDataViewModel> GetSpecificYearData(string pvSystemId, DateTime date);
        IEnumerable<DeviceDataViewModel> GetSpecificYearDeviceData(string pvSystemId, DateTime date);

    }
}