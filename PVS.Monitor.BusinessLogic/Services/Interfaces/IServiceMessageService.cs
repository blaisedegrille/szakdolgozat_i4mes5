﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Services.Interfaces
{
    public interface IServiceMessageService
    {
        IEnumerable<ServiceMessageViewModel> GetPvSystemMessages(string pvSystemId);
    }
}
