﻿using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Services
{
    public interface IGroupService
    {
        /// <summary>
        /// Egy csoport adatainak lekérdezése a jogosultsággal együtt 
        /// </summary>
        /// <param name="id">a csoport azonosítója</param>
        /// <returns>csoport adatai a jogosultsággal</returns>
        GroupViewModel Get(int id);
        /// <summary>
        /// Az összes csoport adatát adja vissza
        /// </summary>
        /// <returns></returns>
        IEnumerable<GroupListViewModel> GetAll();
        /// <summary>
        /// Inaktív státuszba teszi a csoportot
        /// </summary>
        /// <param name="id">a csoport azonosítója</param>
        CommandResult Delete(int id);
        /// <summary>
        /// Új csoport létrehozása
        /// </summary>
        /// <param name="model">a csoport adatait tartalmazó view model</param>
        /// <returns>A művelet eredménye, új id, hibaüzenet</returns>
        CommandResult Create(GroupViewModel model);

        /// <summary>
        /// Csoport módosítása
        /// </summary>
        /// <param name="model">a csoport adatait tartalmazó view model</param>
        /// <returns>A művelet eredménye, hibaüzenet</returns>
        CommandResult Update(GroupViewModel model);

        /// <summary>
        /// Aktív státuszba teszi a csoportot
        /// </summary>
        /// <param name="id">a csoport azonosítója</param>
        CommandResult Revocation(int id);

        /// <summary>
        /// Aktív státuszba teszi a csoportot
        /// </summary>
        /// <param name="groupId">a csoport azonosítója</param>
        /// <param name="model">a csoport azonosítója</param>
        CommandResult BindGroupUsers(int groupId, IEnumerable<GroupUserViewModel> model);

        /// <summary>
        /// Aktív státuszba teszi a csoportot
        /// </summary>
        /// <param name="groupId">a csoport azonosítója</param>
        /// <param name="model">a csoport azonosítója</param>
        CommandResult BindGroupPvSystems(int groupId, IEnumerable<GroupPvSystemViewModel> model);

        public IEnumerable<GroupUserViewModel> GetGroupUsersByGroupId(int groupId);
        public IEnumerable<GroupPvSystemViewModel> GetGroupPvSystemsByGroupId(int groupId);
    }
}