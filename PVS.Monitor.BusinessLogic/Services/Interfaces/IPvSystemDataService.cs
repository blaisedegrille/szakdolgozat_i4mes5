﻿using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Services
{
    public interface IPvSystemDataService
    {
        IEnumerable<PvSystemDataViewModel> GetSpecificYearProfit(string pvSystemId, string year);
        IEnumerable<PvSystemDataViewModel> GetSpecificYearCO2(string pvSystemId, string year);
    }
}