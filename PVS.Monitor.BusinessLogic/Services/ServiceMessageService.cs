﻿using Coop.NFR.BusinessLogic.Services.Base;
using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Interfaces;
using PVS.Monitor.BusinessLogic.Services.Interfaces;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace PVS.Monitor.BusinessLogic.Services
{
    public class ServiceMessageService : ServiceBase, IServiceMessageService
    {

        private readonly IServiceMessageRepository serviceMessageRepository;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork">Unity of Work pattern megvalósítához szükséges interface</param>
        /// <param name="groupRepository">Csoport adatokat kezelő repository</param>
        public ServiceMessageService(IUnitOfWork unitOfWork, IServiceMessageRepository serviceMessageRepository): base(unitOfWork) {
            this.serviceMessageRepository = serviceMessageRepository;
        }
        public IEnumerable<ServiceMessageViewModel> GetPvSystemMessages(string pvSystemId)
        {
            //  var deviceIdList = this.deviceRepository.GetAllAsQueryable().Where(x => x.PvSystemId == pvSystemId && x.DeviceType == "Inverter").Select(x => x.DeviceId);

            var result = unitOfWork.DbContext.ServiceMessage
                .Where(x => x.PvSystemId == pvSystemId)
                .Select(m => new ServiceMessageViewModel
                {
                    Id = m.Id,
                    PvSystemId = m.PvSystemId,
                    DeviceId = m.DeviceId,
                    LogDateTime = m.LogDateTime,
                    MessageText = m.MessageText,
                    StateCode = m.StateCode,
                    StateSeverity = m.StateSeverity,
                    StateType = m.StateType
                }).ToList();
            return this.ServiceMessageHelper(result);
        }

        public IEnumerable<ServiceMessageViewModel> ServiceMessageHelper(IEnumerable<ServiceMessageViewModel> messageList) 
        {
            foreach (var item in messageList)
            {
                var pvs = unitOfWork.DbContext.PvSystem
                    .Where(w => w.PvSystemId == item.PvSystemId)
                    .Select(s => new PvSystem { Name = s.Name })
                    .FirstOrDefault();
                item.PvSystemName = pvs.Name;
                var device = unitOfWork.DbContext.Device
                    .Where(w => w.DeviceId == item.DeviceId)
                    .Select(s => new Device { DeviceName = s.DeviceName, DeviceType = s.DeviceType })
                    .FirstOrDefault();
                item.DeviceName = device.DeviceName;
                item.DeviceType = device.DeviceType;
            }
            return messageList;
        } 

    }
}

