﻿using Coop.NFR.BusinessLogic.Services.Base;
using Microsoft.IdentityModel.Tokens;
using PVS.Monitor.BusinessLogic.Helpers;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;

using System.Text;

namespace PVS.Monitor.BusinessLogic.Services
{
    public class AuthService : ServiceBase, IAuthService
    {

        private static string Key = "";
        private readonly IUserService userService;

        public AuthService(IUnitOfWork unitOfWork, IUserService userService) : base(unitOfWork)
        {
            this.userService = userService;
        }

        public UserViewModel Authenticate(string username, string password, string key)
        {
            UserViewModel userVm = null;
            Key = key;
            bool isValidPassword = false;

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = unitOfWork.DbContext.User.SingleOrDefault(x => x.LoginName == username && x.Active == true);

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (user != null)
            {
                isValidPassword = BCrypt.Net.BCrypt.Verify(password, user.Password);
            }

            if (isValidPassword)
            {
                userVm = userService.GetByUserName(user.LoginName);
                userVm.Token = this.CreateToken(userVm);
                // userVm.TokenExpiration = DateTime.UtcNow.AddHours(2);
                userVm.TokenExpiration = DateTime.UtcNow.AddHours(1);
            }

            // authentication successful
            return userVm;
        }

        public User Register(string loginName, string password)
        {
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new ExceptionHelper("Password is required");

            if (unitOfWork.DbContext.User.Any(x => x.LoginName == loginName))
                throw new ExceptionHelper("Username \"" + loginName + "\" is already taken");

            // byte[] passwordHash, passwordSalt;
            // CreatePasswordHash(password, out passwordHash, out passwordSalt);
            User user = new User();

            user.LoginName = loginName;
            user.Name = "Test";
            user.Phone = "+36301234567";
            user.Email = "test@test.hu";

            user.Password = CreatePasswordHash(password);
            user.Active = false;

            unitOfWork.DbContext.User.Add(user);
            unitOfWork.DbContext.SaveChanges();

            return user;
        }

        private string CreateToken(UserViewModel userVm) {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Key);
            var claimtomb = new List<Claim>();
            foreach (var item in userVm.Roles)
            {
                claimtomb.Add(new Claim(ClaimTypes.Role, item));
            }
            claimtomb.Add(new Claim(ClaimTypes.Name, userVm.LoginName));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claimtomb),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }

        private string CreatePasswordHash(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        /*
        public IEnumerable<User> GetAll()
        {
            return unitOfWork.DbContext.User.Select(x => x).AsEnumerable();
        }

        public User GetById(int id)
        {
            return unitOfWork.DbContext.User.Find(id);
        }

        public void Update(User userParam, string password = null)
        {
            var user = unitOfWork.DbContext.User.Find(userParam.Id);

            if (user == null)
                throw new ExceptionHelper("User not found");

            // update username if it has changed
            if (!string.IsNullOrWhiteSpace(userParam.Username) && userParam.Username != user.Username)
            {
                // throw error if the new username is already taken
                if (unitOfWork.DbContext.User.Any(x => x.Username == userParam.Username))
                    throw new ExceptionHelper("Username " + userParam.Username + " is already taken");

                user.Username = userParam.Username;
            }

            // update user properties if provided
            if (!string.IsNullOrWhiteSpace(userParam.FirstName))
                user.FirstName = userParam.FirstName;

            if (!string.IsNullOrWhiteSpace(userParam.LastName))
                user.LastName = userParam.LastName;

            // update password if provided
            if (!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            unitOfWork.DbContext.User.Update(user);
            unitOfWork.DbContext.User.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = unitOfWork.DbContext.User.Find(id);
            if (user != null)
            {
                unitOfWork.DbContext.User.Remove(user);
                unitOfWork.DbContext.User.SaveChanges();
            }
        }

        // private helper methods
        private string CreatePasswordHash(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
        */
    }
}
