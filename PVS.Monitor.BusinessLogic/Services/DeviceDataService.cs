﻿using Coop.NFR.BusinessLogic.Services.Base;
using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Interfaces;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Services
{
    public class DeviceDataService : ServiceBase, IDeviceDataService
    {
        public readonly IDeviceDataRepository deviceDataRepository;
        public readonly IDeviceRepository deviceRepository;
        public DeviceDataService(IUnitOfWork unitOfWork, IDeviceDataRepository deviceDataRepository, IDeviceRepository deviceRepository) : base(unitOfWork)
        {
            this.deviceDataRepository = deviceDataRepository;
            this.deviceRepository = deviceRepository;
        }

        // éves

        public IEnumerable<DeviceDataViewModel> GetSpecificYearDeviceData(string pvSystemId, DateTime date) {
            var result = unitOfWork.DbContext.DeviceData
                .Where(x => x.PvSystemId == pvSystemId && x.ChannelName == "EnergyExported" && x.LogDate.Year == date.Year).GroupBy(m => new { m.DeviceId, m.LogDate.Year })
                .Select(m => new {
                    LogDate = m.Key.Year,
                    DeviceId = m.Key.DeviceId,
                    Value = m.Sum(o => o.Value)
                })
                .OrderBy(m => m.LogDate);

            List<DeviceDataViewModel> list = new List<DeviceDataViewModel>();
            var pvSystem = unitOfWork.DbContext.PvSystem.Where(w => w.PvSystemId == pvSystemId).FirstOrDefault();

            foreach (var item in result)
            {
                var device = unitOfWork.DbContext.Device.Where(w => w.DeviceId == item.DeviceId).FirstOrDefault();
                DeviceDataViewModel vm = new DeviceDataViewModel();
                vm.PvSystemId = pvSystemId;
                vm.PvSystemName = pvSystem.Name;
                vm.DeviceId = device.DeviceId;
                vm.DeviceName = device.DeviceName;
                vm.Year = item.LogDate;
                vm.Value = item.Value.Value;
                list.Add(vm);
            }
            return list;


        }
        public IEnumerable<DeviceDataViewModel> GetAllDayInRangeData(string pvSystemId, DateTime from, DateTime to) 
        {
            //  var deviceIdList = this.deviceRepository.GetAllAsQueryable().Where(x => x.PvSystemId == pvSystemId && x.DeviceType == "Inverter").Select(x => x.DeviceId);
            var result = unitOfWork.DbContext.DeviceData.Where(x => x.PvSystemId == pvSystemId && x.ChannelName == "EnergyExported" && x.LogDate >= from && x.LogDate <= to).GroupBy(m => new { m.LogDate })
                .Select(m => new {
                LogDate = m.Key.LogDate,
                Value = m.Sum(o => o.Value)
                });

            List<DeviceDataViewModel> list = new List<DeviceDataViewModel>();
            var pvSystem = unitOfWork.DbContext.PvSystem.Where(w => w.PvSystemId == pvSystemId).FirstOrDefault();

            foreach (var item in result)
            {
                DeviceDataViewModel vm = new DeviceDataViewModel();
                vm.PvSystemId = pvSystemId;
                vm.PvSystemName = pvSystem.Name;
                vm.LogDate = item.LogDate.Date;
                vm.Value = item.Value.Value;
                list.Add(vm);
            }
            return list;
        }

        public IEnumerable<DeviceDataViewModel> GetSpecificYearData(string pvSystemId, DateTime date)
        {
            //  var deviceIdList = this.deviceRepository.GetAllAsQueryable().Where(x => x.PvSystemId == pvSystemId && x.DeviceType == "Inverter").Select(x => x.DeviceId);
            var result = unitOfWork.DbContext.DeviceData
                .Where(x => x.PvSystemId == pvSystemId && x.ChannelName == "EnergyExported" && x.LogDate.Year == date.Year ).GroupBy(m => new { m.LogDate.Month })
                .Select(m => new {
                    LogDate = m.Key.Month,
                    Value = m.Sum(o => o.Value)
                })
                .OrderBy(m => m.LogDate);

            /*
            result.GroupBy(m => new { m.LogDate.Month }).Select(m => new { 
                Month = m.Key,
                Value = m.Sum(item => item.Value)
            });
            */

            List<DeviceDataViewModel> list = new List<DeviceDataViewModel>();
            var pvSystem = unitOfWork.DbContext.PvSystem.Where(w => w.PvSystemId == pvSystemId).FirstOrDefault();

            foreach (var item in result)
            {
                DeviceDataViewModel vm = new DeviceDataViewModel();
                vm.PvSystemId = pvSystemId;
                vm.PvSystemName = pvSystem.Name;
                vm.Month = item.LogDate;
                vm.Value = item.Value.Value;
                list.Add(vm);
            }
            return list;
        }

        public IEnumerable<DeviceDataViewModel> GetSpecificMonthData(string pvSystemId, DateTime date)
        {
            //  var deviceIdList = this.deviceRepository.GetAllAsQueryable().Where(x => x.PvSystemId == pvSystemId && x.DeviceType == "Inverter").Select(x => x.DeviceId);
            var result = unitOfWork.DbContext.DeviceData
                .Where(x => x.PvSystemId == pvSystemId && x.ChannelName == "EnergyExported" && x.LogDate.Year == date.Year && x.LogDate.Month == date.Month).GroupBy(m => new { m.LogDate })
                .Select(m => new {
                    LogDate = m.Key.LogDate,
                    Value = m.Sum(o => o.Value)
                });

            List<DeviceDataViewModel> list = new List<DeviceDataViewModel>();
            var pvSystem = unitOfWork.DbContext.PvSystem.Where(w => w.PvSystemId == pvSystemId).FirstOrDefault();

            foreach (var item in result)
            {
                DeviceDataViewModel vm = new DeviceDataViewModel();
                vm.PvSystemId = pvSystemId;
                vm.PvSystemName = pvSystem.Name;
                vm.LogDate = item.LogDate.Date;
                vm.Value = item.Value.Value;
                list.Add(vm);
            }
            return list;
        }

        public IEnumerable<DeviceDataViewModel> GetSpecificDayData(string pvSystemId, DateTime date)
        {
            //  var deviceIdList = this.deviceRepository.GetAllAsQueryable().Where(x => x.PvSystemId == pvSystemId && x.DeviceType == "Inverter").Select(x => x.DeviceId);

            var result = unitOfWork.DbContext.DeviceData
                .Where(x => x.PvSystemId == pvSystemId && x.ChannelName == "EnergyExported" && x.LogDate == date)
                .Select(m => new DeviceDataViewModel {
                    LogDate = m.LogDate,
                    DeviceId = m.DeviceId,
                    Value = m.Value
                }).ToList();
            return this.SpecificHelper(pvSystemId, result);
        }

        private IEnumerable<DeviceDataViewModel> SpecificHelper(string pvSystemId, IEnumerable<DeviceDataViewModel> model) 
        {
            foreach (var item in model)
            {
                item.PvSystemId = pvSystemId;
                var pvSystem = unitOfWork.DbContext.PvSystem
                    .Where(w => w.PvSystemId == pvSystemId)
                    .Select(s => new PvSystem { Name = s.Name }).
                    FirstOrDefault();

                item.PvSystemName = pvSystem.Name;
                var device = unitOfWork.DbContext.Device
                    .Where(w => w.DeviceId == item.DeviceId)
                    .Select(s => new Device { DeviceName = s.DeviceName }).
                    FirstOrDefault();
                item.DeviceName = device.DeviceName;
            }

            return model;
        }


        // havi

        // heti

        // napi


    }
}
