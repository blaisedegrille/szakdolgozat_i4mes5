﻿using Coop.NFR.BusinessLogic.Services.Base;
using PVS.Monitor.BusinessLogic.Helpers;
using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Interfaces;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PVS.Monitor.BusinessLogic.Services
{
    public class UserService : CommonService, IUserService
    {
        /// <summary>
        /// Felhasználó adatokat kezelő repository
        /// </summary>
        private readonly IUserRepository userRepository;

        /// <summary>
        /// Felhasználó - szerepkör adatokat kezelő repository
        /// </summary>
        private readonly IUserRoleRepository userRoleRepository;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="userRepository"></param>
        public UserService(IUnitOfWork unitOfWork, IUserRepository userRepository, IUserRoleRepository userRoleRepository)
            : base(unitOfWork)
        {
            this.userRepository = userRepository;
            this.userRoleRepository = userRoleRepository;
        }

        public CommandResult Create(NewUserViewModel userVM)
        {
            // validation
            if (string.IsNullOrWhiteSpace(userVM.Password))
                throw new ExceptionHelper("Password is required");

            var user = userRepository.GetUserByUserName(userVM.LoginName);
            if (user != null)
            {
                throw new ExceptionHelper("Username \"" + userVM.LoginName + "\" is already taken");
            }
            else
            {
                User newUser = new User() {
                    LoginName = userVM.LoginName,
                    Name = userVM.Name,
                    Phone = userVM.Phone,
                    Email = userVM.Email,
                    Password = this.CreatePasswordHash(userVM.Password),
                    Active = true,
                    UserRole = SetUserRole(userVM.Roles)
                };
                userRepository.Add(newUser);
                unitOfWork.Commit();
            }
            return new CommandResult() { IsSuccess = true, Message = "Sikeres mentés"  };
        }

        public CommandResult Update(EditUserViewModel userVM)
        {
            userRoleRepository.Delete(x => x.UserId == userVM.Id);
            User user = userRepository.GetById(userVM.Id);
            user.LoginName = userVM.LoginName;
            user.Name = userVM.Name;
            if (userVM.Password != null)
                user.Password = this.CreatePasswordHash(userVM.Password);
            user.Phone = userVM.Phone;
            user.Email = userVM.Email;
            user.Active = userVM.Active;
            user.UserRole = SetUserRole(userVM.Roles);
            user = userRepository.Update(user);
            unitOfWork.Commit();
            return new CommandResult { IsSuccess = true, Id = user.Id };
        }

        // user és userrole-oktorlese
        public CommandResult Delete(int id)
        {
            var user = userRepository.GetById(id);
            var roles = userRoleRepository.GetAllAsQueryable().Where(w => w.UserId == id);

            foreach (var role in roles)
            {
                userRoleRepository.Delete(role);
            }

            userRepository.Delete(user);
            unitOfWork.Commit();

            return new CommandResult() { IsSuccess = true, Message = "Sikeres törlés" };
        }

        public IEnumerable<UserViewModel> GetUserList()
        {
            return userRepository.GetUsersWithRoles();
        }

        public UserViewModel GetByUserName(string loginName)
        {
            UserViewModel userView = null;
            UserViewModel user = userRepository.GetUserWithRolesByUserName(loginName);
            if (user != null)
            {
                userView = new UserViewModel();
                userView.Id = user.Id;
                userView.LoginName = user.LoginName;
                userView.Name = user.Name;
                userView.Email = user.Email;
                userView.Roles = user.Roles;
                userView.Token = user.Token;
                return userView;
            }
            else
            {
                return userView;
            }
        }

        private ICollection<UserRole> SetUserRole(List<RoleViewModel> userRoles)
        {
            List<UserRole> userRoleList = new List<UserRole>();
            // int userId = userVM.
            foreach (var item in userRoles)
            {
                UserRole ur = new UserRole();
                ur.RoleId = item.Id;
                ur.Active = true;
                userRoleRepository.Add(ur);
                userRoleList.Add(ur);
            }
            return userRoleList;
        }

        private string CreatePasswordHash(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }
    }
}
