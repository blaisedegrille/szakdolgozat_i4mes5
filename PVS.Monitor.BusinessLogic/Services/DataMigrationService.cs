﻿using Coop.NFR.BusinessLogic.Services.Base;
using Newtonsoft.Json.Linq;
using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Interfaces;
using PVS.Monitor.BusinessLogic.ViewModels;
using PVS.Monitor.Entities.Models;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;

namespace PVS.Monitor.BusinessLogic.Services
{
    public class DataMigrationService : ServiceBase, IDataMigrationService
    {

        private readonly IPvSystemRepository pvSystemRepository;
        private readonly IDeviceRepository deviceRepository;
        private readonly IServiceMessageRepository serviceMessageRepository;
        private readonly IDeviceDataRepository deviceDataRepository;
        private readonly IPvSystemDataRepository pvSystemDataRepository;

        private readonly string URL = "https://api.solarweb.com/swqapi/";
        private readonly string accessKeyId = "FKIA05C20F22239247B0A79694E324026964";
        private readonly string accessKeyValue = "bd2fc7cf-b30f-4444-b32f-4ea4e09110bb";
        private readonly string accessKeyExpiration = "2021-08-02T23:59:59Z";
        private readonly string accessKeyDescription = "Rács Balázs";

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork">Unity of Work pattern megvalósítához szükséges interface</param>
        /// <param name="groupRepository">Csoport adatokat kezelő repository</param>
        public DataMigrationService(
            IUnitOfWork unitOfWork, 
            IPvSystemRepository pvSystemRepository, 
            IDeviceRepository deviceRepository, 
            IServiceMessageRepository serviceMessageRepository,
            IDeviceDataRepository deviceDataRepository,
            IPvSystemDataRepository pvSystemDataRepository)
            : base(unitOfWork) {
            this.pvSystemRepository = pvSystemRepository;
            this.deviceRepository = deviceRepository;
            this.serviceMessageRepository = serviceMessageRepository;
            this.deviceDataRepository = deviceDataRepository;
            this.pvSystemDataRepository = pvSystemDataRepository;
        }

        public CommandResult MigratePvSystemListFirst()
        {
            Migration migration = new Migration();
            migration.MigrationDate = DateTime.Now;
            migration.Description = "Migration started at: " + DateTime.Now.ToString();
            this.unitOfWork.DbContext.Migration.Add(migration);
            unitOfWork.Commit();

            string pvSystemList = this.SwqapiService(URL + "pvsystems");
            PvSystemMigrationViewModel model = Newtonsoft.Json.JsonConvert.DeserializeObject<PvSystemMigrationViewModel>(pvSystemList);
            foreach (var pvSystem in model.PvSystems)
            {
                var system = new PvSystem()
                {
                    PvSystemId = pvSystem.PvSystemId,
                    Name = pvSystem.Name,
                    Description = pvSystem.Name,
                    PeakPower = pvSystem.PeakPower,
                    Country = pvSystem.Address.Country,
                    ZipCode = pvSystem.Address.ZipCode,
                    Street = pvSystem.Address.Street,
                    City = pvSystem.Address.City,
                    Region = pvSystem.Address.State,
                    PictureUrl = pvSystem.PictureUrl,
                    InstallationDate = pvSystem.InstallationDate,
                    LastImport = pvSystem.LastImport,
                    MeteoData = pvSystem.MeteoData,
                    TimeZone = pvSystem.TimeZone,
                    CreateDate = DateTime.Now
                };
                pvSystemRepository.Add(system);
                unitOfWork.Commit();

                // pvSystemhez tartozo osszesitett adatok mentese (napi)
                this.MigratePvSystemDataListFirst(system.PvSystemId, "2019", "2021");

                // pvSystemhez tartozo device-ek adatainak mentese (napi)
                this.MigrateDeviceListFirst(system.PvSystemId);
            }
            foreach (var pvSystem in model.PvSystems)
            {
                // pvSystem-hez tartozo device-ok szerviz adatainak mentese
                this.MigrateServiceMessageListFirst(pvSystem.PvSystemId, "2021-01-01T01:01:00Z", "2021-01-31T01:01:00Z");
                this.MigrateServiceMessageListFirst(pvSystem.PvSystemId, "2021-02-01T01:01:00Z", "2021-02-28T01:01:00Z");
                this.MigrateServiceMessageListFirst(pvSystem.PvSystemId, "2021-03-01T01:01:00Z", "2021-03-31T01:01:00Z");
                this.MigrateServiceMessageListFirst(pvSystem.PvSystemId, "2021-04-01T01:01:00Z", "2021-04-30T01:01:00Z");
            }

            migration = new Migration();
            migration.MigrationDate = DateTime.Now;
            migration.Description = "Migration ended at: " + DateTime.Now.ToString();
            this.unitOfWork.DbContext.Migration.Add(migration);
            unitOfWork.Commit();

            return new CommandResult() { IsSuccess = true };
        }

        public CommandResult MigrateDeviceListFirst(string pvSystemId)
        {
            string deviceList = SwqapiService(URL + "pvsystems/" + pvSystemId + "/devices");
            DeviceMigrationViewModel model = Newtonsoft.Json.JsonConvert.DeserializeObject<DeviceMigrationViewModel>(deviceList);
            foreach (var item in model.Devices)
            {
                var device = new Device()
                {
                    DeviceId = item.DeviceId,
                    PvSystemId = pvSystemId,
                    DeviceName = item.DeviceName,
                    DeviceType = item.DeviceType,
                    DeviceTypeDetails = item.DeviceTypeDetails,
                    DeviceManufacturer = item.DeviceManufacturer,
                    SerialNumber = item.SerialNumber,
                    DataloggerId = item.DataloggerId,
                    NodeType = item.NodeType,
                    NumberMpptrackers = item.NumberMPPTrackers,
                    NumberPhases = item.NumberPhases,
                    NominalAcPower = item.NominalAcPower,
                    FirmwareAvailableVersion = item.Firmware.AvailableVersion,
                    FirmwareInstalledVersion = item.Firmware.InstalledVersion,
                    FirmwareUpdateAvailable = item.Firmware.UpdateAvailable,
                    IsActive = item.IsActive,
                    ActivationDate = item.ActivationDate,
                    DeactivationDate = item.DeactivationDate,
                    Capacity = item.Capacity,
                    IsOnline = item.IsOnline,
                    IpAddressV4 = item.IpAddressV4,
                    CreateDate = DateTime.Now
                };
                if (item.PeakPower != null)
                {
                    device.PeakPowerDc1 = item.PeakPower.Dc1;
                    device.PeakPowerDc2 = item.PeakPower.Dc2;
                }
                else {
                    device.PeakPowerDc1 = null;
                    device.PeakPowerDc2 = null;
                }
                var deviceid = deviceRepository.Get(x => x.DeviceId == item.DeviceId);
                if (deviceid == null)
                {
                    deviceRepository.Add(device);
                    unitOfWork.Commit();
                    this.MigrateDeviceDataListFirst(pvSystemId, device.DeviceId, "2019-01-01", "2021-05-05");
                }
            }
            return new CommandResult() { IsSuccess = true };
        }

        public CommandResult MigrateServiceMessageListFirst(string pvSystemId, string dateFrom, string dateTo)
        {
            // "https://api.solarweb.com/swqapi/pvsystems/ff2ad483-1254-464b-a594-2a4c61241a92/messages/hu?from=2021-03-01T01%3A01%3A00Z&to=2021-03-31T01%3A01%3A00Z"
            string messageList = this.SwqapiService(URL + "pvsystems/" + pvSystemId + "/messages/hu?from=" + dateFrom + "&to=" + dateTo);
            ServiceMessageMigrationViewModel model = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceMessageMigrationViewModel>(messageList);
            foreach (var item in model.Messages)
            {
                var message = new ServiceMessage()
                {
                    PvSystemId = item.PvSystemId,
                    DeviceId = item.DeviceId,
                    StateType = item.StateType,
                    StateCode = item.StateCode,
                    StateSeverity = item.StateSeverity,
                    LogDateTime = item.LogDateTime,
                    MessageText = item.Text,
                    CreateDate = DateTime.Now
                };
                var deviceid = deviceRepository.Get(x => x.DeviceId == item.DeviceId);
                if (deviceid != null)
                {
                    serviceMessageRepository.Add(message);
                    unitOfWork.Commit();
                }
            }
            return new CommandResult() { IsSuccess = true };
        }

        public CommandResult MigratePvSystemDataListFirst(string pvSystemId, string dateFrom, string dateTo)
        {
            // "https://api.solarweb.com/swqapi/pvsystems/ff2ad483-1254-464b-a594-2a4c61241a92/messages/hu?from=2021-03-01T01%3A01%3A00Z&to=2021-03-31T01%3A01%3A00Z"
            string channelData = this.SwqapiService(URL + "pvsystems/" + pvSystemId + "/aggrdata?from=" + dateFrom + "&to=" + dateTo + "&limit=1000000");
            PvSystemDataMigrationViewModel model = Newtonsoft.Json.JsonConvert.DeserializeObject<PvSystemDataMigrationViewModel>(channelData);
            foreach (var item in model.Data)
            {
                foreach (var channel in item.Channels)
                {
                    var data = new PvSystemData()
                    {
                        PvSystemId = model.PvSystemId,
                        LogDate = item.LogDateTime,
                    };
                    data.ChannelName = channel.ChannelName;
                    data.ChannelType = channel.ChannelType;
                    data.Unit = channel.Unit;
                    data.Value = channel.Value;
                    data.CreateDate = DateTime.Now;
                    pvSystemDataRepository.Add(data);
                    unitOfWork.Commit();
                }
            }
            return new CommandResult() { IsSuccess = true };
        }

        public CommandResult MigrateDeviceDataListFirst(string pvSystemId, string deviceId, string dateFrom, string dateTo)
        {
            // "https://api.solarweb.com/swqapi/pvsystems/ff2ad483-1254-464b-a594-2a4c61241a92/messages/hu?from=2021-03-01T01%3A01%3A00Z&to=2021-03-31T01%3A01%3A00Z"
            string channelData = this.SwqapiService(URL + "pvsystems/" + pvSystemId + "/devices/" + deviceId + "/aggrdata?from=" + dateFrom + "&to=" + dateTo + "&limit=1000000");
            DeviceDataMigrationViewModel model = Newtonsoft.Json.JsonConvert.DeserializeObject<DeviceDataMigrationViewModel>(channelData);
            foreach (var item in model.Data)
            {
                foreach (var channel in item.Channels)
                {
                    var data = new DeviceData()
                    {
                        PvSystemId = model.PvSystemId,
                        DeviceId = model.DeviceId,
                        LogDate = item.LogDateTime,
                    };
                    data.ChannelName = channel.ChannelName;
                    data.ChannelType = channel.ChannelType;
                    data.Unit = channel.Unit;
                    data.Value = channel.Value;
                    data.CreateDate = DateTime.Now;
                    var deviceid = deviceRepository.Get(x => x.DeviceId == model.DeviceId);
                    if (deviceid != null)
                    {
                        deviceDataRepository.Add(data);
                        unitOfWork.Commit();
                    }
                }
            }
            return new CommandResult() { IsSuccess = true };
        }

        public string SwqapiService(string Url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Headers.Add("accessKeyId", accessKeyId);
            request.Headers.Add("accessKeyValue", accessKeyValue);

            WebResponse webResponse = request.GetResponse();
            Stream webStream = webResponse.GetResponseStream();
            StreamReader responseReader = new StreamReader(webStream);
            string response = responseReader.ReadToEnd();
            responseReader.Close();
            return response;
        }

    }
}

/*
  public PvSystemMigrationViewModel MigratePvSystemList()
        {
            string pvSystemList = this.SwqapiService(URL + "pvsystems");
            PvSystemMigrationViewModel model = Newtonsoft.Json.JsonConvert.DeserializeObject<PvSystemMigrationViewModel>(pvSystemList);
            // pvSystemRepository.Add(group);
            // unitOfWork.Commit();
            return model;
        }

        public DeviceMigrationViewModel MigrateDeviceList()
        {
            // var pvsIdList = this.unitOfWork.DbContext.PvSystem.SelectMany(x => x.Swid);

            /*foreach (var item in pvsIdList)
            {
                string deviceList = SwqapiService(URL + "pvsystems/" + item + "/devices");
                DeviceMigrationViewModel model = Newtonsoft.Json.JsonConvert.DeserializeObject<DeviceMigrationViewModel>(deviceList);
            }

string deviceList = SwqapiService(URL + "pvsystems/" + "ff2ad483-1254-464b-a594-2a4c61241a92" + "/devices");
DeviceMigrationViewModel model = Newtonsoft.Json.JsonConvert.DeserializeObject<DeviceMigrationViewModel>(deviceList);
// pvSystemRepository.Add(group);
// unitOfWork.Commit();
return model;
        }

        public ServiceMessageMigrationViewModel MigrateServiceMessageList(string dateFrom, string dateTo)
{
    string pvSystemList = this.SwqapiService(URL + "pvsystems");
    ServiceMessageMigrationViewModel model = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceMessageMigrationViewModel>(pvSystemList);
    // pvSystemRepository.Add(group);
    // unitOfWork.Commit();
    return model;
}

*/