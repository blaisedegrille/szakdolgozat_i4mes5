﻿using Coop.NFR.BusinessLogic.Services.Base;
using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;
using PVS.Monitor.BusinessLogic.Repositories.Interfaces;
using PVS.Monitor.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Services
{
    public class RoleService : ServiceBase, IRoleService
    {
        /// <summary>
        /// Role adatokat kezelő repository
        /// </summary>
        private readonly IRoleRepository roleRepository;

        private readonly IUserRoleRepository userRoleRepository;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="roleRepository"></param>
        public RoleService(IUnitOfWork unitOfWork, IRoleRepository roleRepository, IUserRoleRepository userRoleRepository)
            : base(unitOfWork)
        {
            this.roleRepository = roleRepository;
            this.userRoleRepository = userRoleRepository;
        }

        public List<RoleViewModel> Roles(string email)
        {
            return roleRepository.GetAllAsQueryable().Where(x => x.Active).Select(x => new RoleViewModel
            {
                Id = x.Id,
                RoleName = x.Name
            }).ToList();
        }
    }
}
