﻿using PVS.Monitor.BusinessLogic.Infrastructure.Interfaces;

namespace Coop.NFR.BusinessLogic.Services.Base
{
    public class ServiceBase
    {
        protected readonly IUnitOfWork unitOfWork;

        public ServiceBase(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }       
    }
}
