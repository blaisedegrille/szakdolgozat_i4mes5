﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace PVS.Monitor.BusinessLogic.Helpers
{
    public class ExceptionHelper : Exception
    {
        public ExceptionHelper() : base() { }

        public ExceptionHelper(string message) : base(message) { }

        public ExceptionHelper(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args))
        {
        }
    }
}
