﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PVS.Monitor.Api.Infrastructure
{
    /// <summary>
    /// A segédosztály a dependency injection configuráció számára, amely azokat az osztályokat adja vissza a különböző metódusokban, 
    /// amelyekre a dependency injection-t végre kell hajtani
    /// </summary>
    public static class DependencyInjectionHelper
    {
        public static IEnumerable<Type> GetTypesWithCustomAttribute<T>(Assembly assembly)
        {
            foreach (Type type in assembly.GetTypes())
            {
                if (type.GetCustomAttributes(typeof(T), true).Length > 0)
                {
                    yield return type;
                }
            }
        }

        public static IEnumerable<Type> GetTypesWithSuffix(Assembly assembly, String suffix)
        {
            foreach (Type type in assembly.GetTypes())
            {
                if (!type.IsInterface && type.Name.EndsWith(suffix))
                {
                    yield return type;
                }
            }
        }

        public static IEnumerable<Type> GetTypesWithSuffix(Assembly[] assemblies, String suffix)
        {
            foreach (var assembly in assemblies)
            {
                if (!assembly.FullName.StartsWith("PVS.Monitor")) continue;

                foreach (Type type in assembly.GetTypes())
                {
                    if (!type.IsInterface && type.Name.EndsWith(suffix))
                    {
                        yield return type;
                    }
                }
            }
        }

        public static IEnumerable<Provider> GetTypesWithSuffixes(Assembly[] assemblies, params String[] suffixes)
        {
            foreach (var assembly in assemblies)
            {
                if (!assembly.FullName.StartsWith("PVS.Monitor")) continue;
                var allProviderTypes = assembly.GetTypes();
                foreach (Type interf in allProviderTypes.Where(w => w.IsInterface))
                {
                    foreach (String suffix in suffixes)
                    {
                        if (interf.Name.EndsWith(suffix))
                        {
                            var impl = allProviderTypes.FirstOrDefault(c => c.IsClass && interf.Name.Substring(1) == c.Name);
                            yield return new Provider() { Interface = interf, Implementation = impl } ;
                        }
                    }
                }
            }
        }

        public static Assembly GetAssemblyByName(string name)
        {
            return Assembly.Load(name);
        }
    }
}
