﻿using System;

namespace PVS.Monitor.Api.Infrastructure
{
    public class Provider
    {
        public Type Interface { get; set; }
        public Type Implementation { get; set; }
    }
}
