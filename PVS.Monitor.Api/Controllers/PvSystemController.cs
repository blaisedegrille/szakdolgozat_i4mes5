﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PVS.Monitor.Api.Helpers;
using PVS.Monitor.Api.Messages.Requests;
using PVS.Monitor.Api.Messages.Responses;
using PVS.Monitor.Api.Models;
using PVS.Monitor.API.Controllers;
using PVS.Monitor.BusinessLogic.Services;
using PVS.Monitor.BusinessLogic.ViewModels;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PvSystemController : BaseApiController
    {
        /// <summary>
        /// PvSystem adatokat kezelő service
        /// </summary>
        private readonly IPvSystemService pvSystemService;
        
        private readonly IPvSystemDataService pvSystemDataService;


        /// <summary>
        /// Létrehoz egy új példányt a <see cref="PvSystemController"/> osztályból
        /// </summary>
        /// <param name="PvSystemService">PvSystem adatokat kezelő service</param>
        public PvSystemController(IPvSystemService pvSystemService, IPvSystemDataService pvSystemDataService)
        {
            this.pvSystemService = pvSystemService;
            this.pvSystemDataService = pvSystemDataService;
        }

        [Authorize]
        [HttpGet]
        [Route("GetPvSystemById")]
        public PvSystemResponse GetPvSystemById(int id)
        {
            return new PvSystemResponse() { PvSystem = pvSystemService.GetPvSystemById(id) };
        }

        [Authorize(Roles = Roles.ServiceUser + "," + Roles.ReportUser)]
        [HttpGet]
        [Route("GetPvSystemList")]
        public PvSystemListResponse GetAllPvSystems()
        {
            return new PvSystemListResponse() { PvSystemList = pvSystemService.GetAllPvSystems() };
        }

        [Authorize(Roles = Roles.ServiceUser + "," + Roles.ReportUser)]
        [HttpGet]
        [Route("GetPvSystemDropDown")]
        public PvSystemDropDownResponse GetPvSystemDropDown()
        {
            return new PvSystemDropDownResponse() { PvSystemListDropDownList = pvSystemService.GetPvSystemDropDown() };
        }

        /// <summary>
        /// Új csoport létrehozása
        /// </summary>
        /// <param name="request">a csoport adatait tartalmazó kérés</param>
        /// <returns>a művelet eredménye, hibaüzenet, ha hiba történt</returns>
        [Authorize]
        [HttpPost]
        [Route("CreatePvSystem")]
        public PvSystemResponse Post(PvSystemRequest request)
        {
            var result = pvSystemService.CreatePvSystem(request.PvSystem);
            return ResponseHelper.CreateResponse<PvSystemResponse>(result);
        }

        /// <summary>
        /// Új csoport létrehozása
        /// </summary>
        /// <param name="request">a csoport adatait tartalmazó kérés</param>
        /// <returns>a művelet eredménye, hibaüzenet, ha hiba történt</returns>
        [Authorize(Roles = Roles.ServiceUser + "," + Roles.ReportUser)]
        [HttpPost]
        [Route("CreatePvSystemsFromList")]
        public PvSystemResponse Post(PvSystemMigrationRequest request)
        {
            var result = pvSystemService.CreatePvSystemsFromList(request.PvSystems);
            return ResponseHelper.CreateResponse<PvSystemResponse>(result);
        }

        [Authorize(Roles = Roles.ServiceUser + "," + Roles.ReportUser + "," + Roles.Admin)]
        [HttpPost]
        [Route("GetSpecificYearProfit")]
        public IEnumerable<PvSystemDataViewModel> GetSpecificYearProfit(PvSystemDataRequest request)
        {
            IEnumerable<PvSystemDataViewModel> model = pvSystemDataService.GetSpecificYearProfit(request.PvSystemId, request.Year);
            return model;
        }

        [Authorize(Roles = Roles.ServiceUser + "," + Roles.ReportUser + "," + Roles.Admin)]
        [HttpPost]
        [Route("GetSpecificYearCO2")]
        public IEnumerable<PvSystemDataViewModel> GetSpecificYearCO2(PvSystemDataRequest request)
        {
            IEnumerable<PvSystemDataViewModel> model = pvSystemDataService.GetSpecificYearCO2(request.PvSystemId, request.Year);
            return model;
        }
    }
}
