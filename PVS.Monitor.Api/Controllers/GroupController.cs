﻿using Microsoft.AspNetCore.Mvc;
using PVS.Monitor.Api.Helpers;
using PVS.Monitor.Api.Messages.Requests;
using PVS.Monitor.Api.Messages.Responses;
using PVS.Monitor.API.Controllers;
using PVS.Monitor.BusinessLogic.Services;

namespace PVS.Monitor.Api.Controllers
{
    /*
    // [Route("api/[controller]")]
    // [ApiController]
    public class GroupController : BaseApiController
    {

        /// <summary>
        /// Csoport adatokat kezelő service
        /// </summary>
        private readonly IGroupService groupService;

        /// <summary>
        /// Létrehoz egy új példányt a <see cref="GroupController"/> osztályból
        /// </summary>
        /// <param name="GroupService">Felhasználó adatokat kezelő service</param>
        public GroupController(IGroupService groupService)
        {
            this.groupService = groupService;
        }

        [HttpGet]
        [Route("GetGroupList")]
        public GroupListResponse Get() 
        {
            return new GroupListResponse() { GroupList = groupService.GetAll() };
        }

        /// <summary>
        /// Új csoport létrehozása
        /// </summary>
        /// <param name="request">a csoport adatait tartalmazó kérés</param>
        /// <returns>a művelet eredménye, hibaüzenet, ha hiba történt</returns>
        [HttpPost]
        [Route("CreateGroup")]
        public GroupResponse Post(GroupRequest request)
        {
            var result = groupService.Create(request.Group);
            return ResponseHelper.CreateResponse<GroupResponse>(result);
        }

        /// <summary>
        /// Csoport módosítása
        /// </summary>
        /// <param name="request">a csoport adatait tartalmazó kérés</param>
        /// <returns>a művelet eredménye, hibaüzenet, ha hiba történt</returns>
        [HttpPut]
        [Route("UpdateGroup")]
        public GroupResponse Put(GroupRequest request)
        {
            var result = groupService.Update(request.Group);
            return ResponseHelper.CreateResponse<GroupResponse>(result);
        }

        /// <summary>
        /// Csoport törlése (inaktívvá tétele)
        /// </summary>
        /// <param name="id">a csoport azonosítója</param>
        /// <returns>a művelet eredménye, hibaüzenet, ha hiba történt</returns>
        [HttpDelete]
        [Route("DeleteGroup")]
        public GroupResponse Delete(int id)
        {
            var result = groupService.Delete(id);
            return ResponseHelper.CreateResponse<GroupResponse>(result);
        }

        /// <summary>
        /// Csoport visszaállítás
        /// </summary>
        /// <param name="id">a csoport azonosítója</param>
        /// <returns>a művelet eredménye, hibaüzenet, ha hiba történt</returns>
        [HttpPost("GroupRevocation")]
        public GroupResponse Revocation(int id)
        {
            var result = groupService.Revocation(id);
            return ResponseHelper.CreateResponse<GroupResponse>(result);
        }

        [HttpGet]
        [Route("GetGroupPvSystems")]
        public GroupPvSystemResponse GetGroupPlants(int groupId)
        {
            return new GroupPvSystemResponse() { GroupPvSystems = groupService.GetGroupPvSystemsByGroupId(groupId) };
        }

        /// <summary>
        /// Plant módosítása
        /// </summary>
        /// <param name="request">a plant adatait tartalmazó kérés</param>
        /// <returns>a művelet eredménye, hibaüzenet, ha hiba történt</returns>
        [HttpPut]
        [Route("BindGroupUser")]
        public GroupUserResponse Put(GroupUserRequest request)
        {
            var result = groupService.BindGroupUsers(request.GroupId, request.GroupUsers);
            return ResponseHelper.CreateResponse<GroupUserResponse>(result);
        }

        [HttpGet]
        [Route("GetGroupUsers")]
        public GroupUserResponse GetGroupUsers(int groupId)
        {
            return new GroupUserResponse() { GroupUsers = groupService.GetGroupUsersByGroupId(groupId) };
        }

        /// <summary>
        /// Plant módosítása
        /// </summary>
        /// <param name="request">a plant adatait tartalmazó kérés</param>
        /// <returns>a művelet eredménye, hibaüzenet, ha hiba történt</returns>
        [HttpPut]
        [Route("BindGroupPvSystem")]
        public GroupPvSystemResponse Put(GroupPvSystemRequest request)
        {
            var result = groupService.BindGroupPvSystems(request.GroupId, request.GroupPvSystems);
            return ResponseHelper.CreateResponse<GroupPvSystemResponse>(result);
        }
    }
    */
}
