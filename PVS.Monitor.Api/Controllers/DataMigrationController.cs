﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PVS.Monitor.Api.Models;
using PVS.Monitor.API.Controllers;
using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.Services;
using PVS.Monitor.BusinessLogic.ViewModels;
using System;

namespace PVS.Monitor.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DataMigrationController : BaseApiController
    {
        /// <summary>
        /// DataMigration service
        /// </summary>
        private readonly IDataMigrationService dataMigrationService;

        /// <summary>
        /// Létrehoz egy új példányt a <see cref="DataMigrationController"/> osztályból
        /// </summary>
        /// <param name="DataMigrationService"> DataMigration service </param>
        public DataMigrationController(IDataMigrationService dataMigrationService)
        {
            this.dataMigrationService = dataMigrationService;
        }

        [Authorize(Roles = Roles.MigrateUser)]
        [HttpGet]
        [Route("GetPvSystemListDB")]
        public CommandResult GetPvSystemListDB()
        {
            CommandResult cr = dataMigrationService.MigratePvSystemListFirst();
            return cr;
        }
        /*
        [Authorize(Roles = Roles.MigrateUser)]
        [HttpGet]
        [Route("GetDeviceListDB")]
        public CommandResult GetDeviceListDB(string pvSystemId)
        {
            CommandResult cr = dataMigrationService.MigrateDeviceListFirst(pvSystemId);
            return cr;
        }

        [Authorize(Roles = Roles.MigrateUser)]
        [HttpGet]
        [Route("GetServiceMessageListDB")]
        public CommandResult GetServiceMessageListDB(string pvSystemId, string dateFrom, string dateTo)
        {
            CommandResult cr = dataMigrationService.MigrateServiceMessageListFirst(pvSystemId, dateFrom, dateTo);
            return cr;
        }

        [Authorize(Roles = Roles.MigrateUser)]
        [HttpGet]
        [Route("GetPvSystemDataMigrationListDB")]
        public CommandResult GetPvSystemDataMigrationListDB(string pvSystemId, string dateFrom, string dateTo)
        {
            CommandResult cr = dataMigrationService.MigratePvSystemDataListFirst(pvSystemId, dateFrom, dateTo);
            return cr;
        }

        [Authorize(Roles = Roles.MigrateUser)]
        [HttpGet]
        [Route("GetDeviceDataMigrationListDB")]
        public CommandResult GetDeviceDataMigrationListDB(string pvSystemId, string deviceId, string dateFrom, string dateTo)
        {
            CommandResult cr = dataMigrationService.MigrateDeviceDataListFirst(pvSystemId, deviceId, dateFrom, dateTo);
            return cr;
        }
        */
    }
}
