﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PVS.Monitor.Api.Messages.Requests;
using PVS.Monitor.Api.Models;
using PVS.Monitor.API.Controllers;
using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.Services;
using PVS.Monitor.BusinessLogic.ViewModels;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseApiController
    {

        /// <summary>
        /// Felhasználó adatokat kezelő service
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// Létrehoz egy új példányt a <see cref="UserController"/> osztályból
        /// </summary>
        /// <param name="userService">Felhasználó adatokat kezelő service</param>
        public UserController(IUserService userService)
        {
            this.userService = userService;
        }


        [Authorize(Roles = Roles.Admin)]
        [HttpPost]
        [Route("CreateUser")]
        public CommandResult CreateUser(NewUserRequest request)
        {
            return userService.Create(request.NewUser);
        }

        [Authorize(Roles = Roles.Admin)]
        [HttpPost]
        [Route("DeleteUser")]
        public CommandResult DeleteUser(int id)
        {
            return userService.Delete(id);
        }

        [Authorize(Roles = Roles.Admin)]
        [HttpPost]
        [Route("UpdateUser")]
        public CommandResult UpdateUser(EditUserRequest request)
        {
            return userService.Update(request.EditUser);

        }

        [Authorize(Roles = Roles.Admin)]
        [HttpGet]
        [Route("GetUserList")]
        public IEnumerable<UserViewModel> GetUserList()
        {
            return userService.GetUserList();
        }


        [Authorize(Roles = Roles.Admin)]
        [HttpGet]
        [Route("GetUserByLoginName")]
        public UserViewModel GetUserByLoginName(string loginName)
        {
            return userService.GetByUserName(loginName);
        }
    }
}
