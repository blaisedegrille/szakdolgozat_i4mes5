﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PVS.Monitor.Api.Messages.Requests;
using PVS.Monitor.Api.Models;
using PVS.Monitor.API.Controllers;
using PVS.Monitor.BusinessLogic.Infrastructure;
using PVS.Monitor.BusinessLogic.Services;
using PVS.Monitor.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceDataController : BaseApiController
    {
        /// <summary>
        /// DeviceData service
        /// </summary>
        private readonly IDeviceDataService deviceDataService;

        /// <summary>
        /// Létrehoz egy új példányt a <see cref="DeviceDataController"/> osztályból
        /// </summary>
        /// <param name="DeviceDataService"> DeviceData service </param>
        public DeviceDataController(IDeviceDataService deviceDataService)
        {
            this.deviceDataService = deviceDataService;
        }

        [Authorize(Roles = Roles.ReportUser)]
        [HttpPost]
        [Route("GetAllDayInRangeData")]
        public IEnumerable<DeviceDataViewModel> GetAllDayInRangeData(DeviceDataRequest request)
        {
            IEnumerable<DeviceDataViewModel> model = deviceDataService.GetAllDayInRangeData(request.PvSystemId, request.DateFrom.AddHours(2), request.DateTo.AddHours(2));
            return model;
        }

        [Authorize(Roles = Roles.ReportUser)]
        [HttpPost]
        [Route("GetSpecificDayData")]
        public IEnumerable<DeviceDataViewModel> GetSpecificDayData(DeviceDataRequest request)
        {
            IEnumerable<DeviceDataViewModel> model = deviceDataService.GetSpecificDayData(request.PvSystemId, request.DateFrom.AddHours(2));
            return model;
        }

        [Authorize(Roles = Roles.ReportUser)]
        [HttpPost]
        [Route("GetSpecificMonthData")]
        public IEnumerable<DeviceDataViewModel> GetSpecificMonthData(DeviceDataRequest request)
        {
            IEnumerable<DeviceDataViewModel> model = deviceDataService.GetSpecificMonthData(request.PvSystemId, request.DateFrom);
            return model;
        }

        [Authorize(Roles = Roles.ReportUser)]
        [HttpPost]
        [Route("GetSpecificYearData")]
        public IEnumerable<DeviceDataViewModel> GetSpecificYearData(DeviceDataRequest request)
        {
            IEnumerable<DeviceDataViewModel> model = deviceDataService.GetSpecificYearData(request.PvSystemId, request.DateFrom);
            return model;
        }

        [Authorize(Roles = Roles.ServiceUser + "," + Roles.ReportUser + "," + Roles.Admin)]
        [HttpPost]
        [Route("GetSpecificYearDeviceData")]
        public IEnumerable<DeviceDataViewModel> GetSpecificYearDeviceData(DeviceDataRequest request)
        {
            IEnumerable<DeviceDataViewModel> model = deviceDataService.GetSpecificYearDeviceData(request.PvSystemId, request.DateFrom);
            return model;
        }
    }
}
