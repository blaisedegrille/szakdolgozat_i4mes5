﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PVS.Monitor.Api.Messages.Requests;
using PVS.Monitor.Api.Models;
using PVS.Monitor.API.Controllers;
using PVS.Monitor.BusinessLogic.Services;
using PVS.Monitor.BusinessLogic.Services.Interfaces;
using PVS.Monitor.BusinessLogic.ViewModels;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceMessageController : BaseApiController
    {
        /// <summary>
        /// DataMigration service
        /// </summary>
        private readonly IServiceMessageService serviceMessageService;

        /// <summary>
        /// Létrehoz egy új példányt a <see cref="ServiceMessageController"/> osztályból
        /// </summary>
        /// <param name="DataMigrationService"> DataMigration service </param>
        public ServiceMessageController(IServiceMessageService serviceMessageService)
        {
            this.serviceMessageService = serviceMessageService;
        }

        [Authorize(Roles = Roles.ServiceUser)]
        [HttpGet]
        [Route("GetPvSystemMessage")]
        public IEnumerable<ServiceMessageViewModel> GetAllDayInRangeData(string pvSystemId)
        {
            IEnumerable<ServiceMessageViewModel> model = serviceMessageService.GetPvSystemMessages(pvSystemId);
            return model;
        }
    }
}
