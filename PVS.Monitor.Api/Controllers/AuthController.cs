﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PVS.Monitor.Api.Helpers;
using PVS.Monitor.Api.Messages.Requests;
using PVS.Monitor.BusinessLogic.Helpers;
using PVS.Monitor.BusinessLogic.Services;


namespace PVS.Monitor.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService authService;
        private readonly AppSettings appSettings;

        public AuthController(IAuthService userService, IOptions<AppSettings> appSettings)
        {
            this.authService = userService;
            this.appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody] AuthenticateRequest request)
        {
            var user = this.authService.Authenticate(request.LoginName, request.Password, this.appSettings.Secret);
            if (user == null)
                return BadRequest(new { message = "Hibás felhasználónév vagy jelszó!" });

            // return basic user info and authentication token
            return Ok(new
            {
                Id = user.Id,
                Name = user.Name,
                LoginName = user.LoginName,
                Email = user.Email,
                Roles = user.Roles,
                Token = user.Token,
                TokenExpiration = user.TokenExpiration
            });
        }

        
        [AllowAnonymous]
        [HttpPost("Register")]
        public IActionResult Register([FromBody] RegisterRequest request)
        {
            try
            {
                this.authService.Register(request.Username, request.Password);
                return Ok();
            }
            catch (ExceptionHelper ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
