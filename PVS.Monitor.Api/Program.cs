using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;

namespace PVS.Monitor.Api
{
    public class Program
    {
        // private static string _env = "dev";
        public static void Main(string[] args)
        {
            // SetEnvironment();
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
             WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
        }


    /*
    private static void SetEnvironment() 
    {
        try
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json", false).Build();
            _env = config.GetSection("Environment").Value;
        }
        catch (Exception)
        {
            _env = "dev";
        }
    }

    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args).ConfigureAppConfiguration((hostingContext, config) => 
        {
            config.AddJsonFile("appsettings.json");
            config.AddJsonFile($"appsettings.{_env}.json", optional: true);

        }).UseStartup<Startup>();

    }
    */
}
