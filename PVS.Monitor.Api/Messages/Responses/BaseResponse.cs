﻿using PVS.Monitor.Api.Models;
using PVS.Monitor.BusinessLogic.Infrastructure;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Messages.Responses
{
    /// <summary>
    /// Alap válaszüzenet
    /// </summary>
    public class BaseResponse
    {

        /// <summary>
        /// át veszi a hiba listát
        /// </summary>
        public void SetResponse(CommandResult result)
        {
            if (!result.IsSuccess)
            {
                ErrorList.Add(new ApiError() { ErrorCode = result.ErrorCode, Message = result.Message, ErrorLevel = ErrorLevel.Critical });
            }
        }

        /// <summary>
        /// Hiba lista
        /// </summary>
        public List<ApiError> ErrorList { get; set; } = new List<ApiError>();

    }
}