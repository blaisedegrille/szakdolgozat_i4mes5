﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Messages.Responses
{
    public class GroupPvSystemResponse : BaseResponse
    {
        public IEnumerable<GroupPvSystemViewModel> GroupPvSystems { get; set; }
    }
}
