﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Messages.Responses
{
    public class PvSystemDropDownResponse : BaseResponse
    {
        public IEnumerable<PvSystemDropDownViewModel> PvSystemListDropDownList { get; set; }
    }
}
