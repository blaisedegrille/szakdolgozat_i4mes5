﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Messages.Responses
{
    public class PvSystemListResponse : BaseResponse
    {
        public IEnumerable<PvSystemViewModel> PvSystemList { get; set; }
    }
}
