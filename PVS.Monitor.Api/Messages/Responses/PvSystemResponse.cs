﻿using PVS.Monitor.BusinessLogic.ViewModels;

namespace PVS.Monitor.Api.Messages.Responses
{
    public class PvSystemResponse : BaseResponse
    {
        public PvSystemViewModel PvSystem { get; set; }
    }
}
