﻿namespace PVS.Monitor.Api.Messages.Responses
{
    public class GroupResponse : BaseResponse
    {
        public int Id { get; set; }
    }
}
