﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Messages.Responses
{
    public class GroupUserResponse : BaseResponse
    {
        public IEnumerable<GroupUserViewModel> GroupUsers { get; set; }

    }
}
