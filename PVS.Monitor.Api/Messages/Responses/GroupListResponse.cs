﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PVS.Monitor.Api.Messages.Responses
{
    public class GroupListResponse : BaseResponse
    {
        public IEnumerable<GroupListViewModel> GroupList { get; set; }
    }
}
