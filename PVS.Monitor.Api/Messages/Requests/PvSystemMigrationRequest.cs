﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class PvSystemMigrationRequest : BaseRequest
    {
        public IEnumerable<PvSystemMigrationViewModel> PvSystems { get; set; }
    }
}
