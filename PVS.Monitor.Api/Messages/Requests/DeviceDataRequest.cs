﻿using System;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class DeviceDataRequest : BaseRequest
    {
        public List<string> PvSystemIdList { get; set; }
        public string PvSystemId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

    }
}
