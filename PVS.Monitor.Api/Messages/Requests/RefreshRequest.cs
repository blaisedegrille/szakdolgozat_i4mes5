﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class RefreshRequest : BaseRequest
    {
        [Required]
        public string RefreshToken { get; set; }
    }
}
