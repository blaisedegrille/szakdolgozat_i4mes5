﻿using PVS.Monitor.Api.Messages.Requests;
using System.ComponentModel.DataAnnotations;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class AuthenticateRequest
    {
        [Required]
        public string LoginName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
