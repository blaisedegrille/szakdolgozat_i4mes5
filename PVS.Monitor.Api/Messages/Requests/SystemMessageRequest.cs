﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class SystemMessageRequest : BaseRequest
    {
        public string PvSystemId { get; set; }
    }
}
