﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System.ComponentModel.DataAnnotations;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class EditUserRequest
    {
        public EditUserViewModel EditUser { get; set; }
    }
}
