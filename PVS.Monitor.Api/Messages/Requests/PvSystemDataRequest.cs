﻿using System;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class PvSystemDataRequest : BaseRequest
    {
        public string PvSystemId { get; set; }
        public string Year { get; set; }
    }
}
