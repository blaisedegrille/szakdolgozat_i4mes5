﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class GroupPvSystemRequest
    {
        public int GroupId { get; set; }
        public IEnumerable<GroupPvSystemViewModel> GroupPvSystems { get; set; }
    }
}
