﻿using PVS.Monitor.BusinessLogic.ViewModels;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class PvSystemRequest : BaseRequest
    {
        public PvSystemViewModel PvSystem { get; set; }
    }
}
