﻿using PVS.Monitor.BusinessLogic.ViewModels;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class GroupRequest : BaseRequest
    {
        public GroupViewModel Group { get; set; }
    }
}
