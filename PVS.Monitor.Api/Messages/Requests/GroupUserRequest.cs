﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System.Collections.Generic;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class GroupUserRequest
    {
        public  int GroupId { get; set; }
        public IEnumerable<GroupUserViewModel> GroupUsers { get; set; }

    }
}
