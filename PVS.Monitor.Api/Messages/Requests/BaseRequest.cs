﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PVS.Monitor.Api.Messages.Requests
{
    /// <summary>
    /// Alap kérés, ami tartalmazza a felület nyelvét
    /// </summary>
    public class BaseRequest
    {
        /// <summary>
        /// Nyelv
        /// </summary>
        public string Language { get; set; }
    }
}