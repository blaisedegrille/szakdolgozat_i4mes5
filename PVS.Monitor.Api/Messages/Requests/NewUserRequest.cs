﻿using PVS.Monitor.BusinessLogic.ViewModels;
using System.ComponentModel.DataAnnotations;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class NewUserRequest
    {
        public NewUserViewModel NewUser { get; set; }
    }
}
