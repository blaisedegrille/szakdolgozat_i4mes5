﻿using System.ComponentModel.DataAnnotations;

namespace PVS.Monitor.Api.Messages.Requests
{
    public class RegisterRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
