﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PVS.Monitor.Api.Models
{
    /// <summary>
    /// Hiba üzenet szintje
    /// </summary>
    public enum ErrorLevel
    {
        Validation = 1,
        Warning = 2,
        Critical = 3
    }
}