﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PVS.Monitor.Api.Models
{
    public class Roles
    {
        public const string Admin = "Admin";
        public const string MigrateUser = "MigrateUser";
        public const string ReportUser = "ReportUser";
        public const string ServiceUser = "ServiceUser";
    }
}
