﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PVS.Monitor.Api.Models
{
    /// <summary>
    /// Api error, this class contains the error description 
    /// </summary>
    public class ApiError
    {
        /// <summary>
        /// Identifier of the error
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// String representation of the error
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// String representation of the error's title
        /// </summary>
        public string MessageTitle { get; set; }

        /// <summary>
        /// level of the error (validation, warning, critical)
        /// </summary>
        public ErrorLevel ErrorLevel { get; set; }

        /// <summary>
        /// If the error happened on a field on the UI, this property contains the name of that field
        /// </summary>
        public string FieldName { get; set; }
    }
}
