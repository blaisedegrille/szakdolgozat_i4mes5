﻿using PVS.Monitor.Api.Messages.Requests;
using PVS.Monitor.Api.Messages.Responses;
using PVS.Monitor.Api.Models;
using PVS.Monitor.BusinessLogic.Infrastructure;
using System;

namespace PVS.Monitor.Api.Helpers
{
    /// <summary>
    /// Response helper methods
    /// </summary>
    public static class ResponseHelper
    {

        /// <summary>
        /// Create a response with parameters which can be copied or calculated from a request
        /// </summary>
        /// <typeparam name="TReq">request type</typeparam>
        /// <typeparam name="TRes">response type</typeparam>
        /// <param name="request"></param>
        public static TRes CreateResponse<TReq, TRes>(TReq request) where TReq : BaseRequest where TRes : BaseResponse, new()
        {
            TRes response = new TRes();
            return response;
        }

        /// <summary>
        /// Create a response with parameters which can be copied or calculated from a request
        /// </summary>
        /// <typeparam name="TReq">request type</typeparam>
        /// <typeparam name="TRes">response type</typeparam>
        /// <param name="request"></param>
        public static TRes CreateResponse<TRes>(CommandResult result) where TRes : BaseResponse, new()
        {
            TRes response = new TRes();
            MapCommandResultToResponse<TRes>(response, result);
            return response;
        }

        /// <summary>
        /// Maps the command result to the response
        /// </summary>
        /// <typeparam name="TRes"></typeparam>
        /// <param name="response"></param>
        /// <param name="result"></param>
        public static void MapCommandResultToResponse<TRes>(TRes response, CommandResult result) where TRes : BaseResponse
        {
            if (result == null)
            {
                throw new ArgumentNullException(nameof(result));
            }

            if (!result.IsSuccess)
            {
                response.ErrorList.Add(new ApiError() { ErrorLevel = ErrorLevel.Validation, Message = result.Message });
            }
        }
    }
}