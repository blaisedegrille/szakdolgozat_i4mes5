#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["PVS.Monitor.Api/PVS.Monitor.Api.csproj", "PVS.Monitor.Api/"]
COPY ["PVS.Monitor.BusinessLogic/PVS.Monitor.BusinessLogic.csproj", "PVS.Monitor.BusinessLogic/"]
COPY ["PVS.Monitor.Entities/PVS.Monitor.Entities.csproj", "PVS.Monitor.Entities/"]
RUN dotnet restore "PVS.Monitor.Api/PVS.Monitor.Api.csproj"
COPY . .
WORKDIR "/src/PVS.Monitor.Api"
RUN dotnet build "PVS.Monitor.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "PVS.Monitor.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "PVS.Monitor.Api.dll"]
