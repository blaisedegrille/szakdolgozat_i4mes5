<div align="center">
<h3 align="center">PV Monitor</h3>


[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![node](https://img.shields.io/badge/node-14.15.1-green)]()
[![ngx--rocket](https://img.shields.io/badge/ngx--rocket-9.1.0-green)]()
[![NET Core](https://img.shields.io/badge/.NETCore-3.1.0-green)]()
</div>

**Fejlesztői környezet**

- Kliens: Visual Studio Code
- BackEnd: Visual Studio 2019

# Running .net core 3.1 & angular application on docker

#### Initialize project
```bash
git clone git@gitlab.com:{$USER_NAME}/docker-dotnet-angular.git
cd docker-dotnet-angular
```

#### Running the api with frontend & database service
```bash
docker-compose up --build -d
```

#### Building api with forntend image (backend & frontend are in one image)
```bash
docker build --tag szakapi .
```

#### Building db image
```bash
cd db/docker/
docker build --tag szakdb .
```




