import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Credentials, CredentialsService } from './credentials.service';

export interface LoginRequest {
  username: string;
  password: string;
  remember?: boolean;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private readonly JWT_TOKEN = 'JWT_TOKEN';
  private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';
  clearTimeout: any;

  constructor(private http: HttpClient, private credentialsService: CredentialsService, private router: Router) {}

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginRequest): Observable<Credentials> {
    const loginName: string = context.username;
    const password: string = context.password;

    let data: any;
    return this.http
      .post(`/api/Auth/Authenticate`, { loginName, password }) // Send the state to the server
      .pipe(
        map(
          (
            r: any // Process the response
          ) => {
            data = r;
            this.credentialsService.setCredentials(data, context.remember);
            this.doLoginUser(data.token, data.tokenExpiration); 
            this.handleUser(data);
            return data;
          }
        )
      );
  }

  handleUser(response: any) {
    const date = new Date().getTime();
    const expirationDate = new Date(response.tokenExpiration).getTime();
    //console.log('HELLO');
    this.autoLogout(expirationDate - date);
  }

  /*
  autoLogin() {
    let credentials = this.credentialsService.credentials;

    let expirationDate = new Date(credentials.refreshTokenExpiration).getTime();

    let date = new Date().getTime();

    this.autoLogout(expirationDate - date);
  }
*/

  autoLogout(expirationDate: number) {
    console.log('Good bye');
    console.log(expirationDate);
    this.clearTimeout = setTimeout(() => {
      this.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
    }, expirationDate);
  }

  forgotpassword(email: any): Observable<any> {
    return this.http
      .post(`api/Authenticate/ForgotPassword`, { email }) // Send the state to the server
      .pipe(
        map(
          (
            r: any // Process the response
          ) => {
            return r;
          }
        )
      );
  }

  resetPassword(password: string, confirmPassword: string, token: string): Observable<any> {
    return this.http
      .post(`api/Authenticate/ResetPassword`, { password, confirmPassword, token }) // Send the state to the server
      .pipe(
        map(
          (
            r: any // Process the response
          ) => {
            return r;
          }
        )
      );
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    /*
    this.http.post(`api/Authenticate/logout`, {}).pipe(
      map(
        (
          r: any // Process the response
        ) => {
          this.credentialsService.setCredentials();
          this.removeTokens();
        }
      )
    );*/
    this.credentialsService.setCredentials();
    this.removeTokens();
    if (this.clearTimeout) {
      clearTimeout(this.clearTimeout);
    }
    return of(true);
  }

  /*refreshToken() {
    return this.http
      .post(`api/Authenticate/refresh-token`, {
        refreshToken: this.getRefreshToken(),
      })
      .pipe(
        tap((tokens: any) => {
          this.storeJwtToken(tokens.jwt);
        })
      );
  }*/

  isLoggedIn() {
    return !!this.getJwtToken();
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  private doLoginUser(token: any, tokenExpiration: any) {
    this.storeTokens(token);
  }

  private doLogoutUser() {
    this.removeTokens();
  }

  private getRefreshToken() {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  private storeJwtToken(jwt: string) {
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }

  private storeTokens(token: any) {
    localStorage.setItem(this.JWT_TOKEN, token);
    // localStorage.setItem(this.REFRESH_TOKEN, refreshToken);
  }

  private removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
    // localStorage.removeItem(this.REFRESH_TOKEN);
  }
}
