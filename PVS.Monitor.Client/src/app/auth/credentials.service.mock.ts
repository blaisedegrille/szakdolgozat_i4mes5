import { Credentials } from './credentials.service';

export class MockCredentialsService {
  credentials: Credentials | null = {
    userName: 'racsbalazs',
    token: '@Ez@Egy@Titkos@Jelszo@',
    refreshToken: '@Ez@Egy@Titkos@Jelszo@', 
    refreshTokenExpiration: new Date(2019, 8, 1),
    roles: [],
    menus: []
  };

  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  setCredentials(credentials?: Credentials, _remember?: boolean) {
    this.credentials = credentials || null;
  }
}
