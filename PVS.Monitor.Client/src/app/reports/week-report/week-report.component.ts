import { Component, OnInit, Injectable } from '@angular/core';
import { MatDatepickerInputEvent, MatDateRangeSelectionStrategy, DateRange, MAT_DATE_RANGE_SELECTION_STRATEGY } from '@angular/material/datepicker';
import { ReportService } from '@app/services/report.service';
import { ChartTooltipOptions, ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Color, Label, ThemeService } from 'ng2-charts';
import { DateAdapter } from '@angular/material/core';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';


import * as moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { Moment } from 'moment';

@Injectable()
export class FiveDayRangeSelectionStrategy<D> implements MatDateRangeSelectionStrategy<D> {
  constructor(private _dateAdapter: DateAdapter<D>) { }

  selectionFinished(date: D | null): DateRange<D> {
    return this._createFiveDayRange(date);
  }

  createPreview(activeDate: D | null): DateRange<D> {
    return this._createFiveDayRange(activeDate);
  }

  private _createFiveDayRange(date: D | null): DateRange<D> {
    if (date) {
      const start = this._dateAdapter.addCalendarDays(date, 0);
      const end = this._dateAdapter.addCalendarDays(date, 6);
      return new DateRange<D>(start, end);
    }

    return new DateRange<D>(null, null);
  }
}

@Component({
  selector: 'app-week-report',
  templateUrl: './week-report.component.html',
  styleUrls: ['./week-report.component.scss'],
  providers: [{
    provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
    useClass: FiveDayRangeSelectionStrategy
  }]
})
export class WeekReportComponent implements OnInit {
  // loader
  public isLoading = false;
  // picker
  public startDate = new Date(2019, 0, 1);
  public endDate = new Date(2021, 0, 1);
  public minDate: Moment;
  public maxDate: Moment;

  public chartIsVisible = false;

  public selectedPvSystem: any;
  public pvSystemInput: any[] = [];

  //unit selection
  public selectedUnit: any;
  public unitInput: any[] = ['Wh', 'kWh', 'MWh'];

  public dateFrom: Date;
  public dateTo: Date;

  public chartData: any[] = [];
  public deviceValue: any;
  public deviceName: string;


  public pvSystemName: string;
  public pvSystemId: string = 'ff2ad483-1254-464b-a594-2a4c61241a92';
  public dateStr: string = '2020-05-31';

  // public dateList: any[] = [];
  public valueList: any[] = [];
  public dataList: any[] = [];
  // public dateList: Date[] = [];
  
  public dateString: string;
  public dateLog: Date;

  public chartTooltipOptions: ChartTooltipOptions = {
    position: 'center'
  };

  public barChartOptions: ChartOptions;
  public barChartPlugins = [pluginDataLabels];

  public barChartOptionsWh: ChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'Heti riport '
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Wh',
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 20,
        }
      }
    }
  };

  public barChartOptionskWh: ChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'Heti riport '
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'kWh',
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 20,
        }
      }
    }
  };

  public barChartOptionsMWh: ChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'Heti riport '
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'MWh',
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 20,
        }
      }
    }
  };

  // public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartLabels: Label[] =
    ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
      '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
      '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
  public barChartType: ChartType = 'bar';

  /*
    public color: Color[] = [
      {
        borderColor: '#00a6e6',
        backgroundColor: 'rgba(0, 169, 240, 0.46)',
        hoverBackgroundColor: '#00a6e6',
        borderWidth: 2
      },
    ];
    */

  public barChartLegend = true;

  public color: Color[] = [
    {
      //NARANCSSÁRGA
      borderColor: '#a569bd',
      backgroundColor: '#e1bee7',
      hoverBackgroundColor: '#c39bd3',
      borderWidth: 2
    }
  ];

  public random: number;
  public randomMemory: number;
  public colora: Color[] = [
    {
      //VILÁGOSKÉK - AQUA
      backgroundColor: ' #b3e5fc',
      hoverBackgroundColor: '#4DC4FF',
      borderColor: '#45B0E5',
      borderWidth: 2
    },
    {
      //ZÖLD - MINT
      backgroundColor: '#dcedc8',
      hoverBackgroundColor: '#66bb6a',
      borderColor: '#47c98a',
      borderWidth: 2
    },
    {
      //barna
      borderColor: '#AAB2BD',
      backgroundColor: '#d7ccc8',
      hoverBackgroundColor: '#a1887f',
      borderWidth: 2
    },
    {
      //Lila
      borderColor: '#a569bd',
      backgroundColor: '#e1bee7',
      hoverBackgroundColor: '#c39bd3',
      borderWidth: 2
    },
    {
      //PIROS
      backgroundColor: '#ffab91 ',
      hoverBackgroundColor: '#E9573F',
      borderColor: '#E9573F',
      borderWidth: 2
    },
    {
      //PIROS
      backgroundColor: '#ffab91 ',
      hoverBackgroundColor: '#E9573F',
      borderColor: '#E9573F',
      borderWidth: 2
    },
    {
      //VILÁGOSKÉK - AQUA
      backgroundColor: ' #b3e5fc',
      hoverBackgroundColor: '#4DC4FF',
      borderColor: '#45B0E5',
      borderWidth: 2
    },
    {
      //SÁRGA
      backgroundColor: '#ffe082',
      hoverBackgroundColor: '#FFCE54',
      borderColor: '#F6BB42',
      borderWidth: 2
    },
    {
      //SÁRGA
      backgroundColor: '#ffe082',
      hoverBackgroundColor: '#FFCE54',
      borderColor: '#F6BB42',
      borderWidth: 2
    },
    {
      //ZÖLD - MINT
      backgroundColor: '#dcedc8',
      hoverBackgroundColor: '#66bb6a',
      borderColor: '#47c98a',
      borderWidth: 2
    },
  ];


  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];

  constructor(private reportService: ReportService, private themeService: ThemeService) { }

  ngOnInit(): void {
    this.setLoader(true);
    this.setColor();
    this.getDropdownData();
    const currentYear = moment().year();
    this.minDate = moment([currentYear - 2, 0, 1]);
    this.maxDate = moment([currentYear + 0, 4, 5]);
  }

  setLoader(visible: boolean) {
    const mainContainer = document.getElementById("mainContainer");
    if (visible) {
      mainContainer.style.opacity = "0.3";
      this.isLoading = true;
    } else {
      mainContainer.style.opacity = "1";
      this.isLoading = false;
    }
  }

  setChartTitle():string {
    this.pvSystemName = '';
    this.pvSystemInput.forEach((pvSystem) => {
      if (pvSystem.pvSystemId === this.selectedPvSystem) {
        this.pvSystemName = pvSystem.name;
        console.log(this.pvSystemName);
      }
    });
    return 'Havi riport: ' + this.pvSystemName;
  }

  getRandom() {
    this.random = Math.floor(Math.random() * this.colora.length);
    return this.random;
  }

  setColor() {
    this.color = [];
    this.randomMemory = this.getRandom();
    var number = this.getRandom();

    if (this.randomMemory === number) {
      while (this.randomMemory === this.random) {
        this.getRandom();
      }
    }
    let color = {
      borderColor: this.colora[this.random].borderColor,
      backgroundColor: this.colora[this.random].backgroundColor,
      hoverBackgroundColor: this.colora[this.random].hoverBackgroundColor,
      borderWidth: 2
    };
    this.color.push(color);
  }

  dateFromInput(event: MatDatepickerInputEvent<Date>) {
    this.dateFrom = event.value;
  }

  dateToInput(event: MatDatepickerInputEvent<Date>) {
    this.dateTo = event.value;
  }

  search() {
    this.setColor();
    this.chartIsVisible = false;
    if (this.selectedPvSystem.value !== null && this.dateFrom !== null) {
      this.setLoader(true);
      this.getReportData();
    }
  }

  getDropdownData() {
    this.reportService.getDropdownData()
      .subscribe((response: any) => {
        this.pvSystemInput = response;
        this.setLoader(false);
      });
  }

  getReportData() {
    //this.dateList = [];
    this.dataList = [];
    this.valueList = [];
    this.chartData = [];
    // this.barChartData = [];
    this.barChartLabels = [];
    //this.reportService.GetSpecificDayData(this.pvSystemId, this.dateStr).subscribe((response: any) => {
    this.reportService.GetAllDayInRangeData(this.selectedPvSystem, this.dateFrom, this.dateTo).subscribe((response: any) => {
      this.dataList = response;
      this.dataList.forEach((element) => {
        if (this.selectedUnit === 'Wh') {
          this.deviceValue = element.value.toFixed(2);
          this.barChartOptionsWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionsWh;
        }
        if (this.selectedUnit === 'kWh') {
          this.deviceValue = (element.value / 1000).toFixed(2);
          this.barChartOptionskWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionskWh;
        }
        if (this.selectedUnit === 'MWh') {
          this.deviceValue = (element.value / 1000000).toFixed(2);
          this.barChartOptionsMWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionsMWh;
        }
        this.valueList.push(this.deviceValue);


        this.dateLog = new Date(element.logDate);
        this.dateString = this.dateLog.toLocaleDateString();

        this.barChartLabels.push(this.dateString);
        this.pvSystemName = element.pvSystemName;
      });
      var layout = {
        layout: {
          padding: 20
        }
      };
      this.barChartData = [{
        data: this.valueList,
        label: this.pvSystemName,
      }];
      this.chartIsVisible = true;
      this.setLoader(false);
    });
  }



  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  }

  public randomize(): void {
    this.barChartType = this.barChartType === 'bar' ? 'line' : 'bar';
    this.getReportData();

  }

  chartConfig() {
    this.barChartLabels;
    this.barChartData;
  }

}
