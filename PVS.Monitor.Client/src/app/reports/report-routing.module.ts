import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

import { Shell } from '@app/shell/shell.service';
import { DayReportComponent } from './day-report/day-report.component';
import { YearReportComponent } from './year-report/year-report.component';
import { MonthReportComponent } from './month-report/month-report.component';
import { WeekReportComponent } from './week-report/week-report.component';

const routes: Routes = [
  Shell.childRoutes([
    { path: 'report/dayreport', component: DayReportComponent, data: { title: marker('DayReport') } },
    { path: 'report/yearreport', component: YearReportComponent, data: { title: marker('YearReport') } },
    { path: 'report/monthreport', component: MonthReportComponent, data: { title: marker('MonthReport') } },
    { path: 'report/weekreport', component: WeekReportComponent, data: { title: marker('WeekReport') } },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class ReportRoutingModule {}
