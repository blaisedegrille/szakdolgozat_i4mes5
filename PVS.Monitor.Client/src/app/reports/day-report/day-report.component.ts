import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ReportService } from '@app/services/report.service';
import { ChartDataSets, ChartOptions, ChartTooltipItem, ChartTooltipOptions, ChartType } from 'chart.js';
import { BaseChartDirective, Color, Label, ThemeService } from 'ng2-charts';

import * as pluginDataLabels from 'chartjs-plugin-datalabels';


import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

import * as moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { Moment } from 'moment';

@Component({
  selector: 'app-day-report',
  templateUrl: './day-report.component.html',
  styleUrls: ['./day-report.component.scss']
})
export class DayReportComponent implements OnInit {
  // loader
  public isLoading = false;
  
  // picker
  public startDate = new Date(2019, 0, 1);
  public minDate: Moment;
  public maxDate: Moment;
  public chartIsVisible = false;

  // selection & date input
  public selectedPvSystem: any;
  public pvSystemInput: any[] = [];
  public pvSystemName: string;
  public date: Date;

  //unit selection
  public selectedUnit: any;
  public unitInput: any[] = ['Wh', 'kWh', 'MWh'];

  // chart
  public chartData: any[] = [];
  public deviceValue: any;
  public deviceName: string;

  public pvSystemId: string = 'ff2ad483-1254-464b-a594-2a4c61241a92';
  public dateStr: string = '2020-05-31';

  // public dateList: any[] = [];
  public valueList: any[] = [];
  public dataList: any[] = [];
  // public dateList: Date[] = [];
  public dateString: string;
  public dateLog: Date;

  //excel
  public rows: any[];
  public image: any;

  public chartTooltipOptions: ChartTooltipOptions = {
    position: 'center'
  };
  public barChartOptions: ChartOptions;
  public barChartPlugins = [pluginDataLabels];
  public barChartOptionsWh: ChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: this.setChartTitle()
    },
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Wh',
            fontFamily: 'Arial',
            fontSize: 14,
            fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 12,
        }
      }
    }
  };
  public barChartOptionskWh: ChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: this.setChartTitle()
    },
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'kWh',
            fontFamily: 'Arial',
            fontSize: 14,
            fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 12,
        }
      }
    }
  };
  public barChartOptionsMWh: ChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: this.setChartTitle()
    },
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'MWh',
            fontFamily: 'Arial',
            fontSize: 14,
            fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 12,
        }
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public color: Color[] = [
    {
      //VILÁGOSSZÜRKE
      borderColor: '#AAB2BD',
      backgroundColor: '#d7ccc8',
      hoverBackgroundColor: '#a1887f',
      borderWidth: 2
    },
    {
      //VILÁGOSKÉK - AQUA
      backgroundColor: ' #b3e5fc',
      hoverBackgroundColor: '#4DC4FF',
      borderColor: '#45B0E5',
      borderWidth: 2
    },
    {
      //ZÖLD - MINT
      backgroundColor: '#dcedc8',
      hoverBackgroundColor: '#66bb6a',
      borderColor: '#47c98a',
      borderWidth: 2
    },
    {
      //PIROS
      backgroundColor: '#ffab91 ',
      hoverBackgroundColor: '#E9573F',
      borderColor: '#E9573F',
      borderWidth: 2
    },
    {
      //SÁRGA
      backgroundColor: '#ffe082',
      hoverBackgroundColor: '#FFCE54',
      borderColor: '#F6BB42',
      borderWidth: 2
    },
    {
      //Lila
      borderColor: '#a569bd',
      backgroundColor: '#e1bee7',
      hoverBackgroundColor: '#c39bd3',
      borderWidth: 2
    }
  ];
  public barChartLegend = true;
  public barChartData: ChartDataSets[] = [];

  constructor(private reportService: ReportService, private themeService: ThemeService) { }

  ngOnInit(): void {
    this.setLoader(true);
    this.getDropdownData();
    const currentYear = moment().year();
    console.log(currentYear);
    this.minDate = moment([currentYear - 2, 0, 1]);
    this.maxDate = moment([currentYear + 0, 4, 5]);
  }

  dateInput(event: MatDatepickerInputEvent<Date>) {
    console.log(event);
    this.date = event.value;
  }

  setLoader(visible: boolean) {
    const mainContainer = document.getElementById("mainContainer");
    if (visible) {
      mainContainer.style.opacity = "0.3";
      this.isLoading = true;
    } else {
      mainContainer.style.opacity = "1";
      this.isLoading = false;
    }
  }

  setChartTitle():string {
    this.pvSystemName = '';
    this.pvSystemInput.forEach((pvSystem) => {
      if (pvSystem.pvSystemId === this.selectedPvSystem) {
        this.pvSystemName = pvSystem.name;
        console.log(this.pvSystemName);
      }
    });
    return 'Napi riport: ' + this.pvSystemName;
  }

  search() {
    this.chartIsVisible = false;
    if (this.selectedPvSystem !== undefined && this.date !== undefined && this.selectedUnit !== undefined ) {
      this.setLoader(true);
      this.getReportData();
    }
  }

  getDropdownData() {
    this.reportService.getDropdownData()
      .subscribe((response: any) => {
        this.pvSystemInput = response;
        this.setLoader(false);
      });
  }

  getReportData() {
    this.dataList = [];
    this.valueList = [];
    this.chartData = [];
    this.barChartLabels = [];
    this.reportService.GetSpecificDayData(this.selectedPvSystem, this.date).subscribe((response: any) => {
      this.dataList = response;
      this.dataList.forEach((element) => {
        this.valueList = [];
        if (this.selectedUnit === 'Wh') {
          this.deviceValue = element.value.toFixed(2);
          this.barChartOptionsWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionsWh;
        }
        if (this.selectedUnit === 'kWh') {
          this.deviceValue = (element.value / 1000).toFixed(2);
          this.barChartOptionskWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionskWh;
        }
        if (this.selectedUnit === 'MWh') {
          this.deviceValue = (element.value / 1000000).toFixed(2);
          this.barChartOptionsMWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionsMWh;
        }
        this.valueList.push(this.deviceValue);
        this.dateLog = new Date(element.logDate);
        this.dateString = this.dateLog.toLocaleDateString();
        var chart = {
          data: this.valueList,
          label: element.deviceName
        };
        this.chartData.push(chart);
      });
      this.barChartLabels.push(this.dateString);
      this.barChartData = this.chartData;
      this.chartIsVisible = true;
      this.setLoader(false);
    });
  }

  /*
  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
  */

  
  public exportExcel() {
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('Napi riport');

    let DATA = document.getElementById('chart');
    html2canvas(DATA).then(canvas => {
        this.image = canvas.toDataURL();
    }); 

    const imageId = workbook.addImage({
      base64: this.image,
      extension: 'png',
    });
    
    worksheet.addImage(imageId, {
      tl: { col: 0, row: this.barChartData.length + 2 },
      ext: { width: 736, height: 368 }
    });

    worksheet.columns = [
      { header: 'Erőmű Id', key: 'id', width: 37 },
      { header: 'Erőmű neve', key: 'name', width: 22 },
      { header: 'Inverter neve', key: 'inverter', width: 24 },
      { header: 'Dátum', key: 'datum', width: 15 },
      { header: 'Hozam', key: 'hozam', width: 15, style: { font: { name: 'Arial Black', size:10} } },
    ];

    this.rows = [];
    this.barChartData.forEach((data) => {
      this.rows.push([this.selectedPvSystem, this.pvSystemName, data.label, this.dateString, data.data + ' ' + this.selectedUnit ]);
    });

    worksheet.addRows(this.rows,"n")
    
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'DayReport.xlsx');
    });
  }
   

  public exportPDF():void {
    let DATA = document.getElementById('chart');
    html2canvas(DATA).then(canvas => {
        let fileWidth = 208;
        let fileHeight = canvas.height * fileWidth / canvas.width;
        const FILEURI = canvas.toDataURL('image/png')
        let PDF = new jsPDF('l', 'mm', 'a5');
        let position = 0;
        PDF.addImage(FILEURI, 'PNG', 0, position, fileWidth, fileHeight)
        PDF.save('day-report.pdf');
    });     
    }

  public randomize(): void {
    this.barChartType = this.barChartType === 'bar' ? 'line' : 'bar';
    this.getReportData();
  }

  chartConfig() {
    this.barChartLabels;
    this.barChartData;
  }
}

/*
{
  //VILÁGOSSZÜRKE
  borderColor: '#E6E9ED',
  backgroundColor: '#F5F7FA',
  hoverBackgroundColor: '#E6E9ED',
  borderWidth: 2
},
{
  //SZÜRKE
  borderColor: '#AAB2BD',
  backgroundColor: '#CCD1D9',
  hoverBackgroundColor: '#AAB2BD',
  borderWidth: 2
},*/
/*
{
  borderColor: '#00a6e6',
  backgroundColor: 'rgba(0, 169, 240, 0.46)',
  hoverBackgroundColor: '#00a6e6',
  borderWidth: 2
},
{
  borderColor: 'rgba(255, 255, 70, 1)',
  backgroundColor: 'rgba(255, 255, 70, 0.5)',
  hoverBackgroundColor: 'rgba(255, 255, 120, 1)',
  borderWidth: 2
},
{
  borderColor: 'rgba(255, 0, 0, 0.65)',
  backgroundColor: 'rgba(255, 0, 0, 0.44)',
  hoverBackgroundColor: 'rgba(255, 110, 110, 1)',
  borderWidth: 2
},
{
  borderColor: 'rgba(0, 255, 0, 1)',
  backgroundColor: 'rgba(114, 255, 114, 0.7)',
  hoverBackgroundColor: 'rgba(0, 255, 0, 1)',
  borderWidth: 2
},
{
  borderColor: 'rgba(230, 230, 0, 1)',
  backgroundColor: 'rgba(255, 255, 70, 0.5)',
  hoverBackgroundColor: 'rgba(255, 255, 120, 1)',
  borderWidth: 2
},
{
  borderColor: 'rgba(255, 0, 0, 0.65)',
  backgroundColor: 'rgba(255, 0, 0, 0.44)',
  hoverBackgroundColor: 'rgba(255, 0, 0, 1)',
  borderWidth: 2
},
*/
/*{
//SÖTÉTKÉK
borderColor: '#4A89DC',
backgroundColor: '#5D9CEC',
hoverBackgroundColor: '#4A89DC',
borderWidth: 2
},*/