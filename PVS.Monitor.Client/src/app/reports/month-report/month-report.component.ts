import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDatepicker, MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ReportService } from '@app/services/report.service';
import { ChartDataSets, ChartOptions, ChartTooltipOptions, ChartType } from 'chart.js';
import { Color, Label, ThemeService } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { Moment } from 'moment';

import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'YYYY. MMMM',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-month-report',
  templateUrl: './month-report.component.html',
  styleUrls: ['./month-report.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class MonthReportComponent implements OnInit {
  // loader
  public isLoading = false;

  // picker
  public date = new FormControl(moment());

  public minDate: Moment;
  public maxDate: Moment;

  // public date = new Date(moment());
  // startDate = new Date(2019, 0, 1);

  // dátum formázás
  public dateLog: Date;
  public dateString = "";

  public chartIsVisible = false;

  public selectedPvSystem: any;
  public pvSystemInput: any[] = [];

  //unit selection
  public selectedUnit: any;
  public unitInput: any[] = ['Wh', 'kWh', 'MWh'];

  public chartData: any[] = [];

  public dateFrom: Date;
  public dateTo: Date;

  public pvSystemId: string = 'ff2ad483-1254-464b-a594-2a4c61241a92';
  public pvSystemName: string;
  public dateFromStr: string = '2019-01-01';
  public dateToStr: string = '2019-01-31';

  public valueList: any[] = [];
  public dataList: any[] = [];

  public deviceValue: any;

  public chartTooltipOptions: ChartTooltipOptions = {
    position: 'center'
  };


  public barChartOptions: ChartOptions;
  public barChartPlugins = [pluginDataLabels];

  public barChartOptionsWh: ChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'Havi riport '
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Wh',
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 12,
        }
      }
    }
  };

  public barChartOptionskWh: ChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'Havi riport '
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'kWh',
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 12,
        }
      }
    }
  };

  public barChartOptionsMWh: ChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'Havi riport '
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'MWh',
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 12,
        }
      }
    }
  };

  // public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';

  public color: Color[] = [
    {
      //NARANCSSÁRGA
      borderColor: '#a569bd',
      backgroundColor: '#e1bee7',
      hoverBackgroundColor: '#c39bd3',
      borderWidth: 2
    }
  ];
  
  public random: number;
  public randomMemory: number;
  public colora: Color[] = [
    {
      //VILÁGOSKÉK - AQUA
      backgroundColor: ' #b3e5fc',
      hoverBackgroundColor: '#4DC4FF',
      borderColor: '#45B0E5',
      borderWidth: 2
    },
    {
      //ZÖLD - MINT
      backgroundColor: '#dcedc8',
      hoverBackgroundColor: '#66bb6a',
      borderColor: '#47c98a',
      borderWidth: 2
    },
    {
      //barna
      borderColor: '#AAB2BD',
      backgroundColor: '#d7ccc8',
      hoverBackgroundColor: '#a1887f',
      borderWidth: 2
    },
    {
      //Lila
      borderColor: '#a569bd',
      backgroundColor: '#e1bee7',
      hoverBackgroundColor: '#c39bd3',
      borderWidth: 2
    },
    {
      //PIROS
      backgroundColor: '#ffab91 ',
      hoverBackgroundColor: '#E9573F',
      borderColor: '#E9573F',
      borderWidth: 2
    },
    {
      //PIROS
      backgroundColor: '#ffab91 ',
      hoverBackgroundColor: '#E9573F',
      borderColor: '#E9573F',
      borderWidth: 2
    },
    {
      //VILÁGOSKÉK - AQUA
      backgroundColor: ' #b3e5fc',
      hoverBackgroundColor: '#4DC4FF',
      borderColor: '#45B0E5',
      borderWidth: 2
    },
    {
      //SÁRGA
      backgroundColor: '#ffe082',
      hoverBackgroundColor: '#FFCE54',
      borderColor: '#F6BB42',
      borderWidth: 2
    },
    {
      //SÁRGA
      backgroundColor: '#ffe082',
      hoverBackgroundColor: '#FFCE54',
      borderColor: '#F6BB42',
      borderWidth: 2
    },
    {
      //ZÖLD - MINT
      backgroundColor: '#dcedc8',
      hoverBackgroundColor: '#66bb6a',
      borderColor: '#47c98a',
      borderWidth: 2
    },
  ];

  public barChartLegend = true;

  public ctrlValue: Moment;
  public barChartData: ChartDataSets[] = [];

  constructor(private reportService: ReportService, private themeService: ThemeService) { }

  ngOnInit(): void {
    this.setLoader(true);
    this.setColor();
    this.getDropdownData();
    const currentYear = moment().year();
    this.minDate = moment([currentYear - 2, 0, 1]);
    this.maxDate = moment([currentYear + 0, 4, 5]);
    this.date.setValue(new Date(2019, 0, 4));
  }


  chosenYearHandler(normalizedYear: Moment) {
    this.ctrlValue = this.date.value;
    this.ctrlValue.year(normalizedYear.year());
    this.date.setValue(this.ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    this.ctrlValue = this.date.value;
    this.ctrlValue.month(normalizedMonth.month());
    this.date.setValue(this.ctrlValue);
    datepicker.close();
  }

  setLoader(visible: boolean) {
    const mainContainer = document.getElementById("mainContainer");
    if (visible) {
      mainContainer.style.opacity = "0.3";
      this.isLoading = true;
    } else {
      mainContainer.style.opacity = "1";
      this.isLoading = false;
    }
  }

  setChartTitle():string {
    this.pvSystemName = '';
    this.pvSystemInput.forEach((pvSystem) => {
      if (pvSystem.pvSystemId === this.selectedPvSystem) {
        this.pvSystemName = pvSystem.name;
        console.log(this.pvSystemName);
      }
    });
    return 'Havi riport: ' + this.pvSystemName;
  }

  getRandom() {
    this.random = Math.floor(Math.random() * this.colora.length);
    return this.random;
  }

  setColor() {
    this.color = [];
    this.randomMemory = this.getRandom();
    var number = this.getRandom();

    if (this.randomMemory === number) {
      while (this.randomMemory === this.random) {
        this.getRandom();
      }
    }
    let color = {
      borderColor: this.colora[this.random].borderColor,
      backgroundColor: this.colora[this.random].backgroundColor,
      hoverBackgroundColor: this.colora[this.random].hoverBackgroundColor,
      borderWidth: 2
    };
    this.color.push(color);
  }

  search() {
    this.setColor();
    this.chartIsVisible = false;
    if (this.selectedPvSystem.value !== null && this.dateFrom !== null && this.dateTo !== null) {
      this.setLoader(true);
      this.getReportData();
    }
  }

  dateFromInput(event: MatDatepickerInputEvent<Date>) {
    this.dateFrom = event.value;
  }

  dateToInput(event: MatDatepickerInputEvent<Date>) {
    this.dateTo = event.value;
  }

  getDropdownData() {
    this.reportService.getDropdownData()
      .subscribe((response: any) => {
        this.pvSystemInput = response;
        this.setLoader(false);
      });
  }

  getReportData() {
    this.dataList = [];
    this.valueList = [];
    this.barChartData = [];
    this.barChartLabels = [];
    this.reportService.GetSpecificMonthData(this.selectedPvSystem, this.date.value).subscribe((response: any) => {
      this.dataList = response;
      this.dataList.forEach((element) => {
        if (this.selectedUnit === 'Wh') {
          this.deviceValue = element.value.toFixed(2);
          this.barChartOptionsWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionsWh;
        }
        if (this.selectedUnit === 'kWh') {
          this.deviceValue = (element.value / 1000).toFixed(2);
          this.barChartOptionskWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionskWh;
        }
        if (this.selectedUnit === 'MWh') {
          this.deviceValue = (element.value / 1000000).toFixed(2);
          this.barChartOptionsMWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionsMWh;
        }
        this.valueList.push(this.deviceValue);
        // hónapok formázása
        this.dateLog = new Date(element.logDate);
        this.dateString = this.dateLog.toLocaleDateString();
        this.barChartLabels.push(this.dateString);
        // this.barChartLabels.push(element.logDate.substring(0, 10));  ez volt az eredeti

        this.pvSystemName = element.pvSystemName;
      });
      var layout = {
        layout: {
          padding: 20
        }
      };
      this.barChartData = [{
        data: this.valueList,
        label: this.pvSystemName,
      }];
      this.chartIsVisible = true;
      this.setLoader(false);
    });
  }




  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  }

  public randomize(): void {
    this.barChartType = this.barChartType === 'bar' ? 'line' : 'bar';
    this.getReportData();

  }

  chartConfig() {
    this.barChartLabels;
    this.barChartData;
  }
}
