import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDatepicker, MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ReportService } from '@app/services/report.service';
import { ChartDataSets, ChartOptions, ChartTooltipOptions, ChartType } from 'chart.js';
import { Color, Label, ThemeService } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';


import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { Moment } from 'moment';

import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { animation } from '@angular/animations';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY.',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};
@Component({
  selector: 'app-year-report',
  templateUrl: './year-report.component.html',
  styleUrls: ['./year-report.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class YearReportComponent implements OnInit {
  // loader
  public isLoading = false;

  // picker
  public date = new FormControl(moment());

  // public date = new Date(moment());
  // startDate = new Date(2019, 0, 1);

  public minDate: Moment;
  public maxDate: Moment;

  public monthValue: Moment;
  // dátum formázás


  public dateLog: Date;
  public dateString = "";

  public chartIsVisible = false;

  public selectedPvSystem: any;
  public pvSystemInput: any[] = [];

  public deviceValue: any;

  //unit selection
  public selectedUnit: any;
  public unitInput: any[] = ['Wh', 'kWh', 'MWh'];

  public chartData: any[] = [];

  public dateFrom: Date;
  public dateTo: Date;

  public pvSystemId: string = 'ff2ad483-1254-464b-a594-2a4c61241a92';
  public pvSystemName: string;
  public dateFromStr: string = '2019-01-01';
  public dateToStr: string = '2019-01-31';

  public valueList: any[] = [];
  public dataList: any[] = [];

  public chartTooltipOptions: ChartTooltipOptions = {
    position: 'center'
  };


  public barChartOptions: ChartOptions;
  public barChartPlugins = [pluginDataLabels];

  public barChartOptionsWh: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    title: {
      display: true,
      text: 'Éves riport '
    },
    tooltips: {
      mode: 'index',
      intersect: false
    },
    scales: {
      xAxes: [{
        stacked: false,
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
        ticks: {
          padding: 3
        },
        stacked: false,
        scaleLabel: {
          display: true,
          labelString: 'Wh',
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 12,
        }
      }
    }
  };

  public barChartOptionskWh: ChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'Éves riport '
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'kWh',
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 12,
        }
      }
    }
  };

  public barChartOptionsMWh: ChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'Éves riport '
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Dátum",
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'MWh',
          fontFamily: 'Arial',
          fontSize: 14,
          fontStyle: 'bold'
        }
      }],
    },
    plugins: {
      datalabels: {
        anchor: 'center',
        clamp: true,
        font: {
          size: 12,
        }
      }
    }
  };

  // public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartLabels: Label[] =
    ['2011-01-01', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31',];
  public barChartType: ChartType = 'bar';

  public color: Color[] = [
    {
      //Sötétkék
      borderColor: '#00a6e6',
      backgroundColor: 'rgba(0, 169, 240, 0.46)',
      hoverBackgroundColor: '#00a6e6',
      borderWidth: 2
    }
  ];

  public random: number;
  public randomMemory: number;
  public colora: Color[] = [
    {
      //VILÁGOSKÉK - AQUA
      backgroundColor: ' #b3e5fc',
      hoverBackgroundColor: '#4DC4FF',
      borderColor: '#45B0E5',
      borderWidth: 2
    },
    {
      //barna
      borderColor: '#AAB2BD',
      backgroundColor: '#d7ccc8',
      hoverBackgroundColor: '#a1887f',
      borderWidth: 2
    },
    {
      //Lila
      borderColor: '#a569bd',
      backgroundColor: '#e1bee7',
      hoverBackgroundColor: '#c39bd3',
      borderWidth: 2
    },
    {
      //PIROS
      backgroundColor: '#ffab91 ',
      hoverBackgroundColor: '#E9573F',
      borderColor: '#E9573F',
      borderWidth: 2
    },
    {
      //SÁRGA
      backgroundColor: '#ffe082',
      hoverBackgroundColor: '#FFCE54',
      borderColor: '#F6BB42',
      borderWidth: 2
    },
    {
      //ZÖLD - MINT
      backgroundColor: '#dcedc8',
      hoverBackgroundColor: '#66bb6a',
      borderColor: '#47c98a',
      borderWidth: 2
    },
  ];



  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];

  constructor(private reportService: ReportService, private themeService: ThemeService) { }

  ngOnInit(): void {
    this.setLoader(true);
    this.setColor();
    this.getDropdownData();
    const currentYear = moment().year();
    this.minDate = moment([currentYear - 2, 0, 1]);
    this.maxDate = moment([currentYear + 0, 5, 5]);
  }

  setLoader(visible: boolean) {
    const mainContainer = document.getElementById("mainContainer");
    if (visible) {
      mainContainer.style.opacity = "0.3";
      this.isLoading = true;
    } else {
      mainContainer.style.opacity = "1";
      this.isLoading = false;
    }
  }

  setChartTitle(): string {
    this.pvSystemName = '';
    this.pvSystemInput.forEach((pvSystem) => {
      if (pvSystem.pvSystemId === this.selectedPvSystem) {
        this.pvSystemName = pvSystem.name;
        console.log(this.pvSystemName);
      }
    });
    return 'Éves riport: ' + this.pvSystemName;
  }

  getRandom() {
    this.random = Math.floor(Math.random() * this.colora.length);
    return this.random;
  }

  setColor() {
    this.color = [];
    this.randomMemory = this.getRandom();
    var number = this.getRandom();

    if (this.randomMemory === number) {
      while (this.randomMemory === this.random) {
        this.getRandom();
      }
    }
    let color = {
      borderColor: this.colora[this.random].borderColor,
      backgroundColor: this.colora[this.random].backgroundColor,
      hoverBackgroundColor: this.colora[this.random].hoverBackgroundColor,
      borderWidth: 2
    };
    this.color.push(color);
  }

  search() {
    this.setColor();
    this.chartIsVisible = false;
    if (this.selectedPvSystem !== undefined && this.selectedUnit !== undefined && this.date.value !== undefined) {
      this.setLoader(true);
      this.getReportData();
    }
  }

  getDropdownData() {
    this.reportService.getDropdownData()
      .subscribe((response: any) => {
        this.pvSystemInput = response;
        this.setLoader(false);
      });
  }

  getReportData() {
    this.dataList = [];
    this.valueList = [];
    this.barChartData = [];
    this.barChartLabels = [];
    this.reportService.GetSpecificYearData(this.selectedPvSystem, this.date.value).subscribe((response: any) => {
      this.dataList = response;
      this.dataList.forEach((element) => {
        if (this.selectedUnit === 'Wh') {
          this.deviceValue = element.value.toFixed(2);
          this.barChartOptionsWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionsWh;
        }
        if (this.selectedUnit === 'kWh') {
          this.deviceValue = (element.value / 1000).toFixed(2);
          this.barChartOptionskWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionskWh;
        }
        if (this.selectedUnit === 'MWh') {
          this.deviceValue = (element.value / 1000000).toFixed(2);
          this.barChartOptionsMWh.title.text = this.setChartTitle();
          this.barChartOptions = this.barChartOptionsMWh;
        }
        this.valueList.push(this.deviceValue);
        // hónapok formázása
        //this.dateLog = moment.months(element.logDate);
        this.dateString = element.month.toString();

        this.dateString = moment(this.dateString, 'M').locale('Hu').format('MMMM');

        // this.monthValue = moment('5', 'M').format('MMMM');
        this.barChartLabels.push(this.dateString);
        // this.barChartLabels.push(element.logDate.substring(0, 10));  ez volt az eredeti

        this.pvSystemName = element.pvSystemName;
      });
      var layout = {
        layout: {
          padding: 20
        }
      };
      this.barChartData = [{
        data: this.valueList,
        label: this.pvSystemName,
        fill: true
      }];
      this.chartIsVisible = true;
      this.setLoader(false);
    });
  }


  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    datepicker.close();
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  }

  public randomize(): void {
    this.barChartType = this.barChartType === 'bar' ? 'line' : 'bar';
    this.getReportData();

  }

  chartConfig() {
    this.barChartLabels;
    this.barChartData;
  }
}