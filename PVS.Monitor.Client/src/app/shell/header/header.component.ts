import { Title } from '@angular/platform-browser';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';

import { MenuItem } from 'primeng/api';

import { AuthenticationService, CredentialsService } from '@app/auth';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() sidenav!: MatSidenav;

  items: MenuItem[];
  roles: any[] = [];
  reportIsVisible = false;
  maintenanceIsVisible = false;
  serviceIsVisible = false;

  // public items: any[] = [];

  constructor(
    private router: Router,
    private titleService: Title,
    private authenticationService: AuthenticationService,
    private credentialsService: CredentialsService
  ) {}

  ngOnInit() {
    this.setMenuRoles();
  }

  logout() {
    this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
  }

  get username(): string | null {
    const credentials = this.credentialsService.credentials;
    return credentials ? credentials.name : null;
  }

  get title(): string {
    return this.titleService.getTitle();
  }

  setMenuRoles() {
    this.roles = this.credentialsService.credentials.roles;
    console.log();
    if (this.roles.includes('ServiceUser'))
    {
      this.serviceIsVisible = true;
    }
    if (this.roles.includes('Admin'))
    {
      this.reportIsVisible = true;
    }
    if (this.roles.includes('ReportUser'))
    {
      this.maintenanceIsVisible = true;
    }
    this.setItem();
  }

  setItem() {
    this.items = [
      {
        label: 'Riportok',
        icon: 'pi pi-fw pi-chart-line',
        disabled: !this.reportIsVisible,
        visible: this.reportIsVisible,
        items: [
          {
            label: 'Időszakos lekérdezések',
            icon: 'pi pi-fw pi-chart-bar',
            items: [
              {
                label: 'Éves riport',
                icon: 'pi pi-fw pi-calendar',
                routerLink: 'report/yearreport',
              },
              {
                separator: true,
              },
              {
                label: 'Havi riport',
                icon: 'pi pi-fw pi-calendar',
                routerLink: 'report/monthreport',
              },
              {
                separator: true,
              },
              {
                label: 'Heti riport',
                icon: 'pi pi-fw pi-calendar-minus',
                routerLink: 'report/weekreport',
              },
              {
                separator: true,
              },
              {
                label: 'Napi riport',
                icon: 'pi pi-fw pi-calendar-times',
                routerLink: 'report/dayreport',
              },
            ],
          },
          /*{
            label: 'Hozam riport',
            icon: 'pi pi-fw pi-money-bill',
          },
          {
            separator: true,
          },
          {
            label: 'Export',
            icon: 'pi pi-fw pi-external-link',
          },*/
        ],
      },
      {
        label: 'Karbantartás',
        icon: 'pi pi-fw pi-cog',
        disabled: !this.maintenanceIsVisible,
        visible: this.maintenanceIsVisible,
        items: [
          {
            separator: true,
          },
          {
            label: 'Felhasználók',
            icon: 'pi pi-fw pi-id-card',
            routerLink: 'maintenance/users',
          },
          {
            separator: true,
          },
         /*{
            label: 'Csoportok',
            icon: 'pi pi-fw pi-users',
            routerLink: 'maintenance/groups',
          },
          {
            label: 'Erőművek',
            icon: 'pi pi-fw pi-sitemap',
            routerLink: '/pvsystems',
          },*/
        ],
      },
      {
        label: 'Üzentközpont',
        icon: 'pi pi-fw pi-inbox',
        disabled: !this.serviceIsVisible,
        visible: this.serviceIsVisible,
        items: [
          {
            label: 'Szervíz üzenetek',
            icon: 'pi pi-fw pi-comments',
            routerLink: '/servicemessages'
          },
        ],
      },
      {
        label: 'Információ',
        icon: 'pi pi-fw pi-book',
        routerLink: '/information'
      },
    ];
  }
}
