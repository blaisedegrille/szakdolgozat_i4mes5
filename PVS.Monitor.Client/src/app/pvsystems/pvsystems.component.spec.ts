import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PvSystemsComponent } from './pvsystems.component';

describe('PvSystemsComponent', () => {
  let component: PvSystemsComponent;
  let fixture: ComponentFixture<PvSystemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PvSystemsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PvSystemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
