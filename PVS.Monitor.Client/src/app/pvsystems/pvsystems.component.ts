import { Component, OnInit } from '@angular/core';
import { Address } from '@app/models/address';
import { PvSystem } from '@app/models/pvsystem';
import { PvSystemService } from '@app/services/pvsystem.service';
import { environment } from '@env/environment';
import { MessageService } from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-pvsystems',
  templateUrl: './pvsystems.component.html',
  styleUrls: ['./pvsystems.component.scss'],
})
export class PvSystemsComponent implements OnInit {
  version: string | null = environment.version;

  public address: Address;
  public system: PvSystem;
  public PvSystemList: PvSystem[] = [];

  public installDate: Date;
  public installDateString: string;
  
  public selectedPvSystem: PvSystem = null;

  public systemList: any[] = [];
  public sales: any[];

  constructor(private pvSystemService: PvSystemService, private primengConfig: PrimeNGConfig) {}

  ngOnInit(): void {
    this.getPvSystemList();
    //this.setSales();
    this.primengConfig.ripple = true;
  }

  setSales() {
    this.sales = [
      {
        brand: 'Apple',
        lastYearSale: '51%',
        thisYearSale: '40%',
        lastYearProfit: '$54,406.00',
        thisYearProfit: '$43,342',
      },
      {
        brand: 'Samsung',
        lastYearSale: '83%',
        thisYearSale: '96%',
        lastYearProfit: '$423,132',
        thisYearProfit: '$312,122',
      },
      {
        brand: 'Microsoft',
        lastYearSale: '38%',
        thisYearSale: '5%',
        lastYearProfit: '$12,321',
        thisYearProfit: '$8,500',
      },
      {
        brand: 'Philips',
        lastYearSale: '49%',
        thisYearSale: '22%',
        lastYearProfit: '$745,232',
        thisYearProfit: '$650,323,',
      },
      {
        brand: 'Song',
        lastYearSale: '17%',
        thisYearSale: '79%',
        lastYearProfit: '$643,242',
        thisYearProfit: '500,332',
      },
      {
        brand: 'LG',
        lastYearSale: '52%',
        thisYearSale: ' 65%',
        lastYearProfit: '$421,132',
        thisYearProfit: '$150,005',
      },
      {
        brand: 'Sharp',
        lastYearSale: '82%',
        thisYearSale: '12%',
        lastYearProfit: '$131,211',
        thisYearProfit: '$100,214',
      },
      {
        brand: 'Panasonic',
        lastYearSale: '44%',
        thisYearSale: '45%',
        lastYearProfit: '$66,442',
        thisYearProfit: '$53,322',
      },
      {
        brand: 'HTC',
        lastYearSale: '90%',
        thisYearSale: '56%',
        lastYearProfit: '$765,442',
        thisYearProfit: '$296,232',
      },
      {
        brand: 'Toshiba',
        lastYearSale: '75%',
        thisYearSale: '54%',
        lastYearProfit: '$21,212',
        thisYearProfit: '$12,533',
      },
    ];
  }

  getPvSystemList() {
    this.pvSystemService.getPvSystemsList().subscribe((response: any) => {
      this.systemList = response;
      this.getSystemData();
    });
  }

  getSystemData() {
    this.systemList.forEach((system) => {
      this.address = {
        country: system.country,
        zipCode: system.zipCode,
        street: system.street,
        city: system.city,
        state: system.state,
      };
      this.installDate = new Date(system.installationDate);

      this.system = {
        id: system.id,
        pvSystemId: system.pvSystemId,
        name: system.name,
        address: this.address,
        pictureURL: system.pictureUrl,
        peakPower: system.peakPower,
        installationDate: this.installDate.toLocaleDateString(),
        //installationDate: system.installationDate,
        lastImport: system.lastImport,
        meteoData: system.meteoData,
        timeZone: system.timeZone,
      };
      // this.dateString = this.dateLog.toLocaleDateString();
      this.PvSystemList.push(this.system);
    });
  }

  onRowSelect(event: any) {
    console.log(event);
  }

  onRowUnselect(event: any) {
    console.log(event.data.id);
  }
}
