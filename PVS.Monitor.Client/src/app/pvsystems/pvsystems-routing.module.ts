import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { PvSystemsComponent } from './pvsystems.component';

const routes: Routes = [
  Shell.childRoutes([
    { path: '', redirectTo: '/pvsystems', pathMatch: 'full' },
    { path: 'pvsystems', component: PvSystemsComponent, data: { title: marker('PvSystems') } },
  ]),
];

// Routes = [{ path: 'power-plants', component: PowerPlantsComponent, data: { title: marker('PowerPlants') } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class PvSystemsRoutingModule {}
