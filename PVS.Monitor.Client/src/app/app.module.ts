import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';

import { CoreModule } from '@core';
import { SharedModule } from '@shared';
import { AuthModule } from '@app/auth';
import { HomeModule } from './home/home.module';
import { ShellModule } from './shell/shell.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MaintenanceModule } from './maintenance/maincenance.module';
import { PvSystemsModule } from './pvsystems/pvsystems.module';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ChartModule } from 'primeng/chart';
import { ReportModule } from './reports/report.module';
import { MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ServiceMessagesModule } from './service-messages/service-message.module';
import { JwtInterceptor } from './@core/http/jwt.Interceptor';
import { AboutRoutingModule } from './about/about-routing.module';
import { InformationRoutingModule } from './information/information-routing.module';
import { InformationModule } from './information/information.module';

export const MY_NATIVE_DATE_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    BrowserAnimationsModule,
    MaterialModule,
    CoreModule,
    SharedModule,
    ShellModule,
    HomeModule,
    AuthModule,
    PvSystemsModule,
    MaintenanceModule,
    ReportModule,
    ChartsModule,
    ChartModule,
    ServiceMessagesModule,
    InformationModule,
    AppRoutingModule, // must be imported as the last module as it contains the fallback route
  ],
  declarations: [AppComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    {
      provide: LOCALE_ID,
      useValue: 'hu-HU',
    },
    {provide: MAT_DATE_LOCALE, useValue: 'hu-HU'},
    {provide: LOCALE_ID, useValue: 'hu'},
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: ['LL'],
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      },
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

