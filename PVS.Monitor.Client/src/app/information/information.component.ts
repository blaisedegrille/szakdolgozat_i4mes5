import { Component, OnInit } from '@angular/core';
import { Address } from '@app/models/address';
import { PvSystem } from '@app/models/pvsystem';
import { PvSystemService } from '@app/services/pvsystem.service';
import { environment } from '@env/environment';
import { MessageService } from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss'],
})
export class InformationComponent implements OnInit {

  ngOnInit(): void {
  }
  
}
