import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { InformationComponent } from './information.component';

const routes: Routes = [
  Shell.childRoutes([
    { path: '', redirectTo: '/pvsystems', pathMatch: 'full' },
    { path: 'information', component: InformationComponent, data: { title: marker('Information') } },
  ]),
];

// Routes = [{ path: 'power-plants', component: PowerPlantsComponent, data: { title: marker('PowerPlants') } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class InformationRoutingModule {}
