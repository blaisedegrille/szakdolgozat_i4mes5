import { Component, OnInit } from '@angular/core';
import { PvSystem } from '@app/models/pvsystem';
import { Address } from '@app/models/address';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label, MultiDataSet, SingleDataSet, Color } from 'ng2-charts';
import { PvSystemService } from '@app/services/pvsystem.service';
import { PrimeNGConfig } from 'primeng/api';
import { ReportService } from '@app/services/report.service';
import * as moment from 'moment';
import { TableData } from '@app/models/tableData';


export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  // dialog
  pvInfoDialog = false;
  dialogHeader = '';

  // datatable
  public address: Address;
  public system: PvSystem;
  public PvSystemList: PvSystem[] = [];

  public installDate: Date;
  public installDateString: string;

  public selectedPvSystem: PvSystem = null;

  public systemList: any[] = [];
  public sales: any[];

  // datateble end


  //year selection
  public selectedYear: number;
  public yearInput: any[] = [2019, 2020, 2021];

  //unit selection
  public selectedUnit: string;
  public unitInput: any[] = ['Wh', 'kWh', 'MWh'];
  public deviceValue: any;

  // profit, CO2
  public currencyUnit: any;
  public profitValue: any;
  public co2Unit: any;
  public co2Value: any;
  public dataSource: TableData[] = [];
  public tableData: TableData;
  public displayedColumns: string[] = ['type', 'value', 'unit'];



  tiles: Tile[] = [
    { text: 'One', cols: 1, rows: 1, color: 'lightblue' },
    { text: 'Two', cols: 1, rows: 1, color: 'lightgreen' },
    { text: 'Three', cols: 1, rows: 1, color: 'lightpink' },
    { text: 'Four', cols: 1, rows: 1, color: 'lightyellow' },
  ];

  // chartok
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [32, 22, 100, 40, 53, 32, 67], label: 'Series B' },
    { data: [55, 34, 69, 67, 87, 43, 55], label: 'Series C' },
    { data: [55, 34, 69, 67, 87, 43, 55], label: 'Series D' }
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: ChartOptions = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'red',
      backgroundColor: 'rgba(255,0,0,0.3)',
      hoverBackgroundColor: 'red'
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins: any = [];

  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins: any = [];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];


  public doughnutChartLabels: Label[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData: SingleDataSet = [350, 450, 100];
  public doughnutChartType: ChartType = 'doughnut';

  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [['Download', 'Sales'], ['In', 'Store', 'Sales'], ['Mail Sales']];
  public pieChartData: SingleDataSet = [300, 500, 100];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins: any = [];

  public donutColors = [
    {
      backgroundColor: [
        "#b3e5fc",
        '#dcedc8',
        "#e1bee7",
        "#ffab91",
        "#ffe082",
        "#d7ccc8",
      ]
    }
  ];

  public color: Color[] = [
    {
      //VILÁGOSKÉK - AQUA
      backgroundColor: ' #b3e5fc',
      hoverBackgroundColor: '#4DC4FF',
      borderColor: '#45B0E5',
      borderWidth: 2
    },
    {
      //barna
      borderColor: '#AAB2BD',
      backgroundColor: '#d7ccc8',
      hoverBackgroundColor: '#a1887f',
      borderWidth: 2
    },
    {
      //Lila
      borderColor: '#a569bd',
      backgroundColor: '#e1bee7',
      hoverBackgroundColor: '#c39bd3',
      borderWidth: 2
    },
    {
      //PIROS
      backgroundColor: '#ffab91 ',
      hoverBackgroundColor: '#E9573F',
      borderColor: '#E9573F',
      borderWidth: 2
    },
    {
      //SÁRGA
      backgroundColor: '#ffe082',
      hoverBackgroundColor: '#FFCE54',
      borderColor: '#F6BB42',
      borderWidth: 2
    },
    {
      //ZÖLD - MINT
      backgroundColor: '#dcedc8',
      hoverBackgroundColor: '#66bb6a',
      borderColor: '#47c98a',
      borderWidth: 2
    },
  ];

  // public bgcols =  [{backgroundColor: ["#e84351", "#434a54", "#3ebf9b", "#4d86dc", "#f3af37"]}];

  constructor(private pvSystemService: PvSystemService, private primengConfig: PrimeNGConfig, private reportService: ReportService) { }

  ngOnInit(): void {
    this.getPvSystemList();
    this.primengConfig.ripple = true;
    this.selectedYear = 2019;
    this.selectedUnit = 'kWh';
  }

  getPvSystemList() {
    this.pvSystemService.getPvSystemsList().subscribe((response: any) => {
      this.systemList = response;
      this.getSystemData();
    });
  }

  getSystemData() {
    this.systemList.forEach((system) => {
      this.address = {
        country: system.country,
        zipCode: system.zipCode,
        street: system.street,
        city: system.city,
        state: system.state,
      };
      this.installDate = new Date(system.installationDate);

      this.system = {
        id: system.id,
        pvSystemId: system.pvSystemId,
        name: system.name,
        address: this.address,
        pictureURL: system.pictureUrl,
        peakPower: system.peakPower,
        installationDate: this.installDate.toLocaleDateString(),
        //installationDate: system.installationDate,
        lastImport: system.lastImport,
        meteoData: system.meteoData,
        timeZone: system.timeZone,
      };
      // this.dateString = this.dateLog.toLocaleDateString();
      this.PvSystemList.push(this.system);
    });
  }

  onRowSelect(event: any) {
    console.log(event);
  }

  onRowUnselect(event: any) {
    console.log(event.data.id);
  }

  openPvInfoDialog(pvsystem: PvSystem) {
    this.dataSource = [];
    this.pvInfoDialog = true;
    this.dialogHeader = pvsystem.name;
    this.selectedPvSystem = pvsystem;
    this.GetSpecificYearDeviceData(2019);
    this.GetSpecificYearProfit(2019);
    this.GetSpecificYearCO2(2019);
    console.log(this.dataSource);
  }

  search() {
    this.dataSource = [];
    this.GetSpecificYearDeviceData(this.selectedYear);
    this.GetSpecificYearProfit(this.selectedYear);
    this.GetSpecificYearCO2(this.selectedYear);
  }

  GetSpecificYearDeviceData(year: number) {
    this.doughnutChartLabels = [];
    this.doughnutChartData = [];
    var date = new Date(year, 1, 1);
    console.log(date);
    this.reportService.GetSpecificYearDeviceData(this.selectedPvSystem.pvSystemId, date).subscribe((response: any) => {
      console.log(response)
      response.forEach((element: any) => {
        if (this.selectedUnit === 'Wh') {
          this.deviceValue = element.value.toFixed(2);
          this.doughnutChartData.push(this.deviceValue);
        }
        if (this.selectedUnit === 'kWh') {
          this.deviceValue = (element.value / 1000).toFixed(2);
          this.doughnutChartData.push(this.deviceValue);
        }
        if (this.selectedUnit === 'MWh') {
          this.deviceValue = (element.value / 1000000).toFixed(2);
          this.doughnutChartData.push(this.deviceValue);
        }
        this.doughnutChartLabels.push(element.deviceName);
      });
    });
  }

  GetSpecificYearProfit(year: number) {
    console.log(year);
    this.pvSystemService.GetSpecificYearProfit(this.selectedPvSystem.pvSystemId, year.toString()).subscribe((response: any) => {
      response.forEach((element: any) => {
        this.currencyUnit = element.unit;
        this.profitValue = element.value;
        this.tableData = {
          type: 'Profit',
          value: this.profitValue, 
          unit: this.currencyUnit
        };
        this.dataSource.push(this.tableData);
        console.log(this.dataSource);
      });
    });
  }

  GetSpecificYearCO2(year: number) {
    console.log(year);
    this.pvSystemService.GetSpecificYearCO2(this.selectedPvSystem.pvSystemId, year.toString()).subscribe((response: any) => {
      response.forEach((element: any) => {
        this.co2Unit = element.unit;
        this.co2Value = -element.value;
        this.tableData = {
          type: 'CO2 kibocsátás',
          value: this.co2Value, 
          unit: this.co2Unit 
        };
        this.dataSource.push(this.tableData);
        console.log(this.dataSource);
        console.log(this.co2Value);
        console.log(this.co2Unit);
      });
    });
  }


  /*

  // profit, CO2
  public currencyUnit: string;
  public profitValue: number;
  public co2Unit: string;
  public co2Value: number;


    public doughnutChartLabels: Label[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData: MultiDataSet = [

  */


}






