import { Role } from "./role";

export class EditUser {
  id: number;
  loginName: string;
  name: string;
  password: string;
  email: string;
  phone: string;
  roles: Role[];
}
