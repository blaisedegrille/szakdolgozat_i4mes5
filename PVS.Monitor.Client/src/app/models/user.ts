import { Role } from "./role";

export class User {
  id: number;
  name: string;
  loginName: string;
  email: string;
  phone: string;
  roles: string;
  active: boolean;
  token: string;
  refreshToken: string;
  refreshTokenExpiration: Date;
}
