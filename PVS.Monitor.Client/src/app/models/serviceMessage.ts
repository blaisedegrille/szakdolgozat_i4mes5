import { Address } from './address';

export class ServiceMessage {
  id: number;
  pvSystemId: string;
  pvSystemName: string;
  deviceId: string;
  deviceName: string;
  deviceType: string;
  stateType: string;
  stateCode: number;
  logDateTime: string;
  messageText: string;
}
