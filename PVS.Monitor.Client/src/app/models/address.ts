export class Address {
  country: string;
  zipCode: number;
  street: string;
  city: string;
  state: string;
}
