import { Address } from './address';

export class PvSystem {
  id: number;
  pvSystemId: string;
  name: string;
  address: Address;
  pictureURL: string;
  peakPower: number;
  installationDate: string;
  lastImport: Date;
  meteoData: string;
  timeZone: string;
}
