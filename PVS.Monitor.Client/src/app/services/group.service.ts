import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class GroupService {
  constructor(private router: Router, private http: HttpClient) {}

  public getGroupList() {
    // let headers = new HttpHeaders();
    // headers = this.createAuthorizationHeader(headers);
    const url = `/api/Group/GetGroupList`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response.groupList;
      })
    );
  }

  public createGroup(item: any): Observable<any> {
    const url = `/api/Group/CreateGroup`;
    return this.http.post(url, { group: item });
  }

  public updateGroup(item: any): Observable<any> {
    const url = `api/Group/UpdateGroup`;
    return this.http.put(url, { group: item });
  }

  public getGroupUsers(groupId: number) {
    const url = `api/Group/GetGroupUsers?groupId=${groupId}`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response.groupUsers;
      })
    );
  }

  public bindGroupUser(groupId: number, item: any[]): Observable<any> {
    const url = `api/Group/BindGroupUser`;
    return this.http.put(url, { groupId: groupId, groupUsers: item });
  }

  public getGroupPlants(groupId: number) {
    const url = `api/Group/GetGroupPlants?groupId=${groupId}`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response.groupPlants;
      })
    );
  }

  public bindGroupPlant(groupId: number, item: any[]): Observable<any> {
    const url = `api/Group/BindGroupPlant`;
    return this.http.put(url, { groupId: groupId, groupPlants: item });
  }
}
