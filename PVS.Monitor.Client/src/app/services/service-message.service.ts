import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ServiceMessageService {
  constructor(private http: HttpClient) {}

  public getPvSystemMessage(pvSystemId: string) {
    const url = `/api/ServiceMessage/GetPvSystemMessage?pvSystemId=${pvSystemId}`;
    return this.http.get(url).pipe(
      map((response: any) => {
        console.log(response);
        return response;
      })
    );
  }

  public getGroupList() {
    const url = `/api/Group/GetGroupList`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response.groupList;
      })
    );
  }
}
