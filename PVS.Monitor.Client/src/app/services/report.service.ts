import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ReportService {
  constructor(private http: HttpClient) {}

  public GetAllDayInRangeData(pvSystemId: string, dateFrom: Date, dateTo: Date): Observable<any> {
    const url = `/api/DeviceData/GetAllDayInRangeData`;
    return this.http.post(url, { pvSystemId: pvSystemId, dateFrom: dateFrom, dateTo: dateTo });
  }

  public GetSpecificDayData(pvSystemId: string, date: Date): Observable<any> {
    const url = `/api/DeviceData/GetSpecificDayData`;
    return this.http.post(url, { pvSystemId: pvSystemId, dateFrom: date });
  }

  public GetSpecificMonthData(pvSystemId: string, date: Date): Observable<any> {
    const url = `/api/DeviceData/GetSpecificMonthData`;
    return this.http.post(url, { pvSystemId: pvSystemId, dateFrom: date });
  }

  public GetSpecificYearData(pvSystemId: string, date: Date): Observable<any> {
    const url = `/api/DeviceData/GetSpecificYearData`;
    return this.http.post(url, { pvSystemId: pvSystemId, dateFrom: date });
  }

  public GetSpecificYearDeviceData(pvSystemId: string, date: Date): Observable<any> {
    const url = `/api/DeviceData/GetSpecificYearDeviceData`;
    return this.http.post(url, { pvSystemId: pvSystemId, dateFrom: date });
  }
  
  public getDropdownData() {
    const url = `/api/PvSystem/GetPvSystemDropDown`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response.pvSystemListDropDownList;
      })
    );
  }


/*
  public GetSpecificDayData(pvSystemId: string, date: string) {
    const url = `/api/DeviceData/GetSpecificDayData?pvSystemId=${pvSystemId}&date=${date}`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

    GetAllDayInRangeData(pvSystemId: string, dateFrom: string, dateTo: string) {
    const url = `/api/DeviceData/GetAllDayInRangeData?pvSystemId=${pvSystemId}&from=${dateFrom}&to=${dateTo}`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response;
      })
    );
  }
*/
}
