import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PvSystemService {
  constructor(private router: Router, private http: HttpClient) {}

  /*
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      accessKeyId: 'FKIA0B3B0F5FC5A34EA598C2E0D17FCDC172',
      accessKeyValue: '4ea47b00-58e0-4f2f-9f11-efabce0b2593',
    }),
  };
  */
 
  public getPvSystemsList() {
    // let headers = new HttpHeaders();
    // headers = this.createAuthorizationHeader(headers);
    const url = `/api/PvSystem/GetPvSystemList`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response.pvSystemList;
      })
    );
  }

  
  public GetSpecificYearProfit(pvSystemId: string, year: string): Observable<any> {
    const url = `/api/PvSystem/GetSpecificYearProfit`;
    return this.http.post(url, { pvSystemId: pvSystemId, year: year });
  }
  
  public GetSpecificYearCO2(pvSystemId: string, year: string): Observable<any> {
    const url = `/api/PvSystem/GetSpecificYearCO2`;
    return this.http.post(url, { pvSystemId: pvSystemId, year: year });
  }



}
