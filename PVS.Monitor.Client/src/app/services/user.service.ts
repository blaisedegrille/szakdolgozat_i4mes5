import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private router: Router, private http: HttpClient) {}

  public getUserList() {
    const url = `/api/User/GetUserList`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  public getUserByLoginName(loginName: string) {
    const url = `/api/User/GetUserByLoginName?loginName=${loginName}`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  public createUser(item: any): Observable<any> {
    const url = `/api/User/CreateUser`;
    return this.http.post(url, { newUser: item });
  }

  public updateUser(item: any): Observable<any> {
    const url = `/api/User/UpdateUser`;
    return this.http.post(url, { editUser: item });
  }

  public deleteUser(userId: number): Observable<any> {
    const url = `/api/User/DeleteUser?id=${userId}`;
    return this.http.post(url, {} );
  }

}
