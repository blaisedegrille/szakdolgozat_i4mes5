import { Component, OnInit } from '@angular/core';
import { Address } from '@app/models/address';
import { ServiceMessage } from '@app/models/serviceMessage';
import { ReportService } from '@app/services/report.service';
import { ServiceMessageService } from '@app/services/service-message.service';


@Component({
  selector: 'app-service-message',
  templateUrl: './service-message.component.html',
  styleUrls: ['./service-message.component.scss'],
})
export class ServiceMessagesComponent implements OnInit {

  // selection
  public selectedPvSystem: any;
  public pvSystemInput: any[] = [];
  public pvSystemName = "";
  public date: Date;
  public dateString = "";


  public address: Address;
  public serviceMessage: ServiceMessage;
  public ServiceMessageList: ServiceMessage[] = [];

  public selectedServiceMessage: ServiceMessage = null;
  public systemLabelVisible = false;
  public noData = false;

  public messageList: any[] = [];

  constructor(private serviceMessageService: ServiceMessageService, private reportService: ReportService) {}

  ngOnInit(): void {
    this.getDropdownData();
  }

  getDropdownData() {
    this.reportService.getDropdownData()
      .subscribe((response: any) => {
        this.pvSystemInput = response;
      });
  }

  search() {
    this.messageList = [];
    console.log(this.selectedPvSystem)
    this.serviceMessageService.getPvSystemMessage(this.selectedPvSystem).subscribe((response: any) => {
      this.messageList = response;
      console.log(this.messageList)
      this.getSystemData();
    });
  }

  getSystemData() {
    this.pvSystemName = "";
    this.ServiceMessageList = [];
    if (this.messageList.length === 0) {
      this.pvSystemName = 'Nincs Adat';
      this.noData = true;
    } else {
      this.noData = false;
      this.messageList.forEach((message) => {
        this.date = new Date(message.logDateTime);
        this.dateString = this.date.toLocaleString();
        // console.log(this.date.toDateString());
        this.serviceMessage = {
          id: message.id,
          pvSystemId: message.pvSystemId,
          pvSystemName: message.pvSystemName,
          deviceId: message.deviceId,
          deviceName: message.deviceName,
          deviceType: message.deviceType,
          logDateTime: this.dateString,
          messageText: message.messageText,
          stateCode: message.stateCode,
          stateType: message.stateType,
        };
        this.pvSystemName = this.serviceMessage.pvSystemName;
        this.ServiceMessageList.push(this.serviceMessage);
      });
    }
    this.systemLabelVisible = true;
  }

  onRowSelect(event: any) {
    console.log(event);
  }

  onRowUnselect(event: any) {
    console.log(event.data.id);
  }
}
/*
this.userMaintainerService.getUserList().subscribe((response: any) => {
  this.userList = response;
  this.userListGridData = process(this.userList, this.userListGridState);
  console.log(this.userListGridData);
});
*/
