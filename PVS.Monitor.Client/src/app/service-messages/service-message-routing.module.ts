import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { ServiceMessagesComponent } from './service-message.component';

const routes: Routes = [
  Shell.childRoutes([
    { path: '', redirectTo: '/servicemessages', pathMatch: 'full' },
    { path: 'servicemessages', component: ServiceMessagesComponent, data: { title: marker('ServiceMessages') } },
  ]),
];

// Routes = [{ path: 'power-plants', component: PowerPlantsComponent, data: { title: marker('PowerPlants') } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class ServiceMessagesRoutingModule {}
