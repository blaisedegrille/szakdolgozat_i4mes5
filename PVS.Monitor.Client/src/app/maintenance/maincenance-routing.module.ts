import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

import { Shell } from '@app/shell/shell.service';
import { UserMaintainerComponent } from './user-maintainer/user-maintainer.component';
import { GroupMaintainerComponent } from './group-maintainer/group-maintainer.component';

const routes: Routes = [
  Shell.childRoutes([
    { path: 'maintenance/users', component: UserMaintainerComponent, data: { title: marker('Users') } },
    { path: 'maintenance/groups', component: GroupMaintainerComponent, data: { title: marker('Groups') } },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class MaintenanceRoutingModule {}
