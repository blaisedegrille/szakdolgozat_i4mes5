import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupMaintainerComponent } from './group-maintainer.component';

describe('GroupMaintainerComponent', () => {
  let component: GroupMaintainerComponent;
  let fixture: ComponentFixture<GroupMaintainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroupMaintainerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupMaintainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
