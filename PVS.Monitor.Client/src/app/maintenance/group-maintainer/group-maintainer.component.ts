import { Component, OnInit } from '@angular/core';
import { Group } from '@app/models/group';
import { GroupService } from '@app/services/group.service';
// import { GroupMaintainerService } from '@app/services/group-maintainer.service';

@Component({
  selector: 'app-group-maintainer',
  templateUrl: './group-maintainer.component.html',
  styleUrls: ['./group-maintainer.component.scss'],
})
export class GroupMaintainerComponent implements OnInit {
  groupList: any[] = [];
  GroupList: Group[] = [];
  selectedGroup: Group;
  group: Group;

  clonedGroups: { [s: string]: Group } = {};

  // dialog
  groupDialog: boolean;
  submitted: boolean;
  message: string;

  constructor(private groupService: GroupService) {
    //  private userMaintainerService: UserMaintainerService,
  }

  ngOnInit() {
    this.getGroupList();
  }

  getGroupList() {
    this.GroupList = [];
    this.groupService.getGroupList().subscribe((response: any) => {
      this.groupList = response;
      this.groupList.forEach((element) => {
        this.group = {
          id: element.id,
          name: element.name,
          description: element.description,
          active: element.active,
        };
        this.GroupList.push(this.group);
      });
    });
  }

  onEdit(group: Group) {
    console.log(group);
  }

  onRowEditInit(group: Group) {
    this.clonedGroups[group.id] = { ...group };
  }

  onRowEditSave(group: Group) {
    /* if (group.price > 0) {
       delete this.clonedGroups[product.id];
       this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Product is updated' });
     }
     else {
       this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Invalid Price' });
     }
     */
  }

  onRowEditCancel(group: Group, index: number) {
    this.GroupList[index] = this.clonedGroups[group.id];
    delete this.clonedGroups[group.id];
  }

  openNew() {
    this.group = {
      id: 0,
      name: '',
      description: '',
      active: true,
    };
    this.submitted = false;
    this.groupDialog = true;
  }

  hideDialog() {
    this.groupDialog = false;
    this.submitted = false;
  }

  saveGroup() {
    this.submitted = true;
    this.groupService.createGroup(this.group).subscribe((res: any) => {
      console.log(res);
      /*
      if (res.errorList.length) {
        this.message = this.commonService.saveUnsuccessful + ' ' + res.errorList[0].message;
      } else {
        this.show('Sikeres mentés');
        this.message = this.commonService.saveSuccessful;
      }
      */
      this.getGroupList();
    });
    this.groupDialog = false;
  }
}
