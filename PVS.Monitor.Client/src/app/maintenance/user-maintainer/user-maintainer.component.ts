import { Component, OnInit } from '@angular/core';
import { Group } from '@app/models/group';
import { EditUser } from '@app/models/editUser';
import { Product } from '@app/models/Product';
import { Role } from '@app/models/role';
import { User } from '@app/models/user';
import { GroupService } from '@app/services/group.service';
import { UserService } from '@app/services/user.service';
import { AbstractControlOptions, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
// import { UserMaintainerService } from '@app/services/user-maintainer.service';

@Component({
  selector: 'app-user-maintainer',
  templateUrl: './user-maintainer.component.html',
  styleUrls: ['./user-maintainer.component.scss'],
})
export class UserMaintainerComponent implements OnInit {
  // stepper
  isLinear = true;
  firstUserFormGroup: FormGroup;
  secondUserFormGroup: FormGroup;
  thirdUserFormGroup: FormGroup;


  // snackbar 
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';


  editUserForm: FormGroup;

  passwordFiledVisibility = false;
  editing = false;
  answered = false;

  userList: any[] = [];
  UserList: User[] = [];
  selectedUser: User;
  user: User;
  editUser: EditUser;

  dialogHeader = "Új felhasználó"

  roleString: string;

  role: Role;
  roles: Role[] = [];

  clonedUsers: { [s: string]: User } = {};

  // dialog
  userDialog: boolean;
  submitted: boolean;
  message: string;

  password: string;
  confirmPassword: string;

  adminRole = false;
  serviceRole = false;
  reportRole = false;

  hasRole: boolean;
  public passwordEquals: boolean;

  constructor(private userService: UserService, private messageService: MessageService, private _formBuilder: FormBuilder, private _snackBar: MatSnackBar) {
    //  private userMaintainerService: UserMaintainerService,
  }

  ngOnInit() {
    this.getUserList();
    this.passwordEquals = false;
    this.editUserForm = new FormGroup({
      loginName: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      phone: new FormControl('', [
        Validators.minLength(4)
      ]),
      adminRole: new FormControl(false, [
        Validators.required,
      ]),
      reportRole: new FormControl(false, [
        Validators.required,
      ]),
      serviceRole: new FormControl(false, [
        Validators.required,
      ]),
    });

    // stepper
    this.firstUserFormGroup = this._formBuilder.group({
      loginName: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      phone: new FormControl('', [
        Validators.minLength(4)
      ]),
    });


    this.secondUserFormGroup = this._formBuilder.group({
      roleSelected: new FormControl(false, [
        Validators.requiredTrue
      ])
    });

    this.thirdUserFormGroup = this._formBuilder.group({
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      confirmPassword: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ])
    }, { validators: this.checkFormPasswords });

  }

  public requireOneControl() {
    if (this.adminRole || this.reportRole || this.serviceRole) {
      console.log('OK');
      this.secondUserFormGroup.setErrors(null);
      this.secondUserFormGroup.value.roleSelected = true;
      this.secondUserFormGroup.updateValueAndValidity();
    }
    else {
      console.log('at least one item');
      this.secondUserFormGroup.setErrors({ 'invalid': true });
    }
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'OK', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  getUserList() {
    this.UserList = [];
    this.userService.getUserList().subscribe((response: any) => {
      this.userList = response;
      this.userList.forEach((userItem) => {
        this.roleString = '';
        userItem.roles.forEach((roleItem: string) => {
          if (this.roleString === '') {
            this.roleString += roleItem;
          } else {
            this.roleString += ", " + roleItem;
          }
        });
        this.user = {
          id: userItem.id,
          name: userItem.name,
          loginName: userItem.loginName,
          email: userItem.email,
          phone: userItem.phone,
          roles: this.roleString,
          active: userItem.active,
          token: userItem.token,
          refreshToken: userItem.refreshToken,
          refreshTokenExpiration: userItem.refreshTokenExpiration
        };
        this.UserList.push(this.user);
      });
    });
  }

  onEdit(user: User) {
    console.log(user);
  }

  // jelszo modositas
  showPasswordFileds() {
    this.passwordFiledVisibility = true;
    this.thirdUserFormGroup.get('password').setErrors({});
    this.thirdUserFormGroup.get('confirmPassword').setErrors({ 'required': true });
    this.thirdUserFormGroup.updateValueAndValidity();
    this.answered = true;
  }

  //jelszo modositas neluli tovabblepes
  goWithoutModify() {
    this.thirdUserFormGroup.get('password').setErrors(null);
    this.thirdUserFormGroup.get('confirmPassword').setErrors(null);
    this.thirdUserFormGroup.updateValueAndValidity();
    this.answered = true;
  }

  // felhasználó törlése
  removeUser(user: User) {
    this.userService.deleteUser(user.id).subscribe((response: any) => {
      if (response.isSuccess) {
        console.log(response);
        this.openSnackBar('Sikeres törlés');
        this.getUserList();
      }
    });
  }

  // felhasználó módosítása
  onRowEdit(user: User) {
    // this.clonedUsers[user.id] = { ...user };
    this.editUser = null;
    this.editing = true;
    this.adminRole = false;
    this.reportRole = false;
    this.serviceRole = false;
    var any;
    console.log(user);
    this.userService.getUserByLoginName(user.loginName).subscribe((response: any) => {
      any = response.roles;
      any.forEach((role: string) => {
        if (role === 'Admin') {
          this.adminRole = true;
          this.roles.push({ id: 1, name: role });
        }
        if (role === 'ReportUser') {
          this.reportRole = true;
          this.roles.push({ id: 2, name: role });
        }
        if (role === 'ServiceUser') {
          this.serviceRole = true;
          this.roles.push({ id: 3, name: role });
        }
      });
      console.log(this.roles)
      this.editUser = {
        id: user.id,
        loginName: user.loginName,
        name: user.name,
        email: user.email,
        phone: user.phone,
        roles: this.roles,
        password: ""
      };
    });
    this.dialogHeader = 'Felhasználó adatainak módosítása';
    this.submitted = false;
    this.userDialog = true;
  }

  onRowEditSave(group: Group) {
    /* if (group.price > 0) {
       delete this.clonedGroups[product.id];
       this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Product is updated' });
     }
     else {
       this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Invalid Price' });
     }
     */
  }

  onRowEditCancel(user: User, index: number) {
    this.UserList[index] = this.clonedUsers[user.id];
    delete this.clonedUsers[user.id];
  }

  openNew() {
    this.editing = false;
    this.adminRole = false;
    this.reportRole = false;
    this.serviceRole = false;
    this.dialogHeader = "Új felhasználó rögzítése";
    this.editUser = {
      id: 0,
      loginName: "",
      name: "",
      email: "",
      phone: "",
      roles: [],
      password: ""
    };

    this.submitted = false;
    this.userDialog = true;
  }

  hideDialog() {
    this.userDialog = false;
    this.submitted = false;
  }

  /*
    adminRole: boolean;
  serviceRole: boolean;
  reportRole: boolean;
  */

  checkRoles(): boolean {
    this.hasRole = false;
    this.editUser.roles = [];
    if (this.adminRole) {
      this.role = {
        id: 1,
        name: 'Admin'
      }
      this.editUser.roles.push(this.role);
      this.hasRole = true;
    }
    if (this.reportRole) {
      this.role = {
        id: 2,
        name: 'ReportUser'
      }
      this.editUser.roles.push(this.role);
      this.hasRole = true;
    }
    if (this.serviceRole) {
      this.role = {
        id: 3,
        name: 'ServiceUser'
      }
      this.editUser.roles.push(this.role);
      this.hasRole = true;
    }
    console.log(this.hasRole);
    return this.hasRole;
  }



  checkFormPasswords(group: FormGroup) { // here we have the 'passwords' group
    const password = group.get('password').value;
    const confirmPassword = group.get('confirmPassword').value;
    return password === confirmPassword ? null : { notSame: true }
  }

  checkPasswords(): boolean {
    this.passwordEquals = false;
    this.editUser.password = '';
    if (this.password !== this.confirmPassword) {
      this.passwordEquals = false;
      return false;
    }
    else {
      this.passwordEquals = true;
      this.editUser.password = this.password;
      return true;
    }
  }


  saveUser() {
    if (this.editing) {
      this.editing = false;
    } else {
      this.submitted = true;
      if (this.checkPasswords() && this.checkRoles()) {
        console.log(this.editUser);
        this.userService.createUser(this.editUser).subscribe((res: any) => {
          console.log(res);
          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Sikeres mentés!' });
          this.openSnackBar('Sikeres mentés');
          this.getUserList();
          this.userDialog = false;
        });
      }
      else {
        console.log('sikertelen');
        this.openSnackBar('Sikertelen mentés');
      }
    }
    /*
    
           this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Product is updated' });
     }
     else {
       this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Invalid Price' });
    
    
    
    this.userService.createUser(this.user).subscribe((res: any) => {
          console.log(res);
          
          if (res.errorList.length) {
            this.message = this.commonService.saveUnsuccessful + ' ' + res.errorList[0].message;
          } else {

          }
          
          this.getUserList();
        });
        */
    //  this.userDialog = false;
  }
}
